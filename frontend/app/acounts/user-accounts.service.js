angular.module('city_alert').service('UserAccountsService', ['$q', 'PromiseChain','HttpService', function ($q, PromiseChain,HttpService) {
    return {
        addAccount: function (data) {
            return HttpService.formPost('/user/create', data);
        },
        getAccounts: function(){
            return HttpService.handle('GET', '/users');
        },
        deleteAccount: function (data) {
            return HttpService.handle('DELETE', '/user/'+data.id);
        },
        updateAccount: function (newData, oldData) {
            return HttpService.handle('PUT', '/user', newData);
        },
        initSecondaryApp: function (app) {
            secondaryApp = app;
        }
    };
}]);