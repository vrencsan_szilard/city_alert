angular.module('city_alert').controller('UserAccountsController', ['GridFactory', '$scope', '$uibModal', 'TranslationService', 'ErrorService',
    'UserAccountsService', 'ValidationFactory', '$filter','NotificationService',
    function (GridFactory, $scope, $uibModal, TranslationService, ErrorService, UserAccountsService, ValidationFactory, $filter, NotificationService) {


        //region Declarations - non $scope
        //endregion

        //region Declarations - $scope
        $scope.runtimeData = {
            dataLoaded: false
        };

        $scope.gridFactory = {};

        $scope.actions = {
            delete: function (item) {
                showModal({
                    key: 'delete',
                    item: item,
                    templateUrl: 'confirmation-modal',
                    title: TranslationService.translate('CONFIRM_DELETE')
                });
            },
            edit: function (item) {
                showModal({
                    key: 'edit',
                    item: item
                });
            }
        };

        //endregion

        //region Functions non $scope

        function UserAccountEditModalController($scope, $uibModalInstance, data, notificationTypesObject) {
            $scope.data = data;
            var validationFactory   = null,
                validationResult;
            $scope.passwordPlaceholder = '';
            $scope.notificationTypes = angular.copy(notificationTypesObject.notificationTypes);
            $scope.output = {
                model: []
            };
            $scope.runtimeData = {
                username: '',
                password: '',
                type: null,
                notificationTypes: ''
            };
    

            if (data.key == 'edit') {
                $scope.runtimeData = angular.copy(data.item);
                // console.log($scope.runtimeData);
               
                angular.forEach($scope.runtimeData.notificationTypes, function(outputMod){
                    angular.forEach( $scope.notificationTypes, function(inputMod){
                        if (inputMod.id === outputMod.id) {
                            inputMod.ticked = true;
                        }
                    });
                });
                $scope.passwordPlaceholder = "************";
            }

            function doTheMagic() {
                $scope.runtimeData.notificationTypes = '';
                angular.forEach($scope.output.model, function(notificationType){
                    $scope.runtimeData.notificationTypes += notificationType.id + ' ';
                });
                $scope.runtimeData.notificationTypes = $scope.runtimeData.notificationTypes.slice(0, -1);

                if ($scope.runtimeData.type !== null) {
                    //for an app admin the notification types are not needed
                    delete $scope.runtimeData.notificationTypes;  
                }
                switch (data.key) {
                    case 'add':
                        {
                            if ($scope.runtimeData.type === null) {
                                //for a limited account the type is not needed
                                delete $scope.runtimeData.type;
                            } 
                            UserAccountsService.addAccount($scope.runtimeData).then(function () {
                                $uibModalInstance.close();
                                $scope.notificationTypes = notificationTypesObject.notificationTypes;
                            }).catch(function (error) {
                                if (angular.isUndefined($scope.runtimeData.type)) {
                                    $scope.runtimeData.type = null;
                                }
                                ErrorService.showErrorMessage(error);
                            });
                            break;
                        }
                    case 'edit':
                        {
                            if (angular.isUndefined($scope.runtimeData.password) || $scope.runtimeData.password === null || $scope.runtimeData.password.length === 0) {
                                delete $scope.runtimeData.password;
                            }
                            UserAccountsService.updateAccount($scope.runtimeData, $scope.data.item).then(function () {
                                $uibModalInstance.close();
                                $scope.notificationTypes = notificationTypesObject.notificationTypes;
                            }).catch(function (error) {
                                console.log('the error', error);
                                if (error.data.code === 'ER_DUP_ENTRY') {
                                    error.data.code = 'auth/user-already-in-use';
                                }
                                ErrorService.showErrorMessage(error);
                            });
                            break;
                        }
                    case 'delete':
                        {
                            UserAccountsService.deleteAccount($scope.data.item).then(function () {
                                $uibModalInstance.close();
                            }).catch(function (error) {
                                ErrorService.showErrorMessage(error);
                            });
                            break;
                        }
                }
            }


            $scope.$on('CLOSE_ALL_MODALS_NOTIFICATION', function () {
                $uibModalInstance.dismiss('cancel');
            });

            $scope.ok = function () {
                if (data.key !== 'delete') {
                    validationFactory = new ValidationFactory();
                    if (data.key !== 'edit') {
                        validationFactory.addRule($scope.runtimeData.password, 'required', 'pass-input', 'PASSWORD_REQUIRED');
                    }
                    validationFactory.addRule($scope.runtimeData.username, 'required', 'email-input', 'USERNAME_REQUIRED');
                    if ($scope.runtimeData.type === null) {
                        validationFactory.addRule($scope.output.model, 'required-array', '', 'CATEGORY_REQUIRED');
                    }

                    validationResult = validationFactory.validateWithResultArray();
                    if (validationResult.isValid) {
                        doTheMagic();
                    } else {
                        angular.forEach(validationResult.array, function (v) {
                            ErrorService.showErrorMessage({
                                code: v.errorMessage,
                                message: v.errorMessage
                            });
                        });
                    }
                } else {
                    doTheMagic();
                }

            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }

        function showModal(data) {
            var out = $uibModal.open({
                animation: true,
                templateUrl: data.templateUrl || 'edit-user-popup.html',
                controller: ['$scope', '$uibModalInstance', 'data', 'notificationTypesObject', UserAccountEditModalController],
                size: 'md',
                resolve: {
                    data: data,
                    notificationTypesObject: {notificationTypes:$scope.notificationTypes}
                }
            });
            out.result.then(function () {
                switch (data.key) {
                    case 'add':
                        {
                            alertify.success(TranslationService.translate('ACCOUNT_WAS_CREATED', 'The account has been created.'));
                            break;
                        }
                    case 'edit':
                        {
                            alertify.success(TranslationService.translate('CHANGES_SAVED', 'The changes have been saved.'));
                            break;
                        }
                    case 'delete':
                        {
                            alertify.success(TranslationService.translate('ACCOUNT_WAS_DELETED', 'The account has been deleted.'));
                            break;
                        }
                }
                reloadGrid();
            }, function () {
                //modal canceled
            });
        }


        function reloadGrid() {
            UserAccountsService.getAccounts().then(function(result){
                $scope.gridFactory.reloadWithItems(result.data);
                $scope.runtimeData.dataLoaded = true;
            }).catch(function(error){
                ErrorService.showErrorMessage(error);
            });
        }
        //endregion

        //region Functions $scope

        $scope.addAccount = function () {
            showModal({
                key: 'add'
            });
        };

        $scope.getAccountTypeString = function(string){
            if (string === 'APP_ADMIN') {
                var a = TranslationService.translate('GENERAL_ACCOUNT');
                return a;
            } else {
                return TranslationService.translate('LIMITED_ACCOUNT');
            }
        };
        //endregion


        //region Event listeners

        // Unregister
        $scope.$on('$destroy', function () {

        });
        //endregion

        //region Initialization
        var init = function () {
            NotificationService.getNotificationTypes().then(function(res){
                $scope.notificationTypes = res.data;
                UserAccountsService.getAccounts().then(function(result){
                    $scope.gridFactory = new GridFactory('user_accounts_grid', result.data);
                    $scope.runtimeData.dataLoaded = true;
                }).catch(function(error){
                    ErrorService.showErrorMessage(error);
                });
            }).catch(function(error){
                ErrorService.showErrorMessage(error);
            });
        };
        init();
        //endregion
    }
]);