angular.module('city_alert').controller('DbInfoController', ['$scope','DistrictService','NotificationService',
    function ($scope, DistrictService, NotificationService) {

        $scope.notificationTypeImages = [
            'community_assets.png', 'heavy_traffic.png', 'noise.png', 'road_hole.png', 'road_mark.png', 'road_work.png', 'traffic_lights.png', 'trash.png', 'weather.png', 'snow.png', 'sun.png', 'water.png', 'rain.png', 'all_weather.png', 'gun.png', 'guitar.png', 'bomb.png', 'accident.png', 'epidemic.png', 'fire.png', 'poison.png', 'protest.png', 'quake.png', 'stage.png'
        ];


        DistrictService.getDistricts().then(function(result){
            $scope.districtList = result.data;
        });

        NotificationService.getNotificationTypes().then(function(result){
            $scope.notificationTypes = result.data;
        });
    }
]);