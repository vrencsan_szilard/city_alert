/**
 * Created by LIH on 7/24/2017.
 */
angular.module('city_alert').controller('HeaderCtrl', ['$scope','$rootScope','LoginService', function($scope,$rootScope,LoginService){

    //region Declarations - non $scope
    //endregion

    //region Declarations - $scope
    $scope.userData = {};
    $scope.cityData = {};
    $scope.adminType = null;
    $scope.adminTypes = LoginService.adminTypes;
    //endregion


    //region Functions non $scope
    //endregion

    //region Functions $scope
    //endregion


    //region Event listeners

    // Unregister
    $scope.$on('$destroy', function () {

    });
    //endregion

    //region Initialization
    var init = function() {
        $rootScope.showMenu = true;
        $scope.adminType = LoginService.getCurrentAdminType();
        if ($scope.adminType === LoginService.adminTypes.ADMIN) {
            // $scope.userData = LoginService.getUserData();
            // $scope.userData.$loaded().then(function () {
            //     $scope.cityData = LoginService.getCityData($scope.userData.cityKey);
            //     $scope.cityData.$loaded().then(function () {
            //     }, function (error) {
            //         console.log(error);
            //     });
            // });
        }
    };
    init();
    //endregion
}]);