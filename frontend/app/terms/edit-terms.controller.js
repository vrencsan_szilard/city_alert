angular.module('city_alert').controller('EditTermsCtrl', ['$scope','TermsService','ErrorService','TranslationService',function($scope,TermsService,ErrorService,TranslationService){
    $scope.termsText        = '';
    $scope.oldText        = '';
    $scope.isButtonDisabled = true;

    
    var termsID,
        defaultTermsHtml = '<p class="c2">\r\n'+
        '            <span class="c3">Termeni și condiții</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c4">Termeni și condiții de funcționare</span>\r\n'+
        '            <span class="c0"> a aplicației „Cluj Now” aparținând Primăriei Municipiului Cluj-Napoca</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">           </span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">VĂ RUGĂM SĂ CITIȚI CU ATENȚIE TERMENII ȘI CONDIȚIILE PREZENTATE ÎN CONTINUARE.\r\n'+
        '                ACESTEA CONȚIN INFORMAȚII IMPORTANTE CU PRIVIRE LA DREPTURILE ȘI OBLIGAȚIILE DUMNEAVOASTRĂ.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c11">\r\n'+
        '            <span class="c3">1. DESPRE APLICAȚIE</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c6">\r\n'+
        '            <span class="c0">„Cluj Now”  este o modalitate simplă de informare a clujenilor și turiștilor care\r\n'+
        '                ne vizitează orașul privind:</span>\r\n'+
        '        </p>\r\n'+
        '        <ul class="c8 lst-kix_list_2-0 start">\r\n'+
        '            <li class="c6 c9">\r\n'+
        '                <span class="c0">lucrările care se efectuează pe domeniul public al municipiului Cluj-Napoca și care influențează\r\n'+
        '                    traficul rutier;</span>\r\n'+
        '            </li>\r\n'+
        '            <li class="c6 c9">\r\n'+
        '                <span class="c0">restricțiile de circulație ca urmare a organizării unor evenimente și manifestări publice</span>\r\n'+
        '            </li>\r\n'+
        '            <li class="c6 c9">\r\n'+
        '                <span class="c0">alerte transmise de către Inspectoratul pentru Urgență Cluj în legătură cu diferite\r\n'+
        '                    fenomene metereologice sau eventuale situații de urgență. </span>\r\n'+
        '            </li>\r\n'+
        '        </ul>\r\n'+
        '        <p class="c11">\r\n'+
        '            <span class="c0">Aplicația este disponibilă pentru sistemele de operare iOS și Android.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c3">2. ACCEPTAREA TERMENILOR DE FUNCȚIONARE</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">Prin descărcarea și folosirea aplicației „Cluj Now” acceptați și sunteți\r\n'+
        '                de acord cu aceşti </span>\r\n'+
        '            <span class="c4">Termeni și condiții</span>\r\n'+
        '            <span class="c0"> </span>\r\n'+
        '            <span class="c4">de funcționare</span>\r\n'+
        '            <span class="c0">. Primăria Municipiului Cluj-Napoca poate să modifice acești </span>\r\n'+
        '            <span class="c4">Termeni și condiții</span>\r\n'+
        '            <span class="c0"> de funcționare oricând, cu scopul de a îmbunătăţi serviciul. Prin continuarea\r\n'+
        '                folosirii aplicației „Cluj Now”, după ce sunt efectuate schimbări ai acestor </span>\r\n'+
        '            <span\r\n'+
        '                class="c4">Termeni și condiții de funcționare</span>\r\n'+
        '                <span class="c0">, sunteți de acord să acceptaţi acele schimbări.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c11">\r\n'+
        '            <span class="c0">Aplicația este disponibilă pentru sistemele de operare iOS și Android.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">În cadrul aplicaței „Cluj Now”, termenii de mai jos au următoarea semnificație:\r\n'+
        '                </span>\r\n'+
        '        </p>\r\n'+
        '        <ul class="c8 lst-kix_list_1-0 start">\r\n'+
        '            <li class="c2 c9">\r\n'+
        '                <span class="c4">Utilizator</span>\r\n'+
        '                <span class="c0"> – orice persoană fizică care se înregistrează în Aplicație și\r\n'+
        '                    accesează conținutul pus la dizpoziție de către deținătorul Aplicației.</span>\r\n'+
        '            </li>\r\n'+
        '            <li class="c2 c9">\r\n'+
        '                <span class="c4">Atenționări metereologice</span>\r\n'+
        '                <a id="id.gjdgxs"></a>\r\n'+
        '                <span class="c0"> – reprezintă tipul de notificare transmis de către Inspectoratul de Urgență Cluj\r\n'+
        '                    în legătură cu diferite fenomene metereologice sau eventuale situații de urgență.</span>\r\n'+
        '            </li>\r\n'+
        '            <li class="c2 c9">\r\n'+
        '                <span class="c4">Restricții circulație</span>\r\n'+
        '                <span class="c0"> – </span>\r\n'+
        '                <span class="c4">lucrări </span>\r\n'+
        '                <span class="c0"> - reprezintă tipul de notificare ce informează utilizatorul aplicației cu privire la restricţiile\r\n'+
        '                    de circulaţie ca urmare a efectuării unor lucrări la infrastructura stradală, reţele\r\n'+
        '                    utilitare sau alte lucrări.</span>\r\n'+
        '            </li>\r\n'+
        '            <li class="c2 c9">\r\n'+
        '                <span class="c4">Restricții circulație – evenimente – </span>\r\n'+
        '                <span class="c0">reprezintă tipul de notificare ce</span>\r\n'+
        '                <span class="c4"> </span>\r\n'+
        '                <span class="c0">informează utilizatorul aplicației cu privire la restricţiile de circulaţie ca urmare a organizării\r\n'+
        '                    unor evenimente sau manifestări publice. </span>\r\n'+
        '            </li>\r\n'+
        '        </ul>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c3">3. LIMITAREA FOLOSIRII APLICAȚIEI</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">Aplicația „Cluj Info” este destinată persoanelor care au împlinit vârsta de 18\r\n'+
        '                ani şi care doresc să se informeze cu privire la situaţiile prevăzute la punctul 1- </span>\r\n'+
        '            <span\r\n'+
        '                class="c4">Despre Aplicaţie</span>\r\n'+
        '                <span class="c0">.  În cazul utilizării aplicației de către persoane minore, aceasta poate fi descărcată\r\n'+
        '                    și utilizată doar împreună cu, sau sub supravegherea părinților sau a tutorelui,\r\n'+
        '                    care răspund pentru acţiunile minorului.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c3">4. CONFIDENȚIALITATE ȘI SECURITATEA DATELOR CU CARACTER PERSONAL</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">Orice informație pusă la dispoziție de către dumneavoastră sau colectată de către\r\n'+
        '                Primăria Municipiului Cluj-Napoca atunci când folosiți aplicația „Cluj Now”\r\n'+
        '                este supusă Politicii de Confidențialitate a „Cluj Now”. Utilizatorul își poate\r\n'+
        '                seta locația dispozitivului mobil pentru a primi notificări furnizate de către Aplicație.\r\n'+
        '                Aplicația „Cluj Now” stochează informații cu caracter personal provenind din informaţiile\r\n'+
        '                privind localizarea telefonului dumneavoastră atunci când selectați această opțiune.\r\n'+
        '                Prin selectarea acestei opțiuni, ați fost informat și v-ați exprimat acordul în mod\r\n'+
        '                explicit și fără echivoc ca aplicația „Cluj Now” să folosească datele\r\n'+
        '                dumneavoastră cu caracter personal.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">Aplicația „Cluj Now” asigură păstrarea confidențialității datelor cu caracter\r\n'+
        '                personal furnizate pentru utilizarea acesteia. Această obligație nu se aplică în cazul oricărei\r\n'+
        '                informații dezvăluite de utilizatori către terți.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">Aplicația „Cluj Now” prelucrează datele cu caracter personal și informațiile deținute/transmise\r\n'+
        '                de către dumneavoastră cu bună credință, asigurând respectarea dreptului la viața\r\n'+
        '                privată și a legislației în vigoare.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c3">5. PROPRIETATE INTELECTUALĂ</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">Primăria Municipiului Cluj-Napoca și/sau licențiatorii săi sunt unicii proprietari ai aplicației\r\n'+
        '                „Cluj Now”, ceea ce include orice software, domeniu și conținut pus la dispoziție\r\n'+
        '                prin intermediul aplicației sau paginilor de Social Media. „Cluj Now” este protejată de\r\n'+
        '                drepturile de autor din România. Aceasta se poate folosi doar în scopuri personale și necomerciale,\r\n'+
        '                iar „Cluj Now” va acorda o licență limitată în acest sens. Orice folosire neautorizată\r\n'+
        '                a aplicației „Cluj Now” va înceta licența limitată pusă la dispoziție\r\n'+
        '                de către Primăria Municipiului Cluj-Napoca. Grafica, logo-urile, imaginile și numele serviciilor\r\n'+
        '                legate de aplicația „Cluj Now” nu pot fi folosite fără acordul scris al Primăriei\r\n'+
        '                Municipiului Cluj-Napoca. Toate celelalte mărci înregistrate ce nu sunt proprietatea Primăriei\r\n'+
        '                Municipiului Cluj-Napoca și apar în aplicația „Cluj Now”, sunt proprietatea respectivilor\r\n'+
        '                proprietari ce pot fi sau nu asociați cu, sau sponsorizați de „Cluj Now”.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c3">6. UTILIZAREA APLICAȚIEI</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">Prin descărcarea și folosirea aplicației „Cluj Now”, sunteți de acord să nu\r\n'+
        '                interferați cu serverele sau rețelele ce au legătură cu aplicația „Cluj Now”\r\n'+
        '                sau să nu încălcați nicio procedură, politică sau regulament al rețelelor\r\n'+
        '                ce au legătură cu aplicația „Cluj Now”. De asemenea, sunteți de acord să\r\n'+
        '                nu:</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">a.            folosiți aplicația „Cluj Now” în scopuri\r\n'+
        '                ilegale;</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">b.            revindeți sau exportați software-ul asociat cu aplicația\r\n'+
        '                „Cluj Now”.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">„Cluj Now” nu promovează și nu recomandă sau tolerează folosirea aplicației\r\n'+
        '                în timpul activităților care ar putea pune viața utilizatorilor în pericol, cum ar\r\n'+
        '                fi conducerea de vehicule. Sunteți de acord să nu utilizați aplicația „Cluj Now”\r\n'+
        '                în timpul desfășurării unor astfel de activități, „Cluj Now” nu este\r\n'+
        '                responsabilă pentru utilizarea aplicației în astfel de condiții, utilizarea se va realiza\r\n'+
        '                pe propria răspundere.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">       </span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">    </span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">     </span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c3">7. SITE-URI ALE ALTOR TERȚI</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">Aplicația „Cluj Now” poate conține link-uri către site-urile web ale unor părți\r\n'+
        '                terțe sau programe ce nu sunt controlate de către sau asociate cu „Cluj Now”. Primăria\r\n'+
        '                Municipiului Cluj-Napoca nu este responsabilă pentru conținutul, ofertele sau politicile de confidențialitate\r\n'+
        '                ale unor asemenea site-uri sau programe, inclusiv, dar fără a se limita la, răscumpărarea\r\n'+
        '                ofertelor de către dumneavoastră sau refuzul comerciantului de a onora orice ofertă. Relația\r\n'+
        '                dumneavoastră cu site-urile părților terțe se desfășoară doar între\r\n'+
        '                dumneavoastră și respectivele părți terțe.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c3">8. COSTURILE DE UTILIZARE ALE APLICAȚIEI „Cluj Now”</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">Primăria Municipiului Cluj-Napoca vă pune la dispoziție aplicația „Cluj Now” în\r\n'+
        '                mod gratuit. Primăria Municipiului Cluj-Napoca nu este responsabilă de percepția unor costuri\r\n'+
        '                suplimentare atunci când utilizați „Cluj Now”. În cazul în care întâmpinați\r\n'+
        '                o astfel de problemă, vă rugăm să contactați furnizorul dumneavoastră de internet\r\n'+
        '                WiFi sau de date mobile. Plata unor asemenea costuri este responsabilitatea dumneavoastră. Este posibil\r\n'+
        '                ca aplicația „Cluj Now” să nu fie accesibilă în unele țări, acest\r\n'+
        '                lucru depinzând de compatibilitatea aplicației „Cluj Now” cu alte rețele străine.\r\n'+
        '                Posibilitatea de a descărca aplicația „Cluj Now” poate fi restricționată sau\r\n'+
        '                prejudiciată atunci când folosiți opțiunea de roaming în străinătate.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c3">9. GARANȚIE ȘI RĂSPUNDERE</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">Primăria Municipiului Cluj-Napoca furnizează aplicația „Cluj Now” ca atare și fără\r\n'+
        '                nicio condiție expresă, implicită sau statutară. Primăria Municipiului Cluj-Napoca nu\r\n'+
        '                este responsabilă pentru nicio eroare sau omisiune apărută în cadrul aplicației „Cluj\r\n'+
        '                Now”, orice defecțiune, întârziere sau întrerupere apărută în cadrul\r\n'+
        '                aplicației „Cluj Now”, orice pierdere sau defecțiune ce reiese din utilizarea aplicației\r\n'+
        '                „Cluj Now”, orice defectare a telefonului dumneavoastră mobil, sau orice manieră de a utiliza\r\n'+
        '                aplicația „Cluj Now” de către utilizatorii acesteia. Primăria Municipiului Cluj-Napoca\r\n'+
        '                își rezervă dreptul de a furniza aplicația „Cluj Now” la discreția sa.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">Primăria Municipiului Cluj-Napoca, asociații, administratorii, reprezentanții, angajații (împreună\r\n'+
        '                sau individual) nu vor fi responsabili în niciun caz pentru eventuale daune sau orice prejudiciu, de orice\r\n'+
        '                fel, direct sau indirect, ce reiese din utilizarea sau are legătură cu aplicația „Cluj Now”,\r\n'+
        '                chiar dacă posibilitatea daunelor sau a producerii prejudiciului a fost menționată sau nu. </span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c3">10. RECLAMAȚII</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">Prin agrearea acestor termeni și condiții sunteți de acord ca orice reclamație, dispută\r\n'+
        '                sau controversă (atât ca prejudiciu sau altfel, chiar dacă sunt deja prezente sau viitoare, incluzând\r\n'+
        '                cele stabilite de lege, drept comun sau reclamații echitabile) între dumneavoastră si „Cluj\r\n'+
        '                Now” ce reiese sau are legătură cu acești </span>\r\n'+
        '            <span class="c4">Termeni și condiții de funcționare</span>\r\n'+
        '            <span class="c0">, interpretarea sau încălcarea acestora vor fi guvernate și rezolvate în conformitate cu legile\r\n'+
        '                din România, competența de soluționare aparținând instanțelor de la sediul „Cluj\r\n'+
        '                Now”.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">Eventualele litigii apărute între deținătorul Aplicației și utilizator se vor rezolva\r\n'+
        '                pe cale amiabilă, iar în cazul în care acest lucru nu este posibil, litigiile vor fi soluționate\r\n'+
        '                de către instanțele române competente.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c3">11. DISPOZIŢII FINALE</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">În cazul în care orice prevedere conținută în acești termeni și condiții\r\n'+
        '                de funcționare, este considerată invalidă sau neexecutorie, asemenea prevedere va fi anulată,\r\n'+
        '                iar celelalte prevederi vor fi considerate aplicabile în continuare. Titlurile au doar scop de referință\r\n'+
        '                și nu definesc, limitează, interpretează sau descriu scopul acelei secțiuni. Incapacitatea\r\n'+
        '                Primăriei Municipiului Cluj-Napoca de a îndeplini una dintre prevederile acestor </span>\r\n'+
        '            <span class="c4">Termeni și condiții de funcționare </span>\r\n'+
        '            <span class="c0">nu va constitui renunțarea la acea prevedere sau la oricare alta.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0"> Deținătorul aplicației își rezervă dreptul de a modifica în orice moment\r\n'+
        '                termenii și condițiile „Cluj Now” fără avertizare prealabilă. Orice modificare\r\n'+
        '                intră în vigoare imediat, din momentul afișării sale în aplicație. Aceste modificări\r\n'+
        '                nu afectează operațiile anterioare.</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c3">12. CONTACTAȚI-NE</span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c2">\r\n'+
        '            <span class="c0">În cazul în care aveți întrebări referitoare la acești </span>\r\n'+
        '            <span class="c4">Termeni și condiții de funcționare</span>\r\n'+
        '            <span class="c0"> sau descărcarea și/sau folosirea aplicației „Cluj Now”, nu ezitați să\r\n'+
        '                ne contactați la telefon 0264 – 596030, interior 4150 sau prin adresa de email </span>\r\n'+
        '            <span class="c7">\r\n'+
        '                <a class="c10" href="mailto:informatica@primariaclujnapoca.ro">informatica@primariaclujnapoca.ro</a>\r\n'+
        '            </span>\r\n'+
        '            <span class="c0">. </span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c1">\r\n'+
        '            <span class="c0"></span>\r\n'+
        '        </p>\r\n'+
        '        <p class="c12">\r\n'+
        '            <span class="c13"></span>\r\n'+
        '        </p>';
            
        ;
    
    function init(){
        TermsService.getTerms().then(function(result){
            var html = result.data[0].text.length === 0 ? defaultTermsHtml : result.data[0].text
            $scope.termsText =  html;
            $scope.oldText   =  html;
            termsID          = result.data[0].id;
        }).catch(function (error) {
            ErrorService.showErrorMessage(error);
        });
    }

    $scope.save = function(){
        
        TermsService.updateTerms({id: termsID, text: $scope.termsText}).then(function(){
            $scope.oldText = angular.copy($scope.termsText);
            alertify.success(TranslationService.translate('CHANGES_SAVED', 'The changes have been saved.'));
        }).catch(function (error) {
            ErrorService.showErrorMessage(error);
        });
    };

    $scope.changed = function(){
        console.log($scope.oldText.length, $scope.termsText.length);
    }

    init();
}]);