/**
 * Created by LIH on 7/24/2017.
 */
angular.module('city_alert').service('TermsService', ['HttpService',function(HttpService){
    return {
        getTerms: function(){
            return HttpService.handle('GET','/terms');
        },
        updateTerms: function(newTerms){
            return HttpService.handle('PUT','/terms', newTerms);
        }
    }
}]);