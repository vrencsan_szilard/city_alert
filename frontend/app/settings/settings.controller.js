/**
 * Created by LIH on 7/27/2017.
 */
angular.module('city_alert').controller('SettingsCtrl', ['$scope', 'ErrorService', 'LoginService', 'NotificationService',
    '$uibModal', 'TranslationService', 'ValidationFactory',
    function ($scope, ErrorService, LoginService, NotificationService, $uibModal, TranslationService, ValidationFactory) {

        //region Declarations - non $scope
        //endregion

        //region Declarations - $scope

        $scope.notificationTypes = [];
        $scope.runtimeData = {
            dataLoaded: false,
            selectedReportTypeImage: 'other.png'
        };
        //endregion

        //region Functions non $scope
        function NotificationTypeController($scope, $uibModalInstance, data) {
            $scope.data = data;
            $scope.notificationTypeImages = [
                'community_assets.png', 'heavy_traffic.png', 'noise.png', 'road_hole.png', 'road_mark.png', 'road_work.png', 'traffic_lights.png', 'trash.png', 'weather.png', 'snow.png', 'sun.png', 'water.png', 'rain.png', 'all_weather.png', 'gun.png', 'guitar.png', 'bomb.png', 'accident.png', 'epidemic.png', 'fire.png', 'poison.png', 'protest.png', 'quake.png', 'stage.png', 'alert.png'
            ];

            if (data.key === 'edit_notification_type') {
                $scope.notificationType = angular.copy(data.notificationType);
            } else {
                $scope.notificationType = {
                    img: 'other.png',
                    name: ''
                };
            }

            $scope.selectImage = function (img) {
                $scope.notificationType.img = img;
            };

            $scope.$on('CLOSE_ALL_MODALS_NOTIFICATION', function () {
                $uibModalInstance.dismiss('cancel');
            });

            $scope.ok = function () {
                validationFactory = new ValidationFactory();
                validationFactory.addRule($scope.notificationType.name, 'required', 'name-input', 'NAME_REQUIRED');
                switch (data.key) {
                    case 'delete':
                        {
                            NotificationService.deleteNotificationType(data.item).then(function () {
                                $uibModalInstance.close();
                            }).catch(function (error) {
                                ErrorService.showErrorMessage({
                                    code: 'DELETE_FAILED'
                                });
                            });
                            break;
                        }
                    case 'edit_notification_type':
                        {
                            validationResult = validationFactory.validateWithResultArray();
                            if (validationResult.isValid) {
                                NotificationService.updateNotificationType($scope.notificationType).then(function () {
                                    $uibModalInstance.close();
                                }).catch(function (error) {
                                    ErrorService.showErrorMessage({
                                        code: 'UPDATE_FAILED'
                                    });
                                });
                            } else {
                                angular.forEach(validationResult.array, function (v) {
                                    ErrorService.showErrorMessage({
                                        code: v.errorMessage,
                                        message: v.errorMessage
                                    });
                                });
                            }

                            break;
                        }
                    case 'add_notification_type':
                        {
                            validationResult = validationFactory.validateWithResultArray();
                            if (validationResult.isValid) {
                                NotificationService.addNotificationType($scope.notificationType).then(function () {
                                    $uibModalInstance.close();
                                }).catch(function (error) {
                                    ErrorService.showErrorMessage({
                                        code: 'ADD_FAILED'
                                    });
                                });
                            } else {
                                angular.forEach(validationResult.array, function (v) {
                                    ErrorService.showErrorMessage({
                                        code: v.errorMessage,
                                        message: v.errorMessage
                                    });
                                });
                            }

                            break;
                        }
                }
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }

        function showModal(data) {
            var out = $uibModal.open({
                animation: true,
                templateUrl: data.templateUrl || 'add-notification-type-popup.html',
                controller: ['$scope', '$uibModalInstance', 'data', NotificationTypeController],
                size: 'md',
                resolve: {
                    data: data
                }
            });
            out.result.then(function () {
                init();
                // switch (data.key) {
                //     case 'add':
                //         {
                //             alertify.success(TranslationService.translate('ACCOUNT_WAS_CREATED', 'The account has been created.'));
                //             break;
                //         }
                //     case 'edit':
                //         {
                //             alertify.success(TranslationService.translate('CHANGES_SAVED', 'The changes have been saved.'));
                //             break;
                //         }
                //     case 'delete':
                //         {
                //             alertify.success(TranslationService.translate('ACCOUNT_WAS_DELETED', 'The account has been deleted.'));
                //             break;
                //         }
                // }
                // reloadGrid();
            }, function () {
                //modal canceled
            });
        }
        //endregion

        //region Functions $scope
        $scope.updateNotificationType = function (notificationType) {
            NotificationService.updateNotificationType(notificationType).then(function () {}).catch(function (error) {
                ErrorService.showErrorMessage({
                    code: 'UPDATE_FAILED'
                });
            });
        };


        $scope.shwowDeleteConfirmationModal = function (notificationType) {
            showModal({
                key: 'delete',
                item: notificationType,
                templateUrl: 'confirmation-modal',
                title: TranslationService.translate('CONFIRM_DELETE')
            });
        };

        $scope.showAddNotificationTypeModal = function () {
            showModal({
                key: 'add_notification_type'
            });
        };
        $scope.showEditNotificationTypeModal = function (notificationType) {
            showModal({
                key: 'edit_notification_type',
                notificationType: notificationType
            });
        };
        //endregion


        //region Event listeners

        // Unregister
        $scope.$on('$destroy', function () {

        });
        //endregion

        //region Initialization
        var init = function () {
            NotificationService.getNotificationTypes().then(function (result) {
                $scope.notificationTypes = result.data;
                $scope.runtimeData.dataLoaded = true;
            }).catch(function (error) {
                ErrorService.showErrorMessage(error);
            });
            // var department;
            // $scope.reportTypes = angular.copy(ReportedIssuesService.reportTypes);
            // $scope.adminData = UserService.getAdminData(LoginService.getUid());

        };
        init();
        //endregion
    }
]);