/**
 * Created by LIH on 7/31/2017.
 */
angular.module('city_alert').service('ReportedIssuesService', ['FirebaseService',function(FirebaseService){

    var reportTypes = [
        {
            key: 'road_hole'
        },
        {
            key: 'trash'
        },
        {
            key: 'traffic_lights'
        },
        {
            key: 'heavy_traffic'
        },
        {
            key: 'road_work'
        },
        {
            key: 'road_mark'
        },
        {
            key: 'noise'
        },
        {
            key: 'community_assets'
        },
        {
            key: 'other'
        }
    ],
        statuses = ['new', 'in_progress', 'resolved', 'unresolved'];
    return {
        reportTypes: reportTypes,
        statuses: statuses,
        getPublicReports: function(city){
            return FirebaseService.query('public_reports/','array','cityKey', city);
        },
        getPrivateReports: function(city){
            return FirebaseService.query('private_reports/','array','cityKey', city);
        }
    }
}]);