/**
 * Created by LIH on 7/31/2017.
 */
angular.module('city_alert').controller('ReportedIssuesController', ['$scope','ErrorService','ReportedIssuesService', 'UserService','LoginService','FirebaseService','GridFactory','NgMap','$uibModal','TranslationService',
    function($scope,ErrorService,ReportedIssuesService,UserService,LoginService,FirebaseService,GridFactory, NgMap, $uibModal,TranslationService){

        //region Declarations - non $scope
        //endregion

        //region Declarations - $scope
        $scope.runtimeData = {
            dataLoaded: false,
            listMode: true
        };
        $scope.filters = [{
            key: 'new',
            selected: false,
            property: 'status',
            value: 'new',
            isExclusive: false,
            index: 0
        },{
            key: 'in_progress',
            selected: false,
            property: 'status',
            value: 'in_progress',
            isExclusive: false,
            index: 1
        },{
            key: 'resolved',
            selected: false,
            property: 'status',
            value: 'resolved',
            isExclusive: false,
            index: 2
        },{
            key: 'unresolved',
            selected: false,
            property: 'status',
            value: 'unresolved',
            isExclusive: false,
            index: 3
        },{
            key: 'waiting_for_answer',
            selected: false,
            property: 'feedbackNeeded',
            value: true,
            isExclusive: true
        }];

        $scope.gridFactory = {};
        $scope.reportTypes = [];
        $scope.selectedReportType = null;

        $scope.actions = {
            change_status: function(item){
                showModal({key:'change_status', item: item});
            },
            respond: function(item){
                showModal({key:'respond', item: item});
            },
            view: function(item){
                //showModal({key:'respond', item: item});

               var publicReports = ReportedIssuesService.getPublicReports($scope.adminData.cityKey), indexOfData, privateReports;
                publicReports.$loaded().then(function(){
                    indexOfData = publicReports.$indexFor(item.$id);
                    if (indexOfData === -1 || item.createdAt !== publicReports[indexOfData].createdAt) {
                        privateReports = ReportedIssuesService.getPrivateReports($scope.adminData.cityKey);
                        privateReports.$loaded().then(function(){
                            indexOfData = privateReports.$indexFor(item.$id);
                            if (indexOfData > -1) {
                                $scope.goTo('reported-issues/private/view/'+item.$id)
                            } else {
                                ErrorService.showErrorMessage({code:'ITEM_NOT_FOUND'});
                            }
                        })
                    } else {
                        $scope.goTo('reported-issues/public/view/'+item.$id)
                    }
                });


                //$scope.goTo('reported-issues/view/'+item.$id)
            }
        };

        function ModalController($scope, $uibModalInstance, data) {
            $scope.data = data;
            $scope.runtimeData = {
                response: ''
            };
            if (data.key === 'change_status') {
                $scope.selectedStatus = data.item.status;
                $scope.statuses = ReportedIssuesService.statuses;
                $scope.selectStatus = function(status) {
                    $scope.selectedStatus = status;
                }
            }

            if (data.key === 'respond') {
                $scope.runtimeData.response = data.item.response || '';
            }

            $scope.$on('CLOSE_ALL_MODALS_NOTIFICATION', function(){
                $uibModalInstance.dismiss('cancel');
            });

            $scope.ok = function() {
                if (data.key === 'change_status') {
                    if ($scope.selectedStatus == 'resolved' || $scope.selectedStatus == 'unresolved') {
                        data.item.closedAt = firebase.database.ServerValue.TIMESTAMP;
                    } else {
                        data.item.closedAt = null;
                    }
                    data.item.status = $scope.selectedStatus;
                } else {
                    data.item.response = $scope.runtimeData.response;
                }
                $uibModalInstance.close(data.item);
            };

            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
        }
        //endregion

        //region Functions non $scope
        function showModal(data){
            var out = $uibModal.open({
                animation: true,
                templateUrl: 'action-popup.html',
                controller: ['$scope', '$uibModalInstance','data', ModalController],
                size: data.key === 'respond' ? 'md' : 'sm',
                resolve: {
                    data: data
                }
            });
            out.result.then(function(modifiedData) {
                var indexOfData,publicReports, privateReports;
                if (data.key === 'change_status' || data.key === 'respond') {
                   publicReports = ReportedIssuesService.getPublicReports($scope.adminData.cityKey);
                    publicReports.$loaded().then(function(){
                       indexOfData = publicReports.$indexFor(modifiedData.$id);
                        if (indexOfData === -1 || modifiedData.createdAt !== publicReports[indexOfData].createdAt) {
                            privateReports = ReportedIssuesService.getPrivateReports($scope.adminData.cityKey);
                            privateReports.$loaded().then(function(){
                                indexOfData = privateReports.$indexFor(modifiedData.$id);
                                if (indexOfData > -1) {
                                    privateReports.$save(modifiedData).then(function(){
                                        alertify.success(TranslationService.translate('SAVE_SUCCESSFUL', 'SAVE_SUCCESSFUL'));
                                    }, function(error){
                                        ErrorService.showErrorMessage(error);
                                    });
                                } else {
                                    ErrorService.showErrorMessage({code:'SAVE_FAIL_ITEM_NOT_FOUND'});
                                }
                            })
                        } else {
                            publicReports.$save(modifiedData).then(function(){
                                alertify.success($scope.translate('SAVE_SUCCESSFUL', 'SAVE_SUCCESSFUL'));
                            }, function(error){
                                ErrorService.showErrorMessage(error);
                            });
                        }
                    });
                }
                //CartService.addProduct(modifiedData);
                //$scope.$broadcast('REFRESH_SHOPPING_CART_EVENT');

            }, function() {
                //modal canceled
            });
        }
        //endregion

        //region Functions $scope

        $scope.toggleFilterSelection = function(filter){
          filter.selected = !filter.selected;
            $scope.gridFactory.applyFilter(filter.property, filter.selected ? filter.value : 'not_filtered', filter.isExclusive , filter.index);
        };

        $scope.selectReportType = function(type){
            $scope.selectedReportType = type;
            $scope.gridFactory.applyFilter('type', type.key === 'all_types' ? 'not_filtered' : type.key, true);
        };

        $scope.showOnMap = function(){
            var firsVisibleItem = $scope.gridFactory.visibleItems()[0];
            $scope.runtimeData.listMode = false;
            NgMap.getMap().then(function(map) {
                map.setCenter({lat: firsVisibleItem.latitude, lng: firsVisibleItem.longitude});
            });
        };

        $scope.hideMap = function(){
            $scope.runtimeData.listMode = true;
        };
        //endregion


        //region Event listeners

        // Unregister
        $scope.$on('$destroy', function () {

        });
        //endregion

        //region Initialization
        var init = function() {
            var publicReports, privateReports;
            $scope.reportTypes = angular.copy(ReportedIssuesService.reportTypes);
            $scope.reportTypes.unshift({id: 'ALL_TYPES',key: 'all_types'});
            $scope.selectedReportType = $scope.reportTypes[0];
            $scope.adminData = UserService.getAdminData(LoginService.getUid());
            $scope.adminData.$loaded().then(function(){
                publicReports = ReportedIssuesService.getPublicReports($scope.adminData.cityKey);
                privateReports = ReportedIssuesService.getPrivateReports($scope.adminData.cityKey);
                publicReports.$loaded().then(function(){
                   privateReports.$loaded().then(function(){
                       $scope.gridFactory = new GridFactory('issues_grid', publicReports.concat(privateReports));
                       $scope.runtimeData.dataLoaded = true;
                       publicReports.$watch(function(){
                           $scope.gridFactory.mergeList(publicReports.concat(privateReports));
                       });
                       privateReports.$watch(function(){
                           $scope.gridFactory.mergeList(publicReports.concat(privateReports));
                       });
                   });
                });
            }, function(error){
                ErrorService.showErrorMessage(error);
            });

            //FirebaseService.add('private_reports', {
            //    cityKey: "-KaqeYmnW6e_de4wH_dx",
            //    address: "str Sub Cetate 89, Floresti",
            //    comment: "This is a comment 4",
            //    createdAt: Number(moment().format('x')),
            //    feedbackNeeded: true,
            //    closedAt: 0,
            //    imageUrl: 'https://firebasestorage.googleapis.com/v0/b/CityAlert-901ca.appspot.com/o/report_images%2FrZ9ZVl7MNgfgflF03Qftwj4o0Us11501241937364?alt=media&token=fa51d0fd-4c72-4b40-9060-dde0269d2d06',
            //    latitude: 46.7687048,
            //    longitude: 23.7687048,
            //    status: 'new',
            //    type: 'traffic_lights',
            //    userKey: "rZ9ZVl7MNgfgflF03Qftwj4o0Us1",
            //    votesCount: 70
            //});


        };
        init();
        //endregion

    }]);