/**
 * Created by LIH on 8/4/2017.
 */
angular.module('city_alert').controller('ViewIssueCtrl', ['$scope','$routeParams','FirebaseService','ReportedIssuesService','IsPublic','ActionQueue','ErrorService','TranslationService',
    function($scope,$routeParams,FirebaseService,ReportedIssuesService,IsPublic,ActionQueue,ErrorService,TranslationService){
        //region Declarations - non $scope
        var issueID = $routeParams.issueID;
        //endregion

        //region Declarations - $scope

        $scope.statuses = ReportedIssuesService.statuses;
        $scope.selectedStatus = {};
        $scope.issue = {};
        $scope.dataLoaded = false;
        $scope.reporter = {};
        $scope.actionQueue = new ActionQueue();

        //endregion


        //region Functions non $scope
        //endregion

        //region Functions $scope
        $scope.selectStatus = function(status) {
            $scope.selectedStatus = status;
        };

        $scope.saveIssue = function(){

            function saveComplete(){
                alertify.success(TranslationService.translate('SAVE_SUCCESSFUL', 'SAVE_SUCCESSFUL'));
                $scope.goTo('/reported-issues');
            }

            var data = {status: $scope.selectedStatus};
            if ($scope.selectedStatus == 'resolved' || $scope.selectedStatus == 'unresolved') {
                data.closedAt = firebase.database.ServerValue.TIMESTAMP;
            } else {
                data.closedAt = null;
            }
            if (angular.isDefined( $scope.issue.response)) {
                data.response =  $scope.issue.response;
            }
            //$scope.actionQueue.addAction(function(){
                if (IsPublic) {
                    FirebaseService.update('public_reports/'+issueID, data).then(function(){
                        saveComplete();
                    }, function(error){
                        ErrorService.showErrorMessage(error);
                    });
                } else {
                    FirebaseService.update('private_reports/'+issueID, data).then(function(){
                        saveComplete();
                    }, function(error){
                        ErrorService.showErrorMessage(error);
                    });
                }
            //});
        };

        //endregion


        //region Event listeners

        // Unregister
        $scope.$on('$destroy', function () {
            $scope.actionQueue.clear();
        });
        //endregion

        //region Initialization
        var init = function() {
            if (IsPublic) {
                $scope.issue = FirebaseService.get('public_reports/'+issueID, 'object');
            } else {
                $scope.issue = FirebaseService.get('private_reports/'+issueID, 'object');
            }
            $scope.issue.$loaded().then(function(){
                $scope.selectedStatus = $scope.issue.status;
                $scope.dataLoaded = true;
                if (IsPublic) {
                    $scope.reporter = FirebaseService.get('users/' + $scope.issue.userKey, 'object');
                } else {
                    $scope.reporter = FirebaseService.get('admins_data/' + $scope.issue.userKey, 'object');
                }
            })
        };
        init();
        //endregion

    }]);