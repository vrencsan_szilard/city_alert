/**
 * Created by LIH on 12/17/2016.
 */
angular.module('city_alert').factory('ValidationFactory', ['$timeout',function($timeout){

    function required(value) {
        var retValue = true;
        retValue = angular.isDefined(value) && value !== null;
        if (typeof value === 'string') {
            retValue = retValue && value.length > 0;
        }
        return retValue;
    }
    function requiredArray(value) {
        var retValue = true;
        retValue = angular.isDefined(value) && value !== null && value.length > 0;
        return retValue;
    }

    function latitude(value) {
        var valid = false;
        return (parseFloat(value) >= -90 && parseFloat(value) <= 90);
    }
    function longitude(value) {
        var valid = false;
        return (parseFloat(value) >= -180 && parseFloat(value) <= 180);
    }

    function minLength(values) {
        return (values[0]+'').length >= values[1];
    }

    function email(value) {
        var regExp =/^[^@]+@[^@]+$/g;
        return regExp.test(value);
    }

    function number(value) {
        return !isNaN(value);
    }
    function integer(value) {
        var x;
        if (isNaN(value)) {
            return false;
        }
        if (x < 999999999) {
            x = parseFloat(value);
            return (x | 0) === x;
        }
        return Math.floor(value) === parseFloat(value);
    }

    function equals(values) {
        return values[0] === values[1];
    }

    function differs(values) {
        return values[0] != values[1];
    }

    function password(value) {
        var regExpUppercaseLetter = /[A-Z]+/g;
        var regExpDigit = /\d+/g;
        var regExpSpecial = /[^\w\s]+/g;
        return regExpUppercaseLetter.test(value) && regExpDigit.test(value) && regExpSpecial.test(value) && value.length >= 8;
    }

    function ValidationFactory() {
        this.rules = [];
    }

    ValidationFactory.prototype.addRule = function(value, rule, id, errorMessage){
        var ruleFunction;
        if (typeof rule === 'string') {
            switch (rule) {
                case 'required': {
                    ruleFunction = required;
                    break;
                }
                case 'email': {
                    ruleFunction = email;
                    break;
                }
                case 'number': {
                    ruleFunction = number;
                    break;
                }
                case 'equals': {
                    ruleFunction = equals;
                    break;
                }
                case 'password': {
                    ruleFunction = password;
                    break;
                }
                case 'differs': {
                    ruleFunction = differs;
                    break;
                }
                case 'integer': {
                    ruleFunction = integer;
                    break;
                }
                case 'min-length': {
                    ruleFunction = minLength;
                    break;
                }
                case 'longitude': {
                    ruleFunction = longitude;
                    break;
                }
                case 'latitude': {
                    ruleFunction = latitude;
                    break;
                }
                case 'required-array': {
                    ruleFunction = requiredArray;
                    break;
                }
            }
        } else {
            ruleFunction = rule;
        }
        this.rules.push({
            value: value,
            rule: ruleFunction,
            id: id,
            errorMessage: errorMessage
        });

        //$('#'+id).change(function(){
        //    $timeout(function(){
        //        $('#'+id).removeClass('invalid');
        //    }, 0);
        //
        //});
    };

    ValidationFactory.prototype.clearAll = function () {
        angular.forEach(this.rules, function (ruleObject) {

            if (angular.isDefined(ruleObject.id)) {
                $timeout(function () {
                    $('#' + ruleObject.id).removeClass('invalid');
                }, 0);
            }
        });
    };
    ValidationFactory.prototype.validateWithResultObject = function(){
        var isValid = true, areAllValid = true, i= 0, ruleObject, resultObject = {};

        angular.forEach(this.rules, function(ruleObject){
           
            if (angular.isDefined(ruleObject.id)) {
                $timeout(function () {
                    $('#' + ruleObject.id).removeClass('invalid');
                }, 0);
            }
           
            if (ruleObject.rule === equals || ruleObject.rule === differs || ruleObject.rule === minLength || ruleObject.rule === requiredArray) {
                isValid = ruleObject.rule(ruleObject.value);
            } else if (Array.isArray(ruleObject.value)) {
                angular.forEach(ruleObject.value, function(ruleValue){
                    isValid = isValid && ruleObject.rule(ruleValue);
                });
            }  else {
                isValid = ruleObject.rule(ruleObject.value);
            }
            if (!isValid) {
            //    resultArray.push(ruleObject);
                resultObject[ruleObject.id] = ruleObject.errorMessage;
                if (angular.isDefined(ruleObject.id)) {
                    $timeout(function () {
                        $('#' + ruleObject.id).addClass('invalid');
                    }, 0);
                }
            }
            areAllValid = areAllValid && isValid;
        });

        return {isValid: areAllValid, results: resultObject};        
    };
    ValidationFactory.prototype.validateWithResultArray = function(){
        var isValid = true, areAllValid = true, i= 0, ruleObject, resultArray = [];

        angular.forEach(this.rules, function(ruleObject){
           
            // if (angular.isDefined(ruleObject.id)) {
            //     $timeout(function () {
            //         $('#' + ruleObject.id).removeClass('invalid');
            //     }, 0);
            // }
           
            if (ruleObject.rule === equals || ruleObject.rule === differs || ruleObject.rule === minLength || ruleObject.rule === requiredArray) {
                isValid = ruleObject.rule(ruleObject.value);
            } else if (Array.isArray(ruleObject.value)) {
                angular.forEach(ruleObject.value, function(ruleValue){
                    isValid = isValid && ruleObject.rule(ruleValue);
                });
            }  else {
                isValid = ruleObject.rule(ruleObject.value);
            }
            if (!isValid) {
               resultArray.push(ruleObject);
                // resultObject[ruleObject.id] = ruleObject.errorMessage;
                // if (angular.isDefined(ruleObject.id)) {
                //     $timeout(function () {
                //         $('#' + ruleObject.id).addClass('invalid');
                //     }, 0);
                // }
            }
            areAllValid = areAllValid && isValid;
        });

        return {isValid: areAllValid, array: resultArray};        
    };

    ValidationFactory.prototype.validate = function(){
        var isValid = true, areAllValid = true, i= 0, ruleObject;
        angular.forEach(this.rules, function(ruleObject){
            if (angular.isDefined(ruleObject.id)) {
                $timeout(function () {
                    $('#' + ruleObject.id).removeClass('invalid');
                }, 0);
            }
        });

        angular.forEach(this.rules, function(ruleObject){
            if (ruleObject.rule === equals || ruleObject.rule === differs || ruleObject.rule === minLength) {
                isValid = ruleObject.rule(ruleObject.value);
            } else if (Array.isArray(ruleObject.value)) {
                angular.forEach(ruleObject.value, function(ruleValue){
                    isValid = isValid && ruleObject.rule(ruleValue);
                });
            }  else {
                isValid = ruleObject.rule(ruleObject.value);
            }
            if (!isValid) {
                if (angular.isDefined(ruleObject.id)) {
                    $timeout(function () {
                        $('#' + ruleObject.id).addClass('invalid');
                    }, 0);
                }
            }
            areAllValid = areAllValid && isValid;
        });

        return areAllValid;
    };
    return ValidationFactory;
}]);