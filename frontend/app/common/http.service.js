angular.module('city_alert').service('HttpService', ['$http', 'CONFIG', 'RuntimeService',

function ($http, CONFIG, RuntimeService) {
    function serializeData( data ) {
        // If this is not an object, defer to native stringification.
        if ( ! angular.isObject( data ) ) {
            return( ( data == null ) ? "" : data.toString() );
        }
        var buffer = [];
        // Serialize each key in the object.
        for ( var name in data ) {
            if ( ! data.hasOwnProperty( name ) ) {
                continue;
            }
            var value = data[ name ];
            buffer.push(
                encodeURIComponent( name ) +
                "=" +
                encodeURIComponent( ( value == null ) ? "" : value )
            );
        }
        // Serialize the buffer and clean it up for transportation.
        var source = buffer
            .join( "&" )
            .replace( /%20/g, "+" )
        ;
        return( source );
    }
    
    return {
        handle: function (method, url, data, params) {
            // data.adminType = LoginService.getCurrentAdminType();
            var token = RuntimeService.get('token'),
                http = {
                    method          : method,
                    url             : CONFIG.api + url,
                    data            : data,
                    params          : params,
                    withCredentials : true
                };
            if (angular.isDefined(token) && token !== null && token.length > 0) {
                http.headers = {token: token};
            }

            return $http(http);
        },
        formPost: function(url, data){
            return $http({
                method          : 'post',
                url             :  CONFIG.api + url,
                withCredentials : true,
                headers         : {"Content-type": "application/x-www-form-urlencoded; charset=utf-8"},
                data            : serializeData(data)
            });
        }
    };
}]);