/**
 * Created by LIH on 8/11/2017.
 */
angular.module('city_alert').directive('removeInvalid', function(){
    return {

        link: function(scope, element, attrs, modelCtrl) {
            $(element).focus(function(){
                var list = attrs.removeInvalid.split(',');
                angular.forEach(list, function(id){
                    $('#'+id).removeClass('invalid');
                });
      
            });
           
        }
    };
});