/**
 * Created by LIH on 7/18/2017.
 */
angular.module('city_alert').controller('MainCtrl', ['$scope','$rootScope','TranslationService','$location','CONFIG', 'LoginService','StorageService','$window',
    function($scope, $rootScope,TranslationService,$location,CONFIG, LoginService,StorageService,$window){
        //region Declarations - non $scope
        //endregion

        //region Declarations - $scope
        $rootScope.showMenu = false;
        $scope.conf = CONFIG;
        $rootScope.dataIsLoading = false;
        $rootScope.isAppAdmin = false;
        $rootScope.hideFooter = false;
        //endregion

        //region Functions non $scope
        //endregion

        //region Functions $scope
        $rootScope.translate = function(id, defaultValue, isHTML){
            return TranslationService.translate(id, defaultValue, isHTML);
        };

        $rootScope.goTo = function(path) {
            $location.path(path);
        };

        $rootScope.selectLanguage = function(langCode){
            StorageService.setItem('city_alert_LANGUAGE', langCode);
            $rootScope.selectedLanguage = langCode;
            //$scope.$apply(function(){
            //    console.log('lang selected');
            //$route.reload();
            $window.location.reload();
            //});
        };

        $rootScope.isDev = function(){
            return CONFIG.dev;
        };
        //endregion


        //region Event listeners

        // Unregister
        $scope.$on('$destroy', function () {

        });
        //endregion

        //region Initialization
        var init = function() {
           var tempLanguage = StorageService.getItem('city_alert_LANGUAGE');
            $rootScope.selectedLanguage =  tempLanguage == null ? CONFIG.defaultLanguage : tempLanguage;
            TranslationService.init();
        };
        init();
        //endregion
    }]);