/**
 * Created by LIH on 7/23/2017.
 */
angular.module('city_alert').service('StorageService', ['Encoder',function(Encoder){

    var storageAvailable = false;
    try {
        var x = '__storage_test__';
        window.localStorage.setItem(x, x);
        window.localStorage.removeItem(x);
        storageAvailable = true;
    }
    catch(e) {}

    return {
        //uploadPropertyFile: function(blob, propertyCategory, propertyId, imageIndex){
        //    var storageRef = firebase.storage().ref("properties/"+propertyCategory+'/'+propertyId+'/'+imageIndex);
        //    var storage = $firebaseStorage(storageRef);
        //    return storage.$put(blob);
        //},
        //getImageURL: function(path){
        //    var storageRef = firebase.storage().ref(path);
        //    var storage = $firebaseStorage(storageRef);
        //    return storage.$getDownloadURL();
        //},
        //deleteImage: function(path){
        //    var storageRef = firebase.storage().ref(path);
        //    var storage = $firebaseStorage(storageRef);
        //    return storage.$delete();
        //},
        //uploadImage: function(path, blob){
        //    var storageRef = firebase.storage().ref(path);
        //    var storage = $firebaseStorage(storageRef);
        //    var newMetadata = {
        //        contentType: 'image/png',
        //        cacheControl: 'private, max-age=20000'
        //    };
        //    return storage.$put(blob,newMetadata);
        //},
        //updateMetaData: function(path) {
        //    //TODO TEST METADATA UPDATE
        //    var storageRef = firebase.storage().ref(path);
        //    // Create file metadata to update
        //    var newMetadata = {
        //        contentType: 'image/jpeg',
        //        cacheControl: 'public,max-age=20000'
        //    };
        //    // Update metadata properties
        //    return  storageRef.updateMetadata(newMetadata);
        //},
        setItem: function(key, item){
            if (storageAvailable) {
                localStorage.setItem(key, Encoder.encode(item));
            }
        },
        getItem: function(key) {
            var temp = localStorage.getItem(key);
            if (storageAvailable && temp != null && angular.isDefined(temp)) {
                return Encoder.decode(localStorage.getItem(key));
            } else {
                return null;
            }
        },
        removeItem: function (key) {
            if (storageAvailable) {
                localStorage.removeItem(key);
            }
        }
    }
}]);