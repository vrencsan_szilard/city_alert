/**
 * Created by LIH on 7/23/2017.
 */
angular.module('city_alert').service('Encoder', function(){
    return {
        encode: function(obj){
            var rawStr = angular.isString(obj) ? obj : JSON.stringify(obj);
            var wordArray = CryptoJS.enc.Utf8.parse(rawStr);
            return CryptoJS.enc.Hex.stringify(wordArray);
        },
        decode: function(encodedStr){
            if (encodedStr == null) {
                return null;
            }
            var arr = CryptoJS.enc.Hex.parse(encodedStr);
            var decodedStr = arr.toString(CryptoJS.enc.Utf8);
            return decodedStr;

        }
    }
});