/**
 * Created by LIH on 8/11/2017.
 */
angular.module('city_alert').directive('hoursOnly', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = "";
                if (typeof(inputValue) !== 'undefined') {
                    transformedInput = inputValue;
                }
                if (transformedInput.length > 0) {
                    transformedInput = transformedInput.replace(/[^\d]/g, '');

                    if (transformedInput.length > 2) {
                        transformedInput = transformedInput.substr(0,2);
                    }

                    if (parseInt(transformedInput) > 24) {
                        transformedInput = 24;
                    }

                    if (transformedInput!==inputValue) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$commitViewValue();
                        modelCtrl.$render();
                    }
                }

                return transformedInput;
            });
        }
    };
});