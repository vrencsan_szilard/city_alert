/**
 * Created by LIH on 7/26/2017.
 */
angular.module('city_alert').service('ErrorService', ['Auth', '$window', '$rootScope', function (Auth, $window, $rootScope) {

    function getErrorField(error, fieldName) {
        var field = null;
        if (angular.isDefined(error) && error !== null) {
            if (angular.isDefined(error[fieldName])) {
                field = error[fieldName];
            } else {
                if (angular.isDefined(error.data) && error.data !== null && angular.isDefined(error.data[fieldName]) && error.data[fieldName] !== null) {
                    field = error.data[fieldName];
                }
            }
        }
        return field;
    }

    return {
        showErrorMessage: function (error) {
            var t,
                code    = getErrorField(error, 'code'),
                message = getErrorField(error, 'message'),
                status  = error.status;
            if (status === 401) {
                $rootScope.goTo('/login');
                return;
            }
            if (angular.isDefined(code) && code !== null) {
                t = $rootScope.translate(code, message);
                if (code === t) {
                    alertify.error($rootScope.translate('UNKNOWN_ERROR_CODE', '').replace('{{ERROR_CODE}}', code));
                } else {
                    alertify.error(t);
                }
            } else {
                alertify.error($rootScope.translate('UNKNOWN_ERROR', ''));
            }
        }
    };
}]);