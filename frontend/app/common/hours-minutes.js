/**
 * Created by LIH on 8/11/2017.
 */
angular.module('city_alert').directive('hoursMinutes', function(){
    function getClosestValidMinute(minute, validMinutes){
        var closestTo  = validMinutes[0],
            diff        = Number.MAX_SAFE_INTEGER,
            currentDiff;
        angular.forEach(validMinutes, function(validMinute){
            currentDiff = Math.abs(parseInt(validMinute) - minute);
            if (currentDiff < diff) {
                diff = currentDiff;
                closestTo = validMinute;
            }
        });
        return closestTo;
    }
    function validate(inputValue, total) {
        var transformedInput = "", 
            temp             = [],
            validMinutes     = ['00','15','30','45'],
            hasSeparator     = false,
            firstChar        = '';
        if (typeof(inputValue) !== 'undefined' & inputValue !== null) {
            transformedInput = inputValue;
        }
        hasSeparator = transformedInput.indexOf(':') > -1;
        if (!hasSeparator && transformedInput.length > 2) {
            transformedInput = transformedInput.slice(0, 2) + ':' + transformedInput.slice(2);
            hasSeparator = true;
        }
        if (!hasSeparator) {
            transformedInput = transformedInput.replace(/[^\d]/g, '');
            if (total) {
                if (parseInt(transformedInput) > 23) {
                    transformedInput = '23';
                }
                transformedInput = transformedInput +':00';
            }
        }
        if (total && transformedInput.charAt(0) === ':') {
            transformedInput = '00'+transformedInput;
        }
        if (hasSeparator) {
            temp = transformedInput.split(':');
            temp[0] = temp[0].replace(/[^\d]/g, '');
            temp[1] = temp[1].replace(/[^\d]/g, '');
            if (temp[0].length > 2) {
                temp[0] = temp[0].substr(0,2);
            }
            if (temp[1].length > 2) {
                temp[1] = temp[1].substr(0,2);
            }
            if (parseInt(temp[0]) > 23) {
                temp[0] = 23;
            }
            if (temp[1].length > 1) {
                temp[1] = getClosestValidMinute(temp[1], validMinutes);
            } else {
                if (total) {
                    temp[1] = getClosestValidMinute(temp[1], validMinutes);
                }
            }
            transformedInput = temp.join(':');
        } 

        
        return transformedInput;
    }
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            $(element).blur(function(){
                var transformedInput = validate(modelCtrl.$viewValue, true);
                if (transformedInput!==modelCtrl.$viewValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
            });
            modelCtrl.$parsers.push(function(inputValue){
                var transformedInput = validate(inputValue);
                if (transformedInput!==inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }    
                return transformedInput; 
            });
        }
    };
});