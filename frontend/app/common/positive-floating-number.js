/**
 * Created by LIH on 8/11/2017.
 */
angular.module('city_alert').directive('positiveFloatingNumber', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {

            var maxAllowedDecimals = parseInt(attrs.positiveFloatingNumber);

            var removeExtraDecimals = function(value) {
                var splitNumber;
                if (value.length > 0) {
                    splitNumber = value.split('.');

                    if (splitNumber.length > 2) {
                        splitNumber = [splitNumber[0], splitNumber[1]];
                        value = splitNumber[0] + "." + splitNumber[1];
                    }
                    if (splitNumber.length > 1 && splitNumber[1].length > 1) {
                        value = splitNumber[0] + '.' + splitNumber[1].substring(0, maxAllowedDecimals);
                    }
                }
                return value;
            };

            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = "";
                if (typeof(inputValue) !== 'undefined') {
                    transformedInput = inputValue;
                }
                if (transformedInput.length > 0) {
                    transformedInput = inputValue.replace(',', '.');
                    transformedInput = transformedInput.replace(/[^\d.]/g, '');
                    transformedInput = removeExtraDecimals(transformedInput);

                    if (transformedInput!==inputValue) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }
                }

                return transformedInput;
            });
        }
    };
});