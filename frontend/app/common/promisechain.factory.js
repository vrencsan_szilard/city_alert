/**
 * Created by LIH on 7/31/2017.
 */
angular.module('city_alert').factory('PromiseChain', ['$q',function($q){


    function log(id, obj, message, shouldLog){
        if (shouldLog) {
            console.log('['+id+'] '+
                (obj !== null ? obj.description : '') +
                ' '+message);
        }
    }

    function PromiseChain(shouldLog, id) {
        this.promises = [];
        this.shouldLog = angular.isUndefined(shouldLog) ? false : shouldLog;
        this.id = angular.isUndefined(id) ? (Math.random() + '').replace('.','') : id;
    }

    /**
     * The promise must be a function if you want to flatten in chain, and a promise if you want to flatten at once
     * @param promise
     * @param description
     * @constructor
     */
    PromiseChain.prototype.chain = function(p, description){
        var obj = {promise: p, description: description};
        log(this.id, obj, 'chained', this.shouldLog);
        this.promises.push(obj);
    };

    /**
     * Promises should be functions returning a promise
     * @returns {*}
     */
    PromiseChain.prototype.flattenInChain = function(){
        var deferred = $q.defer(), resolvedPromiseIndex = 0, n=this.promises.length, results=[], that = this;
        console.log('promises: ', this.promises);
        function resolvePromise(index){
            if (angular.isDefined(that.promises[index])) {
                console.log('running promise', that.promises[index].description);
                that.promises[index].promise().then(function (result) {
                    console.log('success', that.promises[index].description);
                    results[index] = result;
                    resolvePromise(index + 1);
                }, function (error) {
                    console.log('fail', that.promises[index].description);
                    deferred.reject(error);
                });
            } else {
                deferred.resolve(results);
            }
        }

        if (n > 0) {
            resolvePromise(0);
        } else {
            deferred.resolve(results);
        }


        return deferred.promise;
    };

    PromiseChain.prototype.flattenAllAtOnce = function(){
        var i= 0, n=this.promises.length, deferred = $q.defer(), failed, results=[], resolvedNr = 0, that = this;

        if (n > 0) {
            angular.forEach(this.promises, function(obj, promiseIndex){
                log(that.id, obj, 'run', this.shouldLog);
                obj.promise.then(function success(result){
                    log(that.id, obj, 'success', that.shouldLog);
                    results[promiseIndex] = result;
                    resolvedNr++;
                    if (resolvedNr === n) {
                        deferred.resolve(results);
                    }
                }, function error(error){
                    log(that.id, obj, 'fail ' + error, that.shouldLog);
                    deferred.reject(error);
                });
            });
        } else {
            deferred.resolve(results);
        }

        return deferred.promise;
    };

    return PromiseChain;

}]);