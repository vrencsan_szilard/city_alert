/**
 * Created by LIH on 8/1/2017.
 */

angular.module('city_alert').factory('ActionQueue', ['$interval',function ($interval) {
    /**
     * Constructor, with class name
     */
    function ActionQueue(time) {
        this.queue = [];
        this.time = typeof (time) !== 'undefined' ? time: 1000;
        var that = this;
        this.interval = $interval(function(){
            that.runOnlyLastAction(true);
        }, this.time);
    }

    /**
     * Add a delayed action to the array and reset the interval
     * @param action
     */
    ActionQueue.prototype.addAction = function(action) {
        this.queue.push({action: action, wasDone: false});
        //the first action will not be delayed, but will remain in the list so the next one would be delayed
        if (this.queue.length === 1) {
            this.runOnlyLastAction(false);
        } else {
            this.resetInterval();
        }
    };

    /**
     * Disregard the whole list and run only the last action
     * @param clearAfter
     */
    ActionQueue.prototype.runOnlyLastAction = function(clearAfter) {
        var next;
        if (this.queue.length > 0) {
            next =  this.queue[this.queue.length - 1];
            if (!next.wasDone) {
                next.action();
                next.wasDone = true;
            }
            if (clearAfter) {
                this.queue = [];
            }
        }
    };
    /**
     * Don't forget to call this if you are using actionQueue
     * example:
     $scope.$on('$destroy', function(){
        $scope.actionQueue.clear();
     });
     */

    ActionQueue.prototype.clear = function() {
        this.queue = [];
        $interval.cancel(this.interval);
        this.interval = undefined;
    };

    ActionQueue.prototype.reset = function() {
        this.clear();
        var that = this;
        this.interval = $interval(function(){
            that.runOnlyLastAction(true);
        }, this.time);
    };

    /**
     * Clears the current interval and starts a new one
     */
    ActionQueue.prototype.resetInterval = function() {
        $interval.cancel(this.interval);
        this.interval = undefined;
        var that = this;
        this.interval = $interval(function(){
            that.runOnlyLastAction(true);
        }, this.time);
    };

    return ActionQueue;
}]);
