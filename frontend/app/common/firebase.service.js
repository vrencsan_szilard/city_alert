/**
 * Created by LIH on 7/27/2017.
 */
angular.module('city_alert').service('FirebaseService', ['$firebaseObject','$firebaseArray','$q',function($firebaseObject, $firebaseArray,$q){
    var variables = {};
    function load(path, type) {
        if (angular.isUndefined(variables[path])) {
            variables[path] = type(firebase.database().ref(path));
        }
        return variables[path];
    }
    function loadWithFilter(path, type, childID, childValue) {
        if (angular.isUndefined(variables[path])) {
            variables[path] = type(firebase.database().ref(path).orderByChild(childID).equalTo(childValue));
        }
        return variables[path];
    }
    var types = {
        'object': $firebaseObject,
        'array': $firebaseArray
    };
    return {
        /**
         * Returns the firebaseObject or the firebaseArray on the given path
         * possible values for type can be either 'object', or 'array'
         * @param path
         * @param type
         * @returns {*}
         */
        get: function(path, type) {
            return load(path, types[type]);
        },
        /**
         * Returns the firebaseObject or the firebaseArray on the given path filtered by childID and childValue
         * possible values for type can be either 'object', or 'array'
         * @param path
         * @param type
         * @param childID
         * @param childValue
         * @returns {*}
         */
        query: function(path, type, childID, childValue) {
            var id = path + 'ck'+childID + 'cv' + childValue;
            return loadWithFilter(path, types[type], childID, childValue);
        },
        /**
         * Pushes a new element into an array
         * @param path
         * @param data
         */
        add: function(path, data){
            return this.get(path, 'array').$add(data);
        },
        /**
         * Updates an object with the given data
         * @param path
         * @param data
         * @returns {*}
         */
        update: function(path, data){
            var fbObj = load(path, types['object']), deferred = $q.defer();
            fbObj.$loaded().then(function(){
                angular.forEach(data, function(value, key){
                    fbObj[key] = value;
                });
                fbObj.$save().then(function(){
                    deferred.resolve();
                }, function(err){
                    deferred.reject(err);
                });
            }, function(error){
                deferred.reject(error);
            });
            return deferred.promise;
        },
        remove: function(path){
            var fbObj = load(path, types['object']);
            return fbObj.$remove();
        },
        reset: function(){
            variables = {};
        },
        timestamp: firebase.database.ServerValue.TIMESTAMP
     };
}]);