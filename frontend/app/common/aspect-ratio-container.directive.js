/**
 * Created by LIH on 7/24/2017.
 */
angular.module('city_alert').directive('aspectRatioContainer', function(){
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            ratio: '@'
        },
        template:
        '<div class="aspect-ratio-container" ng-style="{\'padding-top\':padding}"> ' +
        '<div class="aspect-ratio-body">' +
        '<ng-transclude>' +
        '</ng-transclude>' +
        '</div>' +
        ' </div>',
        controller: ['$scope', function($scope, attr) {
            var ratioArray = $scope.ratio.split(':');
            $scope.padding = (parseFloat(ratioArray[1]) * 100 / parseFloat(ratioArray[0])) + '%';
        }]
    };
});