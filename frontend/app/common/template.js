/**
 * Created by LIH on 7/24/2017.
 */
angular.module('city_alert').controller('ControllerNameCtrl', ['$scope','ErrorService', function($scope,ErrorService){

    //region Declarations - non $scope
    //endregion

    //region Declarations - $scope
    //endregion


    //region Functions non $scope
    //endregion

    //region Functions $scope
    //endregion


    //region Event listeners

    // Unregister
    $scope.$on('$destroy', function () {

    });
    //endregion

    //region Initialization
    var init = function() {
    };
    init();
    //endregion

}]);