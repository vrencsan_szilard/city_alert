angular.module('city_alert').service('RuntimeService', function () {
    var storage = {};
    return {
        set: function(key, value){
            storage[key] = value;
        },
        get: function(key){
            return storage[key];
        },
        delete: function(key){
            if (angular.isDefined(storage[key])) {
                delete storage[key];
            }
        }
    };
});