angular.module('city_alert').service('DistrictService', ['HttpService',function(HttpService){
    return {
        getDistricts: function(){
            return HttpService.handle('GET','/districts');
        },
        getDistrict: function(latitude, longitude){
            return HttpService.handle('POST', '/district', {latitude: latitude, longitude: longitude});
        }
    };
}]);