/**
 * Created by LIH on 7/23/2017.
 */
angular.module('city_alert').service('TranslationService', ['$resource','CONFIG','StorageService','ErrorService','$sce',function($resource, CONFIG, StorageService,ErrorService,$sce){
    var translations  = null, languageInitialized = false, selectedLanguage = CONFIG.defaultLanguage;
    function init(language){
        languageInitialized = false;
        var tempLanguage;
        if (!angular.isDate(language)) {
            tempLanguage = StorageService.getItem('city_alert_LANGUAGE');
            language = tempLanguage == null ? CONFIG.defaultLanguage : tempLanguage;
            selectedLanguage = language;
            languageInitialized = true;
        }
        var languageFilePath = 'translations/' + language + '.json';
        $resource(languageFilePath +'?v='+CONFIG.versionNumber).get(function (data) {
            translations = data;
            languageInitialized[language] = true;
        });
    }
   return {
       init: init,
       translate: function(id, defaultValue, isHTML){
           var temp = '', retValue = defaultValue && (defaultValue + '').length > 0 ? defaultValue : id, uppercaseId = id.toUpperCase();
           if (languageInitialized && translations !== null) {
               try {
                   temp = translations[id];
                   if (!angular.isDefined(temp) || temp === null) {
                       temp = translations[uppercaseId];
                   }
                   if (angular.isDefined(temp) && temp != null) {
                       retValue = temp;
                   } else {
                       console.error('Translation key not found ', id);
                       ErrorService.showErrorMessage(translations['UNKNOWN_ERROR_CODE'].replace('{{ERROR_CODE}}', id))
                   }
               } catch (error) {
                   console.error('Translation issue', error);
               }
               if (isHTML) {
                   retValue = $sce.trustAsHtml(temp);
               }
           }
           return retValue;
       }
   }
}]);