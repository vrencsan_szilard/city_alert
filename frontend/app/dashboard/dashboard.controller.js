/**
 * Created by LIH on 7/24/2017.
 */
angular.module('city_alert').controller('DashboardCtrl', ['$scope','$rootScope','LoginService', function($scope,$rootScope,LoginService){

    //region Declarations - non $scope
    //endregion

    //region Declarations - $scope
    $scope.runtimeData = {
        dataLoaded: false
    };
    //endregion


    //region Functions non $scope
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    //endregion

    //region Functions $scope
    $scope.click = function(a,b){
        console.log('clicked in dashboard',a,b);
    };
    //endregion


    //region Event listeners

    // Unregister
    $scope.$on('$destroy', function () {

    });
    //endregion

    //region Initialization
    function init(){
        
    }
    init();
    //endregion
}]);