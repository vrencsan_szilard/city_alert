/**
 * Created by LIH on 7/24/2017.
 */
angular.module('city_alert').controller('MenuCtrl', ['$scope','MenuService','LoginService','ActionQueue', function($scope,MenuService, LoginService,ActionQueue){

    //region Declarations - non $scope
    var paths = {
        DASHBOARD: '/dashboard',
        SETTINGS: '/settings',
        // REPORTED_ISSUES: '/reported-issues',
        NOTIFY: '/notify',
        EMAIL: '/email-settings',
        CITY_DATA: '/city-data',
        TERMS: '/edit-terms',
        DB_INFO: '/db-info',
        LOG: '/log',
        USER_ACCOUNTS: '/user-accounts',

    };
    var actionQueue = new ActionQueue(500);
    //endregion

    //region Declarations - $scope
    $scope.menuService = MenuService;
    $scope.adminTypes = LoginService.adminTypes;
    //endregion


    //region Functions non $scope
    //endregion

    //region Functions $scope
    $scope.select = function (id) {
        if (id !== $scope.menuService.MENU_POINTS.LOGOUT) {
            $scope.menuService.select(id);
            // function go(){
                $scope.goTo(paths[id]);
            // }
            // actionQueue.addAction(go);
        } else {
            // LoginService.logout();
            // FirebaseService.reset();
            LoginService.logout().then(function () {
                $scope.goTo('/login');
            }, function () {
                $scope.goTo('/login');
            });
            // });
        }
    };

    $scope.currentAdminType = function(){
        return LoginService.getCurrentAdminType();
    };
    //endregion


    //region Event listeners

    // Unregister
    $scope.$on('$destroy', function () {

    });
    //endregion

    //region Initialization
    var init = function() {
    };
    init();
    //endregion

}]);