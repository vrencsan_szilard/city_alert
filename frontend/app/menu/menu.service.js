/**
 * Created by LIH on 7/24/2017.
 */
angular.module('city_alert').service('MenuService', function(){
    var selectedMenu = 0;
    return {
        MENU_POINTS : {
            DASHBOARD: 'DASHBOARD',
            SETTINGS: 'SETTINGS',
            // REPORTED_ISSUES: 'REPORTED_ISSUES',
            NOTIFY: 'NOTIFY',
            CITY_DATA: 'CITY_DATA',
            EMAIL: 'EMAIL',
            USER_ACCOUNTS: 'USER_ACCOUNTS',
            TERMS: 'TERMS',
            DB_INFO: 'DB_INFO',
            LOG: 'LOG',
            LOGOUT: 'LOGOUT'
        },
        select: function(id){
            selectedMenu = id;
        },
        isSelected: function(id) {
            return selectedMenu === id;
        },
        getSelected: function () {
            return selectedMenu;
        }
    }
});