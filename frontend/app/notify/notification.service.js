angular.module('city_alert').service('NotificationService', ['HttpService',function(HttpService){
    return {
        getNotificationTypes: function(){
            return HttpService.handle('GET','/notification/types');
        },
        updateNotificationType: function(notificationType){
            return HttpService.handle('PUT','/notification/type/'+notificationType.id, notificationType);
        },
        deleteNotificationType: function(notificationType){
            return HttpService.handle('DELETE','/notification/type/'+notificationType.id);
        },
        addNotificationType: function(notificationType){
            return HttpService.handle('POST','/notification/type/',notificationType);
        },
        addNotification: function(notification){          
            return HttpService.handle('POST', '/notification', notification);
        },
        updateNotification: function(notification){
            return HttpService.handle('PUT', '/notification/'+notification.id, notification);
        },
        getNotifications: function(){
            return HttpService.handle('GET', '/grid/notifications');
        },
        deleteNotification: function(notification){
            return HttpService.handle('DELETE', '/notification/'+notification.id);
        },
        deleteNotifications: function(notifications){
            return HttpService.handle('POST', '/notifications/delete', {notifications:notifications});
        },
        getNotification: function(notificationID) {
            return HttpService.handle('GET', '/notification/'+notificationID);
        },
        getAddress: function(lat, lng){
            return HttpService.handle('GET', '/address/'+lat+'/'+lng);
        }
    };
}]);