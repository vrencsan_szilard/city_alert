/**
 * Created by LIH on 8/5/2017.
 */
angular.module('city_alert').controller('EditNotifyController', ['$scope', 'ErrorService', '$routeParams', 'mode','NgMap',
    'TranslationService', 'ActionQueue', 'LoginService', 'ValidationFactory', 'NotificationFactory', 'NotificationService', 'DistrictService','$uibModal',
    function ($scope, ErrorService, $routeParams, mode, NgMap, TranslationService, ActionQueue, LoginService, ValidationFactory, NotificationFactory, NotificationService, DistrictService, $uibModal) {

        //region Declarations - non $scope
        var notifyID = $routeParams.notifyID;
        //endregion

        //region Declarations - $scope
        $scope.notification = {};
        $scope.dataLoaded = false;
        $scope.mode = mode;
        $scope.format = TranslationService.translate('DATE_FORMAT', 'MM.DD.YYYY').replace(/Y/g, 'y').replace(/D/g, 'd');
        $scope.altInputFormats = ['M!/d!/yyyy'];
        $scope.actionQueue = new ActionQueue();
        $scope.notificationTypeList = [];
        $scope.districtList = [];
        // $scope.selectedDistricts = [];
        $scope.runtimeData = {
            dt: null,
            shouldSendNow: false,
            time: '',
            fromDate: null,
            fromTime: null,
            untilDate: null,
            untilTime: null,
            processedNotificationTypeList: {},
            processedDistricts: {},
            errorMessages: {},
            locationErrorMessages: {},
            isCityNotification: false
        };
        //endregion

        //region Functions non $scope
        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        function validateLocations() {
            if (!$scope.runtimeData.isCityNotification) {
                var validationFactory = new ValidationFactory();
                angular.forEach($scope.notification.locations, function (location, index) {
                    validationFactory.addRule(location.longitude, 'required', 'longitude-' + index, TranslationService.translate('LONGITUDE_VALIDATION', ''));
                    validationFactory.addRule(location.longitude, 'longitude', 'longitude-' + index, TranslationService.translate('LONGITUDE_VALIDATION', ''));
                    validationFactory.addRule(location.latitude, 'required', 'latitude-' + index, TranslationService.translate('LATITUDE_VALIDATION', ''));
                    validationFactory.addRule(location.latitude, 'latitude', 'latitude-' + index, TranslationService.translate('LATITUDE_VALIDATION', ''));
                    validationFactory.addRule(location.districtID, 'required', 'district-' + index, TranslationService.translate('SELECT_DISTRICT', ''));
                });

                validationObject = validationFactory.validateWithResultObject();
                $scope.runtimeData.locationErrorMessages = validationObject.results;
                return validationObject.isValid;
            } else {
                return true;
            }
        }

        function isLocationValid(location) {
            var validationFactory = new ValidationFactory();
            validationFactory.addRule(location.longitude, 'required');
            validationFactory.addRule(location.longitude, 'longitude');
            validationFactory.addRule(location.latitude, 'required');
            validationFactory.addRule(location.latitude, 'latitude');
            validationObject = validationFactory.validateWithResultObject();
            return validationObject.isValid;
        }

        function validate() {
            var validationFactory, validationObject = true,
                hourArrays = {};
            validationFactory = new ValidationFactory();
            if (!$scope.runtimeData.shouldSendNow) {
                validationFactory.addRule($scope.runtimeData.dt, 'required', 'date', TranslationService.translate('DATE_PLACEHOLDER', ''));
                validationFactory.addRule($scope.runtimeData.time, 'required', 'time', 'Ex: 23:15');
            } else {
                $scope.runtimeData.dt = new Date();
                $scope.runtimeData.time = moment().format('HH:mm');
            }

            validationFactory.addRule($scope.runtimeData.fromDate, 'required', 'from-date', TranslationService.translate('DATE_PLACEHOLDER', ''));
            validationFactory.addRule($scope.runtimeData.fromTime, 'required', 'from-time', 'Ex: 23:15');
            validationFactory.addRule($scope.runtimeData.untilDate, 'required', 'until-date', TranslationService.translate('DATE_PLACEHOLDER', ''));
            validationFactory.addRule($scope.runtimeData.untilTime, 'required', 'until-time', 'Ex: 23:15');
            validationFactory.addRule($scope.notification.typeID, 'required', 'type', TranslationService.translate('SELECT_A_TYPE', ''));
            validationFactory.addRule($scope.notification.subject, 'required', 'subject', TranslationService.translate('REQUIRED', ''));
            validationFactory.addRule($scope.notification.description, 'required', 'description', TranslationService.translate('REQUIRED', ''));
            validationObject = validationFactory.validateWithResultObject();
            $scope.runtimeData.errorMessages = validationObject.results;
            if (!validationObject.isValid) {
                ErrorService.showErrorMessage({
                    code: 'ALL_FIELDS_ARE_REQUIRED'
                });
            } else {
                hourArrays.time = $scope.runtimeData.time.split(':');
                hourArrays.untilTime = $scope.runtimeData.untilTime.split(':');
                hourArrays.fromTime = $scope.runtimeData.fromTime.split(':');
                if (moment($scope.runtimeData.dt).hour(hourArrays.time[0]).minutes(hourArrays.time[1]).seconds(0).isSameOrAfter(moment($scope.runtimeData.untilDate).hour(hourArrays.untilTime[0]).minutes(hourArrays.untilTime[1]).seconds(0))) {
                    validationObject.isValid = false;
                    ErrorService.showErrorMessage({
                        code: 'SCHEDULE_DATE_RANGE_PROBLEM'
                    });
                }
                if (moment($scope.runtimeData.fromDate).hour(hourArrays.fromTime[0]).minutes(hourArrays.fromTime[1]).seconds(0).isSameOrAfter(moment($scope.runtimeData.untilDate).hour(hourArrays.untilTime[0]).minutes(hourArrays.untilTime[1]).seconds(0))) {
                    validationObject.isValid = false;
                    ErrorService.showErrorMessage({
                        code: 'FROM_DATE_RANGE_PROBLEM'
                    });

                }
            }

            return validationObject.isValid;
        }


        function MapController($scope, $uibModalInstance, data) {
   
            NgMap.getMap().then(function(map) {
                var location = [data.lat+'', data.long+''];
                if (location[0].length > 0) {
                    $scope.latitude = data.lat;
                }
                if (location[1].length > 0) {
                    $scope.longitude = data.long;
                }
                if (location[0].length > 0 && location[1].length > 0) {
                    $scope.showMarker = true;
                    $scope.markerPosition = [location[0],location[1]]+"";
                } else {
                    $scope.markerPosition = [46.770144, 23.589581]+"";
                }
                // center="46.770144, 23.589581"
                // console.log(map.getCenter().lat(),map.getCenter().lng());
                // map.setCenter({lat: $scope.latitude, lng: $scope.longitude});
                google.maps.event.trigger(map, "resize");
            });

            $scope.mapClicked = function(clickData){
                $scope.showMarker = true;
                $scope.markerPosition = [clickData.latLng.lat(), clickData.latLng.lng()]+"";
                $scope.latitude = clickData.latLng.lat();
                $scope.longitude = clickData.latLng.lng();
            };

            $scope.positionChanged = function(){
                $scope.markerPosition = [$scope.latitude, $scope.longitude]+"";
            };

            // $scope.data = data;
            // var validationFactory   = null,
            //     validationResult;
            // $scope.passwordPlaceholder = '';

            // $scope.runtimeData = {
            //     username: '',
            //     password: '',
            // };

            // if (data.key == 'edit') {
            //     $scope.runtimeData = angular.copy(data.item);
            //     $scope.passwordPlaceholder = "************";
            // }

            // function doTheMagic() {
            //     switch (data.key) {
            //         case 'add':
            //             {
            //                 UserAccountsService.addAccount($scope.runtimeData).then(function () {
            //                     $uibModalInstance.close();
            //                 }).catch(function (error) {
            //                     ErrorService.showErrorMessage(error);
            //                 });
            //                 break;
            //             }
            //         case 'edit':
            //             {
            //                 if (angular.isUndefined($scope.runtimeData.password) || $scope.runtimeData.password === null || $scope.runtimeData.password.length === 0) {
            //                     delete $scope.runtimeData.password;
            //                 }
            //                 UserAccountsService.updateAccount($scope.runtimeData, $scope.data.item).then(function () {
            //                     $uibModalInstance.close();
            //                 }).catch(function (error) {
            //                     ErrorService.showErrorMessage(error);
            //                 });
            //                 break;
            //             }
            //         case 'delete':
            //             {
            //                 UserAccountsService.deleteAccount($scope.data.item).then(function () {
            //                     $uibModalInstance.close();
            //                 }).catch(function (error) {
            //                     ErrorService.showErrorMessage(error);
            //                 });
            //                 break;
            //             }
            //     }
            // }


            $scope.$on('CLOSE_ALL_MODALS_NOTIFICATION', function () {
                $uibModalInstance.dismiss('cancel');
            });

            $scope.ok = function () {
                $uibModalInstance.close({lat:$scope.latitude, long:$scope.longitude});

            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }



        function showModal(data) {
            var out = $uibModal.open({
                animation: true,
                templateUrl: 'map-popup.html',
                controller: ['$scope', '$uibModalInstance', 'data', MapController],
                size: 'xl',
                resolve: {
                    data: data
                }
            });
            out.result.then(function (location) {
                $scope.notification.locations[data.index].latitude  = location.lat;
                $scope.notification.locations[data.index].longitude = location.long;
                $scope.locationChanged($scope.notification.locations[data.index]);
            }, function () {
                //modal canceled
            });
        }

        //endregion

        //region Functions $scope


        $scope.dateOptions = {
            formatYear: 'yyyy',
            minDate: new Date(),
            startingDay: 1
        };

        $scope.activeFromDateOptions = {
            formatYear: 'yyyy',
            minDate: new Date(),
            startingDay: 1
        };

        $scope.activeUntilDateOptions = {
            formatYear: 'yyyy',
            minDate: new Date(),
            startingDay: 1
        };

        $scope.open1 = function () {
            if ($scope.runtimeData.untilDate !== null) {
                $scope.activeFromDateOptions.maxDate = $scope.runtimeData.untilDate;
            }
            $scope.popup1.opened = true;
        };

        $scope.open2 = function () {
            if ($scope.runtimeData.untilDate !== null) {
                $scope.activeFromDateOptions.maxDate = $scope.runtimeData.untilDate;
            }
            $scope.popup2.opened = true;
        };

        $scope.openUntilDate = function () {
            if ($scope.runtimeData.fromDate !== null) {
                $scope.activeUntilDateOptions.minDate = $scope.runtimeData.fromDate;
            }
            $scope.popup3.opened = true;
        };

        $scope.setDate = function (year, month, day) {
            $scope.runtimeData.dt = new Date(year, month, day);
        };

        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };

        $scope.popup3 = {
            opened: false
        };


        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }

        $scope.addNotification = function () {
            function add() {
                var areLocationsValid = validateLocations();
                if (mode === 'add' && validate() && areLocationsValid) {
                    $scope.notification.setSchedule($scope.runtimeData.dt, $scope.runtimeData.time, $scope.runtimeData.shouldSendNow);
                    $scope.notification.setActiveFrom($scope.runtimeData.fromDate, $scope.runtimeData.fromTime);
                    $scope.notification.setActiveUntil($scope.runtimeData.untilDate, $scope.runtimeData.untilTime);
                    if ($scope.runtimeData.isCityNotification) {
                        delete $scope.notification.locations;
                    }
                    NotificationService.addNotification($scope.notification).then(function () {
                        alertify.success(TranslationService.translate('SAVE_SUCCESSFUL', 'SAVE_SUCCESSFUL'));
                        $scope.goTo('/notify');
                    }).catch(function (error) {
                        ErrorService.showErrorMessage(error);
                    });
                }
            }
            
            $scope.actionQueue.addAction(add);
            // $scope.notification = {"id":null,"description":"Mytable","typeID":1,"schedule":"now","activeFrom":"1517908500000","activeUntil":"1518685200000","subject":"Mutable","sentAt":null,"locations":[{"districtID":18,"latitude":"46.782421","longitude":"23.669988"}]};
            // NotificationService.addNotification($scope.notification).then(function () {
            //     alertify.success(TranslationService.translate('SAVE_SUCCESSFUL', 'SAVE_SUCCESSFUL'));
            //     $scope.goTo('/notify');
            // }).catch(function (error) {
            //     ErrorService.showErrorMessage(error);
            // });

        };

        $scope.saveNotification = function () {
            // $scope.updateDistrictSelection();
            function save() {
                var areLocationsValid = validateLocations();
                if (mode === 'edit' && validate() && areLocationsValid) {
                    $scope.notification.setSchedule($scope.runtimeData.dt, $scope.runtimeData.time, $scope.runtimeData.shouldSendNow);
                    $scope.notification.setActiveFrom($scope.runtimeData.fromDate, $scope.runtimeData.fromTime);
                    $scope.notification.setActiveUntil($scope.runtimeData.untilDate, $scope.runtimeData.untilTime);
                    if ($scope.runtimeData.isCityNotification) {
                        delete $scope.notification.locations;
                    }
                    NotificationService.updateNotification($scope.notification).then(function () {
                        alertify.success(TranslationService.translate('SAVE_SUCCESSFUL', 'SAVE_SUCCESSFUL'));
                        $scope.goTo('/notify');
                    }).catch(function (error) {
                        ErrorService.showErrorMessage(error);
                    });
                }
            }
            $scope.actionQueue.addAction(save);
        };
        $scope.getNotificationType = function (notificationTypeID) {
            return $scope.runtimeData.processedNotificationTypeList[notificationTypeID];
        };

        $scope.selectNotificationType = function (notificationType) {
            if (notificationType.id !== 1) {
                $scope.runtimeData.isCityNotification = false;
            }
            $scope.notification.selectNotificationType(notificationType.id);
        };

        $scope.getDistrict = function (districtID) {
            return $scope.runtimeData.processedDistricts[districtID];
        };


        // $scope.updateDistrictSelection = function(){
        //     $scope.notification.selectDistricts($scope.selectedDistricts);
        // };


        $scope.removeInvalidMarkers = function (id) {
            $('#' + id).removeClass('invalid');
        };

        $scope.addLocation = function () {
            var areCurrentLocationsValid = validateLocations();
            if (areCurrentLocationsValid) {
                $scope.notification.addEmptyLocation();
            } else {
                ErrorService.showErrorMessage({
                    code: 'LOCATIONS_ERROR'
                });
            }
        };

        $scope.removeLocation = function (index) {
            if (!$scope.notification.removeLocation(index)) {
                ErrorService.showErrorMessage({
                    code: 'DELETE_LAST_LOCATION_ERROR'
                });
            }
        };

        $scope.selectDistrict = function (location, district) {
            location.selectDistrict(district);
        };

        $scope.locationChanged = function (location) {
            if (isLocationValid(location)) {
                DistrictService.getDistrict(location.latitude, location.longitude).then(function (result) {
                    location.selectDistrict(result.data);
                }).catch(function (error) {
                    console.log('error', error);
                });

                NotificationService.getAddress(location.latitude, location.longitude).then(function(result){
                    location.address = result.data.address;
                });
            }
        };


        $scope.showMapPopup = function(index) {
            showModal({
                index: index,
                lat: $scope.notification.locations[index].latitude === null ? '' : $scope.notification.locations[index].latitude,
                long: $scope.notification.locations[index].longitude === null ? '': $scope.notification.locations[index].longitude 
            });
        };

        //endregion

        //region Event listeners

        // Unregister
        $scope.$on('$destroy', function () {
            $scope.actionQueue.clear();
        });
        //endregion

        //region Initialization
        var init = function () {
            var schd, activeUntil, activeFrom;
            if (mode === 'add') {
                $scope.notification = new NotificationFactory();
                DistrictService.getDistricts().then(function (result) {
                    angular.forEach(result.data, function (district) {
                        $scope.runtimeData.processedDistricts[district.id] = district;
                    });
                    $scope.districtList = result.data;
                });
            } else {
                NotificationService.getNotification(notifyID).then(function (result) {
                    $scope.notification = new NotificationFactory(result.data[0]);
                    if (!($scope.notification.sentAt === null || angular.isUndefined($scope.notification.sentAt)) || $scope.notification.id === null) {
                        $scope.goTo('/notify');
                    }
                    if (angular.isUndefined(result.data[0].locations) || result.data[0].locations.length === 0) {
                        $scope.runtimeData.isCityNotification = true;
                    }
                    schd = $scope.notification.getSchedule($scope.format);
                    activeUntil = $scope.notification.getActiveUntil($scope.format);
                    activeFrom = $scope.notification.getActiveFrom($scope.format);

                    $scope.runtimeData.dt = schd.date;
                    $scope.runtimeData.time = schd.hour + ':' + schd.minute;
                    $scope.runtimeData.fromDate = activeFrom.date;
                    $scope.runtimeData.fromTime = activeFrom.hour + ':' + activeFrom.minute;
                    $scope.runtimeData.untilDate = activeUntil.date;
                    $scope.runtimeData.untilTime = activeUntil.hour + ':' + activeUntil.minute;

                    // $scope.selectedDistricts = $scope.notification.districts;

                    DistrictService.getDistricts().then(function (result) {
                        angular.forEach(result.data, function (district) {
                            $scope.runtimeData.processedDistricts[district.id] = district;
                            angular.forEach($scope.notification.districts, function (d) {
                                if (d.id === district.id) {
                                    $scope.runtimeData.processedDistricts[district.id].ticked = true;
                                }
                            });
                        });
                        $scope.districtList = result.data;
                    });

                }).catch(function (error) {
                    ErrorService.showErrorMessage(error);
                });
            }

            NotificationService.getNotificationTypes().then(function (result) {
                angular.forEach(result.data, function (notifType) {
                    $scope.runtimeData.processedNotificationTypeList[notifType.id] = notifType;
                });
                $scope.notificationTypeList = result.data;
            });


            NgMap.getMap().then(function(map) {
                var center = map.getCenter();
                google.maps.event.trigger(map, "resize");
                map.setCenter(center);
            });
  




            // var d;
            // if (mode === 'edit') {
            //     $scope.notification = FirebaseService.get('announcements/' + notifyID, 'object');
            //     $scope.notification.$loaded().then(function () {
            //         $scope.dataLoaded = true;
            //         $scope.message = $scope.notification.message;
            //         $scope.subject = $scope.notification.subject;
            //         $scope.runtimeData.shouldSendNow = $scope.notification.sendNow;
            //         if (angular.isDefined($scope.notification.schedule)) {
            //             d = moment($scope.notification.schedule, 'x');
            //             $scope.setDate(d.year(), d.month(), d.date());
            //             $scope.runtimeData.time = d.format('HH');
            //         }
            //     });
            // } else {
            //     d = moment();
            //     $scope.setDate(d.year(), d.month(), d.date());
            //     $scope.runtimeData.time = d.format('HH');
            // }

        };
        init();
        //endregion

    }
]);