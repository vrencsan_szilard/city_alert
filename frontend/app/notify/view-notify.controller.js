/**
 * Created by LIH on 8/5/2017.
 */
angular.module('city_alert').controller('ViewNotifyController', ['$scope','ErrorService','$routeParams',
    'TranslationService','ActionQueue','LoginService','ValidationFactory','NotificationFactory','NotificationService','DistrictService',
    function($scope,ErrorService,$routeParams,TranslationService,ActionQueue,LoginService,ValidationFactory,NotificationFactory, NotificationService, DistrictService){

        //region Declarations - non $scope
        var notifyID = $routeParams.notifyID;
        //endregion

        //region Declarations - $scope
        $scope.notification = {};
        $scope.dataLoaded = false;
        $scope.format = TranslationService.translate('DATE_FORMAT', 'MM.DD.YYYY').replace(/Y/g, 'y').replace(/D/g, 'd');
        $scope.altInputFormats = ['M!/d!/yyyy'];
        $scope.actionQueue = new ActionQueue();
        $scope.notificationTypeList = [];
        $scope.districtList = [];
        // $scope.selectedDistricts = [];
        $scope.runtimeData = {
            dt: null,
            shouldSendNow: false,
            time: '',
            fromDate: null,
            fromTime: null,
            untilDate: null,
            untilTime: null,
            processedNotificationTypeList: {},
            processedDistricts: {},
            errorMessages: {},
            locationErrorMessages: {}
        };

        //endregion

        //region Functions $scope


        $scope.dateOptions = {
            formatYear: 'yyyy',
            minDate: new Date(),
            startingDay: 1
        };

        $scope.activeFromDateOptions = {
            formatYear: 'yyyy',
            minDate: new Date(),
            startingDay: 1
        };

        $scope.activeUntilDateOptions = {
            formatYear: 'yyyy',
            minDate: new Date(),
            startingDay: 1
        };

    
        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0,0,0,0);
                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }
            return '';
        }

        $scope.getNotificationType = function(notificationTypeID){
            return $scope.runtimeData.processedNotificationTypeList[notificationTypeID];
        };

        $scope.selectNotificationType = function(notificationType){
            $scope.notification.selectNotificationType(notificationType.id);
        };
        
        $scope.getDistrict = function(districtID){
            return $scope.runtimeData.processedDistricts[districtID];
        };


        // $scope.updateDistrictSelection = function(){
        //     $scope.notification.selectDistricts($scope.selectedDistricts);
        // };


    
        //endregion


        //region Event listeners

        // Unregister
        $scope.$on('$destroy', function () {
            $scope.actionQueue.clear();
        });
        //endregion

        //region Initialization
        var init = function() {
            var schd, activeUntil, activeFrom;

                NotificationService.getNotification(notifyID).then(function(result){
                    $scope.notification = new NotificationFactory(result.data[0], true);
                    schd = $scope.notification.getSchedule($scope.format);
                    activeUntil = $scope.notification.getActiveUntil($scope.format);
                    activeFrom = $scope.notification.getActiveFrom($scope.format);
                    
                    $scope.runtimeData.dt =schd.date;
                    $scope.runtimeData.time =schd.hour+':'+schd.minute;
                    $scope.runtimeData.fromDate =activeFrom.date;
                    $scope.runtimeData.fromTime =activeFrom.hour+':'+activeFrom.minute;
                    $scope.runtimeData.untilDate =activeUntil.date;
                    $scope.runtimeData.untilTime =activeUntil.hour+':'+activeUntil.minute;

                    // $scope.selectedDistricts = $scope.notification.districts;

                    DistrictService.getDistricts().then(function(result){
                        angular.forEach(result.data, function(district){
                            $scope.runtimeData.processedDistricts[district.id] = district;
                            // angular.forEach($scope.notification.districts, function(d){
                            //     if (d.id === district.id) {
                            //         $scope.runtimeData.processedDistricts[district.id].ticked = true;
                            //     }
                            // });
                        });
                        $scope.districtList = result.data;
                    });

                }).catch(function(error){
                    ErrorService.showErrorMessage(error);
                });
            

            NotificationService.getNotificationTypes().then(function(result){
                angular.forEach(result.data, function(notifType){
                    $scope.runtimeData.processedNotificationTypeList[notifType.id] = notifType;
                });
                $scope.notificationTypeList = result.data;
            });

     

    
           

        };
        init();
        //endregion

    }]);