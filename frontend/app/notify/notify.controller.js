/**
 * Created by LIH on 8/1/2017.
 */
angular.module('city_alert').controller('NotifyCtrl', ['$scope', 'ErrorService', 'LoginService', 'GridFactory', 'NotificationService', 'TranslationService', '$uibModal',
    function ($scope, ErrorService, LoginService, GridFactory, NotificationService, TranslationService, $uibModal) {

        //region Declarations - non $scope
        //endregion

        //region Declarations - $scope
        $scope.filters = [{
            key: 'future_notifications',
            selected: false,
            index: 0,
            property: 'activeFrom',
            comparison: 'smaller', //today is before than the checked date
            value: Number(moment().format('x')),
            isExclusive: true,
            type: 'date'
        }, {
            key: 'expired_notifications',
            selected: false,
            index: 1,
            property: 'activeUntil',
            comparison: 'bigger', //today is after the checked date
            value: Number(moment().format('x')),
            isExclusive: true,
            type: 'date'
        }, {
            key: 'active_notifications',
            selected: false,
            index: 2,
            comparison: 'between',
            property: ['activeFrom', 'activeUntil'],
            value: Number(moment().format('x')),
            isExclusive: true,
            type: 'date'
        }, {
            key: 'today_notifications',
            selected: false,
            index: 2,
            comparison: 'equals',
            property: 'schedule',
            value: Number(moment().format('x')),
            isExclusive: true,
            type: 'date'
        }];
        $scope.categoryFilters =[];
        $scope.notifications = [];
        $scope.gridFactory = {};

        $scope.actions = {
            delete: function (item) {
                showModal({
                    key: 'delete',
                    item: item,
                    templateUrl: 'confirmation-modal',
                    title: TranslationService.translate('CONFIRM_DELETE')
                });
            },
            deleteMany: function(items) {
                showModal({
                    key: 'delete-many',
                    items: items,
                    templateUrl: 'confirmation-modal',
                    title: TranslationService.translate('CONFIRM_DELETE')
                });
            },
            edit: function (item) {
                // showModal({
                //     key: 'edit',
                //     item: item
                // });
                $scope.goTo('notify/edit/'+item.id);
            },
            view: function (item) {
                // showModal({
                //     key: 'edit',
                //     item: item
                // });
                $scope.goTo('notify/view/'+item.id);
            }
        };

        $scope.dataLoaded = false;

        //endregion


        //region Functions non $scope


        function reloadGrid() {
            // UserAccountsService.getAccounts().then(function(result){
            //     $scope.gridFactory.reloadWithItems(result.data);
            //     $scope.runtimeData.dataLoaded = true;
            // }).catch(function(error){
            //     ErrorService.showErrorMessage(error.data);
            // });

            NotificationService.getNotifications().then(function (result) {
                $scope.notifications = result.data;
                $scope.gridFactory.reloadWithItems(result.data);
                $scope.dataLoaded = true;
            }).catch(function (error) {
                ErrorService.showErrorMessage(error);
            });
        }

        function EditNotificationModalController($scope, $uibModalInstance, data) {
            $scope.data = data;


            $scope.$on('CLOSE_ALL_MODALS_NOTIFICATION', function () {
                $uibModalInstance.dismiss('cancel');
            });

            $scope.ok = function () {
                switch (data.key) {
                    case 'delete':
                        {
                            NotificationService.deleteNotification($scope.data.item).then(function () {
                                $uibModalInstance.close();
                            }).catch(function (error) {
                                ErrorService.showErrorMessage(error);
                            });
                            break;
                        }
                    case 'delete-many':
                        {
                            var temp = [];
                            angular.forEach(data.items, function(item){
                                temp.push(item.id);
                            });
                            // return;
                            NotificationService.deleteNotifications(temp).then(function () {
                                $uibModalInstance.close();
                            }).catch(function (error) {
                                ErrorService.showErrorMessage(error);
                            });
                            break;
                        }
                }
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

        }

        function showModal(data) {
            var out = $uibModal.open({
                animation: true,
                templateUrl: data.templateUrl || 'edit-notification-popup.html',
                controller: ['$scope', '$uibModalInstance', 'data', EditNotificationModalController],
                size: 'md',
                resolve: {
                    data: data
                }
            });
            out.result.then(function () {
                switch (data.key) {
                    case 'add':
                        {
                            alertify.success(TranslationService.translate('NOTIFICATION_WAS_CREATED', 'The notification has been created.'));
                            break;
                        }
                    case 'edit':
                        {
                            alertify.success(TranslationService.translate('CHANGES_SAVED', 'The changes have been saved.'));
                            break;
                        }
                    case 'delete':
                        {
                            alertify.success(TranslationService.translate('NOTIFICATION_WAS_DELETED', 'The notification has been deleted.'));
                            break;
                        }
                    case 'delete-many':
                        {
                            alertify.success(TranslationService.translate('NOTIFICATIONS_WHERE_DELETED', 'The notifications have been deleted.'));
                            break;
                        }
                }
                reloadGrid();
            }, function () {
                //modal canceled
            });
        }

        //endregion

        //region Functions $scope
        $scope.toggleFilterSelection = function (filter, exclusiveFilterKey) {
            filter.selected = !filter.selected;
            if (angular.isDefined(exclusiveFilterKey) && exclusiveFilterKey !== null) {
                angular.forEach($scope.filters, function(f){
                    if (f.key !== exclusiveFilterKey) {
                        f.selected = false;
                        $scope.gridFactory.applyFilter(f.property, f.selected ? f.value : 'not_filtered', f.isExclusive, f.index, f.comparison, f.type);
                    }
                });
            }
            $scope.gridFactory.applyFilter(filter.property, filter.selected ? filter.value : 'not_filtered', filter.isExclusive, filter.index, filter.comparison, filter.type);
        };

        $scope.deleteVisible = function(){
            $scope.actions.deleteMany($scope.gridFactory.visibleItems());
        };
        $scope.deleteSelected = function(){
            $scope.actions.deleteMany($scope.gridFactory.selectedItems());
        };

        $scope.add = function(){
             $scope.notification = {"id":null,"description":"Mutable","typeID":1,"schedule":"now","activeFrom":"1517908500000","activeUntil":"1518685200000","subject":"Mutable","sentAt":null,"locations":[{"districtID":18,"latitude":"46.782421","longitude":"23.669988"}]};
            NotificationService.addNotification($scope.notification).then(function () {
                alertify.success(TranslationService.translate('SAVE_SUCCESSFUL', 'SAVE_SUCCESSFUL'));
                // $scope.goTo('/notify');
            }).catch(function (error) {
                ErrorService.showErrorMessage(error);
            });
        };
        //endregion


        //region Event listeners

        // Unregister
        $scope.$on('$destroy', function () {

        });
        //endregion

        //region Initialization
        var init = function () {

            // console.log('ts', FirebaseService.timestamp);

            //FirebaseService.add('announcements', {
            //    cityKey: "-KaqeYmnW6e_de4wH_dx",
            //    schedule: moment('06.08.17','DD.MM.YYYY').format('x'),
            //    createdAt: moment().format('x'),
            //    subject: 'Tomorrow',
            //    message: 'Tmrrw, This is a message for the folks out there'
            //});


            // $scope.adminData = UserService.getAdminData(LoginService.getUid());
            // $scope.adminData.$loaded().then(function(){
            //     $scope.notifications = FirebaseService.query('announcements/','array','cityKey',$scope.adminData.cityKey);

            //     $scope.notifications.$loaded().then(function(){
            //         $scope.gridFactory = new GridFactory('announcements_grid', $scope.notifications);
            //         $scope.dataLoaded = true;
            //     });
            // });


            NotificationService.getNotificationTypes().then(function(results){
                angular.forEach(results.data, function(type, index){
                    $scope.categoryFilters.push({
                        name: type.name,
                        key: 'category_'+type.id,
                        selected: false,
                        index: index + 3,
                        comparison: 'equals',
                        property: 'typeID',
                        value: type.id,
                        isExclusive: false,
                        type: 'number'
                    });
                });
            }).catch(function(error){

            });

            NotificationService.getNotifications().then(function (result) {
                $scope.notifications = result.data;
                $scope.gridFactory = new GridFactory('notifications_grid', $scope.notifications);
                $scope.dataLoaded = true;
            }).catch(function (error) {
                ErrorService.showErrorMessage(error);
            });

        };
        init();
        //endregion

    }
]);