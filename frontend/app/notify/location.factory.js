angular.module('city_alert').factory('LocationFactory',[function(){
    function LocationFactory(location){
        var fields = ['districtID','latitude', 'longitude', 'address'],
            that   = this;
        angular.forEach(fields, function(field){
            that[field] = (angular.isDefined(location) && angular.isDefined(location[field])) ? location[field] : null;
        });
    }

    LocationFactory.prototype.selectDistrict = function(district){
        this.districtID = district.id;
    };

    return LocationFactory;
}]);