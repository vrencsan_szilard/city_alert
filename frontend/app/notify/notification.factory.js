angular.module('city_alert').factory('NotificationFactory',['LocationFactory',function(LocationFactory){
    
    function getDateAndTimeFromTimeStamp(timestamp, format){
        var m = moment(timestamp, 'x');
        return {
            date: m.toDate(),
            hour: m.format('HH'),
            minute: m.format('mm')
        };
    }
    
    function NotificationFactory(notification, dontAddEmptyLocations){
        var fields = ['id', 'description', 'typeID', 'schedule', 'activeFrom', 'activeUntil', 'subject', 'sentAt'],
            that   = this;
        angular.forEach(fields, function(field){
            that[field] = (angular.isDefined(notification) && angular.isDefined(notification[field])) ? notification[field] : null;
        });
        this.locations = [];
        if (angular.isDefined(notification) && angular.isDefined(notification.locations) && notification.locations.length > 0) {
            angular.forEach(notification.locations, function(location){
                that.locations.push(new LocationFactory(location));
            });
        } else {
            if (!dontAddEmptyLocations) {
                this.addEmptyLocation();
            }
        }
    }

    NotificationFactory.prototype.selectNotificationType = function(id){
        this.typeID = id;
    };

    // NotificationFactory.prototype.selectDistricts = function(districts){
    //     var that = this;
    //     this.districts = [];
    //     angular.forEach(districts, function(district){
    //         that.districts.push(district.id);
    //     });
    // };

    NotificationFactory.prototype.setSchedule = function(date, hour, now) {
        this.schedule = now ? 'now' : moment(date).hour(hour.split(':')[0]).minute(hour.split(':')[1]).seconds(0).format('x');
    };

    NotificationFactory.prototype.getSchedule = function(format){
        return getDateAndTimeFromTimeStamp(this.schedule, format);
    };

    NotificationFactory.prototype.setActiveFrom = function(date, hour) {
        this.activeFrom =  moment(date).hour(hour.split(':')[0]).minute(hour.split(':')[1]).seconds(0).format('x');
    };

    NotificationFactory.prototype.getActiveFrom = function(format){
        return getDateAndTimeFromTimeStamp(this.activeFrom, format);
    };

    NotificationFactory.prototype.setActiveUntil = function(date, hour) {
        this.activeUntil =  moment(date).hour(hour.split(':')[0]).minute(hour.split(':')[1]).seconds(0).format('x');
    };

    NotificationFactory.prototype.getActiveUntil = function(format){
        return getDateAndTimeFromTimeStamp(this.activeUntil, format);
    };
    
    NotificationFactory.prototype.addEmptyLocation = function(){
        var n = this.locations.length;
        this.locations.push(new LocationFactory());
        return this.locations[n];
    };
    
    NotificationFactory.prototype.removeLocation = function(index){
        if (this.locations.length > 1) {
            this.locations.splice(index, 1);
            return true;
        }
        return false;
    };
    

    return NotificationFactory;
}]);