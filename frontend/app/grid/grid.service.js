angular.module('city_alert').service('GridService', function () {
  return {
    user_accounts_grid: {
      "headers": {
        "actions": {
          "links": {
            "delete": "id not 1",
            "edit": true
          },
          "order": 0,
          "type": "multiple-links"
        },
        "username": {
          "order": 1,
          "property": "username",
          "type": "text"
        },
        "account_type": {
          "order": 2,
          "property": "type",
          "type": "text",
          "convertValuesFunction": "getAccountTypeString"
        },
        "TYPE_ACCESS": {
          "order": 3,
          "property": "notificationTypes",
          "type": "array",
          "arrayProperty": "name",
          "propertySeparator": ",",
          "defaultValueKey": "ALL"
        }
        // "name" : {
        //   "order" : 2,
        //   "properties" : [ "lastName", "firstName" ],
        //   "property" : "name",
        //   "propertySeparator" : " ",
        //   "type" : "multiple-properties"
        // }
      },
      "order": {
        "username": true,
        "account_type": "desc"
      }
    },
    notifications_grid: {
      "headers": {
        "select_row":{
          "order": 0,
          "type":"row-selection"
        },
        "actions": {
          "links": {
            "delete": true,
            "edit": "not sentAt",
            "view": true
          },
          "order": 1,
          "type": "multiple-links"
        },
        "subject": {
          "order": 2,
          "property": "subject",
          "type": "text"
        },
        "description": {
          "order": 3,
          "property": "description",
          "type": "text"
        },
        "type": {
          "order": 4,
          "property": "typeName",
          "type": "text"
        },
        "district": {
          "order": 5,
          "property": "districts",
          "type": "array",
          "arrayProperty": "name",
          "propertySeparator": ",",
          "defaultValueKey": "WHOLE_CITY"
        },
        "active_period": {
          "order": 6,
          "properties": ["activeFrom","activeUntil"],
          "propertyType": "datetime",
          "propertySeparator": "-",
          "type": "multiple-properties"
        },
        "schedule": {
          "order": 6,
          "property": "schedule",
          "type": "datetime"
        },
        // "sent_at": {
        //   "order": 7,
        //   "property": "sentAt",
        //   "type": "datetime"
        // },
        "views": {
          "order": 8,
          "property": "views",
          "type": "number"
        }
      },
      "order": {
        "select": false,
        "actions": false,
        "subject": true,
        "description": true,
        "type": true,
        "district": true,
        "schedule": "asc",
        "views": true,
        "active_period": false
      }
    }
  };
});