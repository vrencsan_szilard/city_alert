/**
 * Created by LIH on 7/25/2017.
 */
angular.module('city_alert').directive('grid', ['GridFactory','CONFIG','TranslationService',function(GridFactory,CONFIG,TranslationService){
    return {
        restrict:'E',
        scope: {
            gridFactory: '=',
            actions: '=',
            rowSelectable: '@',
            disabledDeleteForId: '@'
        },
        templateUrl:CONFIG.baseUrl +'/grid-directive.html' + '?v='+CONFIG.versionNumber,
        controller: ['$scope','$element', '$attrs', function($scope, $element, $attrs){
            $scope.isRowSelectable = angular.isDefined($scope.rowSelectable) && $scope.rowSelectable.toUpperCase == 'TRUE';
            $scope.runtimeData = {
                allVisibleItemsCheckSelected: false
            };

            $scope.comparator = function(v1, v2){
                if (angular.isUndefined(v1.value) || angular.isUndefined(v1.value)) {
                    return 0;
                }
                var orderBy =  $scope.gridFactory.orderBy.substr(1), order =  $scope.gridFactory.orderBy.substr(0,1);
                if (v1.type === 'number' || v2.type === 'number') {
                    return v1.value < v2.value ? 1 : -1;
                }
            };

            $scope.headerComparator = function(v1, v2){
                return v1.value.order < v2.value.order ? -1 : 1;
            };

            $scope.translate = function(id, defaultValue, isHTML){
                return TranslationService.translate(id, defaultValue, isHTML);
            };

            $scope.action = function(linkID, item){
                $scope.actions[linkID](item);
            };

            $scope.showLink = function(linkValue, item){
                var temp, retValue = false;
                if (angular.isString(linkValue)) {
                    temp = linkValue.split(' ');
                    switch(temp[0]){
                        case 'not':{
                            retValue = (angular.isUndefined(item[temp[1]]) || item[temp[1]] === null || item[temp[1]] === '');
                            break;
                        }
                    }
                    switch(temp[1]){
                        case 'not':{
                            retValue = item[temp[0]] != temp[2];
                            break;
                        }
                    }
                    return retValue;
                } else {
                    return linkValue;
                }
            };

            $scope.callFunction = function(functionName, header, item){
                if (angular.isDefined($scope.$parent[functionName])) {
                    return $scope.$parent[functionName](item[header.property]);
                }
                return '';
            };

            $scope.allVisibleSelectionChanged = function(){
                $scope.gridFactory.toggleAllVisibleItemsSelection($scope.runtimeData.allVisibleItemsCheckSelected);
            };

        }]
    }
}]);