/**
 * Created by LIH on 7/25/2017.
 */
angular.module('city_alert').factory('GridFactory', ['$filter','GridService','TranslationService','localStorageService',function($filter, GridService,TranslationService,localStorageService){

    function diff(nr1, nr2) {
        if (nr1 === nr2) {
            return 0;
        } else {
            if (nr1 === 'ALL' || nr2 === 'ALL') {
                if (nr1 === 'ALL') {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                return nr1 - nr2;
            }
        }
    }
    function areKeysTheSame(obj1, obj2) {
        var areTheSame = true, 
            keys1      = Object.keys(obj1), 
            keys2      = Object.keys(obj2);

        if (keys1.length === keys2.length) {
            angular.forEach(keys1, function(key, index){
                if (!areTheSame) return;
                if (key !== keys2[index]) {
                    areTheSame = false;
                }
            });
        } else {
            areTheSame = false;
        }
        return areTheSame;
    }

    function GridFactory(gridID, items){
        var nrOfItems, visibleItems, that = this, i;
        this.filters = {
            exclusive: {},
            inclusive: []
        };
        this.config = GridService[gridID];
        this.gridID = gridID;
        // this.configLoaded = false;
        this.items = items;
        this.unfilteredItems = items;
        this.filteredAndOrderedItems = [];
        this.filteredHeaderList = [];
        //console.log('these are the items', this.items);
        // this.config.$loaded().then(function(){
            that.configLoaded = true;

            angular.forEach(that.config.order, function(orderValue, headerID){
                if (orderValue==='asc' || orderValue === 'desc') {
                    //that.orderBy = orderValue === 'asc'?'+'+headerID: '-'+headerID;
                    that.orderBy = orderValue === 'asc'?'+'+ that.config.headers[headerID].property: '-'+that.config.headers[headerID].property;
                }
            });
            that.nrOfVisibleButtons = angular.isDefined(that.config.displayedPageNumberCount) ? that.config.displayedPageNumberCount : 5;

            that.visibleItemsPerPageButtons = [];

            that.selectedPage = 1;
            that.filterValue = null;
            that.filterHeader = null;
            that.filterProperty = null;
            that.visiblePageNumbers = [];

            nrOfItems = that.items.length;
            visibleItems = [5, 10, 25, 50, 'ALL'];
            angular.forEach(visibleItems, function(item){
                if (item == 'ALL' || item < nrOfItems) {
                    that.visibleItemsPerPageButtons.push(item);
                }
            });
            that.itemsPerPage = that.visibleItemsPerPageButtons.length > 1 ?  that.visibleItemsPerPageButtons[1] :  that.visibleItemsPerPageButtons[0];
            that.numberOfPages = Math.ceil(nrOfItems /  (that.itemsPerPage === 'ALL' ? nrOfItems : that.itemsPerPage));
            for(i=1; i<=Math.min(that.numberOfPages,that.nrOfVisibleButtons); i+=1) {
                that.visiblePageNumbers.push(i);
            }
            that.selectedPage = that.visiblePageNumbers[0];
            that.firstVisibleItem = (that.selectedPage - 1) * (that.itemsPerPage === 'ALL' ? nrOfItems : that.itemsPerPage);
            this.loadFromLocalStorage();
        // });
    }

    GridFactory.prototype.mergeList = function(list){
        var that = this;
        angular.forEach(list, function(item, index){
            that.unfilteredItems[index] = item;
        });
        while(that.unfilteredItems.length > list.length) {
            that.unfilteredItems.pop();
        }
        this.reload();
    };

    GridFactory.prototype.reloadWithItems = function(items){
        this.items = items;
        this.unfilteredItems = items;
        this.reload();
    };

    GridFactory.prototype.reload = function(){
        var nrOfItems, visibleItems, that = this, i;
        this.visibleItemsPerPageButtons = [];
        this.visiblePageNumbers = [];
        nrOfItems = this.items.length;
        visibleItems = [5, 10, 25, 50, 'ALL'];
        angular.forEach(visibleItems, function(item){
            if (item == 'ALL' || item < nrOfItems) {
                that.visibleItemsPerPageButtons.push(item);
            }
        });
        //this.itemsPerPage = this.visibleItemsPerPageButtons.length > 1 ?  this.visibleItemsPerPageButtons[1] :  this.visibleItemsPerPageButtons[0];
        this.itemsPerPage = diff(this.visibleItemsPerPageButtons.lastItem(), this.itemsPerPage) > 0 ? this.itemsPerPage : this.visibleItemsPerPageButtons.lastItem();
        this.numberOfPages = Math.ceil(nrOfItems /  (this.itemsPerPage === 'ALL' ? nrOfItems : this.itemsPerPage));
        for(i=1; i<=Math.min(this.numberOfPages,this.nrOfVisibleButtons); i+=1) {
            this.visiblePageNumbers.push(i);
        }
        this.selectedPage = diff(this.visiblePageNumbers.lastItem(), this.selectedPage) > 0 ? this.selectedPage : this.visiblePageNumbers.lastItem();

        this.firstVisibleItem = (this.selectedPage - 1) * (this.itemsPerPage === 'ALL' ? nrOfItems : this.itemsPerPage);
        this.saveToLocalStorage();
    };

    GridFactory.prototype.itemVisible = function (index, item) {
        var nrOfItems = this.items.length, isVisible = false;
        isVisible = index >= this.firstVisibleItem  && index < this.firstVisibleItem + (this.itemsPerPage === 'ALL' ? nrOfItems : this.itemsPerPage);
        //if (isVisible && this.filterHeader !== null && this.filterValue  !== null) {
        //    isVisible = item[this.filterHeader].content == this.filterValue;
        //}
        item.visible = isVisible;
        if (!isVisible) {
            item.isSelected = false;
        }
        return isVisible;
    };

    GridFactory.prototype.visibleItems = function(){
        var visibleItems = [];
        angular.forEach(this.filteredAndOrderedItems, function(item){
            if (item.visible) {
                visibleItems.push(item);
            }
        });
        return visibleItems;
    };

    GridFactory.prototype.toggleOrder = function(headerID){
        var that = this;
        if (angular.isDefined(this.config.order[headerID]) && this.config.order[headerID]){
            angular.forEach(this.config.order, function(orderData,  hID){
                //set every item(except the one that was clicked) in order to  either true or false
                //we do this to clear the previously ordererd header
                if(hID !== headerID) {
                    that.config.order[hID] = orderData ? true : false;
                }
            });
            this.config.order[headerID] = this.config.order[headerID] == 'asc' ? 'desc' : 'asc';
            this.orderBy = this.config.order[headerID] === 'asc'?'+'+ this.config.headers[headerID].property: '-'+this.config.headers[headerID].property;
        }
        this.saveToLocalStorage();
    };

    GridFactory.prototype.selectPage = function(pageNumber, shouldNotSave) {
        var that = this, nrOfItems = this.items.length;
        if (pageNumber > 0 && pageNumber <=  this.numberOfPages) {
            this.selectedPage = pageNumber;
            if (this.selectedPage > this.visiblePageNumbers[this.nrOfVisibleButtons - 1]) {
                angular.forEach(this.visiblePageNumbers, function (pg, index) {
                    ++that.visiblePageNumbers[index];
                });
            } else if (this.selectedPage < this.visiblePageNumbers[0]) {
                angular.forEach(this.visiblePageNumbers, function (pg, index) {
                    --that.visiblePageNumbers[index];
                });
            }
            this.firstVisibleItem = (this.selectedPage - 1) * (this.itemsPerPage === 'ALL' ? nrOfItems : this.itemsPerPage);
        }
        if (!shouldNotSave) {
            this.saveToLocalStorage();
        }
    };

    GridFactory.prototype.selectNumberOfItemsPerPage = function(itemsPerPage) {
        var nrOfItems = this.items.length, i;
        this.visiblePageNumbers = [];
        this.itemsPerPage = itemsPerPage;

        this.numberOfPages = Math.ceil(nrOfItems /  (this.itemsPerPage === 'ALL' ? nrOfItems : this.itemsPerPage));
        for(i=1; i<=Math.min(this.numberOfPages,this.nrOfVisibleButtons); i+=1) {
            this.visiblePageNumbers.push(i);
        }

        this.selectedPage = this.visiblePageNumbers[0];
        this.firstVisibleItem = (this.selectedPage - 1) * (this.itemsPerPage === 'ALL' ? nrOfItems : this.itemsPerPage);
        this.saveToLocalStorage();
    };

    GridFactory.prototype.headerForExport = function(){
        var response = [];
        angular.forEach(this.filteredHeaderList, function(header){
            if (header.type != 'multiple-links' && header.type !== 'row-selection') {
                response.push(TranslationService.translate(header.key, header.key, false));
            }
        });

        return response;
    };

    GridFactory.prototype.dataForExport = function(visible){
        var response = [], exportableItems = visible ? this.visibleItems() : this.selectedItems(), 
            that = this, csvItem = {}, csvHeaders = [], tempStr;

        //prepare header
        angular.forEach(this.filteredHeaderList, function(header){
            if (header.type != 'multiple-links' && header.type !== 'row-selection') {
                csvHeaders.push({value: TranslationService.translate(header.key, header.key, false), property: header.property, type: header.type, propertyType: header.propertyType,
                    properties: header.properties, arrayProperty: header.arrayProperty, propertySeparator: header.propertySeparator, defaultValueKey: header.defaultValueKey});
            }
        });


        angular.forEach(exportableItems, function(item){
            csvItem = {};
            angular.forEach(csvHeaders, function(header){
                switch(header.type){
                    case('date'):{
                        csvItem[header.value] = $filter('chDate')(item[header.property], 'x');
                        break;
                    }
                    case('datetime'):{
                        csvItem[header.value] = $filter('chDateTime')(item[header.property], 'x');
                        break;
                    }
                    case('multiple-properties'):{
                        tempStr = '';
                        if (header.properties && header.properties.length > 0) {
                            angular.forEach(header.properties, function(propertyName){
                                tempStr += (header.propertyType === 'datetime' ? $filter('chDateTime')(item[propertyName], 'x') : item[propertyName]) + header.propertySeparator;
                            });
                            tempStr = tempStr.slice(0, -header.propertySeparator.length);
                        } 
                        csvItem[header.value] = tempStr;
                        break;
                    }
                    case('array'):{
                        tempStr = '';
                        if (item[header.property] && item[header.property].length > 0) {
                            angular.forEach(item[header.property], function(arrayItem){
                                tempStr += arrayItem[header.arrayProperty] + header.propertySeparator;
                            });
                            tempStr = tempStr.slice(0, -1);
                        } else {
                            tempStr = TranslationService.translate(header.defaultValueKey, header.defaultValueKey, false);
                        }
                        csvItem[header.value] = tempStr;
                        break;
                    }
                    default:{
                        csvItem[header.value] = item[header.property];
                    }
                }
            });
            response.push(csvItem);
        });
        return response;
    };

    /**
     * Applies a filter for the grid.
     * If the filter is exclusive then the index is not needed.
     * If the filter is inclusive then the index is needed because it is possible to apply many filters for the same property
     * comparison can be 'equals'(default), 'smaller', 'bigger'
     * type can be 'date', 'value'(default)
     * @param property
     * @param value
     * @param isExclusive
     * @param index
     * @param comparison
     * @param type
     */
    GridFactory.prototype.applyFilter = function(property, value, isExclusive, index, comparison, type){
        if (isExclusive) {
            this.filters.exclusive[property] = {
                property: property,
                value: value,
                comparison: comparison,
                type: type
            };
        } else {
            this.filters.inclusive[index] = {
                property: property,
                value: value,
                comparison: comparison,
                type: type
            }
        }

        this.items = $filter('filterGrid')(this.unfilteredItems, this.filters);
        this.reload();
    };

    GridFactory.prototype.deleteLocalStorage = function(){
        localStorageService.remove(this.gridID + '_ITEMS_PER_PAGE');
        localStorageService.remove(this.gridID + '_SELECTED_PAGE');
        localStorageService.remove(this.gridID + '_VISIBLE_PAGE_NUMBERS');
        localStorageService.remove(this.gridID + '_GRID_ORDER');
        localStorageService.remove(this.gridID + '_GRID_ORDER_BY');
        localStorageService.remove(this.gridID + '_NR_OF_PAGES');
        localStorageService.remove(this.gridID + '_NR_OF_ITEMS');
    };

    GridFactory.prototype.saveToLocalStorage = function(){
        localStorageService.set(this.gridID + '_ITEMS_PER_PAGE', this.itemsPerPage);
        localStorageService.set(this.gridID + '_SELECTED_PAGE', this.selectedPage);
        localStorageService.set(this.gridID + '_VISIBLE_PAGE_NUMBERS', this.visiblePageNumbers);
        localStorageService.set(this.gridID + '_GRID_ORDER', this.config.order);
        localStorageService.set(this.gridID + '_GRID_ORDER_BY', this.orderBy);
        localStorageService.set(this.gridID + '_NR_OF_PAGES', this.numberOfPages);
        localStorageService.set(this.gridID + '_NR_OF_ITEMS', this.items.length);
    };

    GridFactory.prototype.loadFromLocalStorage = function(){
        var storedItemsPerPage       = localStorageService.get(this.gridID + '_ITEMS_PER_PAGE'),
            storedSelectedPage       = localStorageService.get(this.gridID + '_SELECTED_PAGE'),
            storedVisiblePageNumbers = localStorageService.get(this.gridID + '_VISIBLE_PAGE_NUMBERS'),
            storedOrder              = localStorageService.get(this.gridID + '_GRID_ORDER'),
            storedOrderBy            = localStorageService.get(this.gridID + '_GRID_ORDER_BY'),
            storedNumberOfPages      = localStorageService.get(this.gridID + '_NR_OF_PAGES'),
            storedNumberOfItem       = localStorageService.get(this.gridID + '_NR_OF_ITEMS'),
            calculatedNumberOfPages  = Math.ceil(this.items.length /  (storedItemsPerPage === 'ALL' ? this.items.length : storedItemsPerPage));
        if (calculatedNumberOfPages === storedNumberOfPages) {
            this.itemsPerPage = storedItemsPerPage === null ? this.itemsPerPage : storedItemsPerPage;
            this.visiblePageNumbers = storedVisiblePageNumbers === null ? this.visiblePageNumbers : storedVisiblePageNumbers;
            this.numberOfPages = storedNumberOfPages === null ? this.numberOfPages : storedNumberOfPages;
            if (storedSelectedPage !== null) {
                this.selectPage(storedSelectedPage, true);
            }
            // this.selectedPage =  storedSelectedPage === null ? this.selectedPage : storedSelectedPage;
    
            if (areKeysTheSame(storedOrder, this.config.order)) {
                this.config.order = storedOrder === null ? this.config.order : storedOrder;
            }
            this.orderBy = storedOrderBy === null ? this.orderBy : storedOrderBy;
        
        } else {
            this.deleteLocalStorage();
        }
        // this.reload();
    };

    GridFactory.prototype.toggleAllVisibleItemsSelection = function(shouldSelect){
        var visibleItems = this.visibleItems();
        angular.forEach(visibleItems, function(item){
            item.isSelected = shouldSelect;
        });
    };

    GridFactory.prototype.selectedItems = function(){
        var visibleItems = this.visibleItems(),
            selectedItems = [];
        angular.forEach(visibleItems, function(item){
            if(item.isSelected) {
                selectedItems.push(item);
            }
        });
        return selectedItems;
    };

    return GridFactory;
}]);