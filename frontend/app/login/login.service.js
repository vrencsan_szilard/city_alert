/**
 * Created by LIH on 7/24/2017.
 */
angular.module('city_alert').service('LoginService', ['$q','Auth','$http','CONFIG','RuntimeService','HttpService',
function($q, Auth, $http, CONFIG, RuntimeService,HttpService) {
    var firebase = {
        database: function(){
            return {
                ref: function(){}
            };
        }
    };
    var //adminsRef = firebase.database().ref('admins'),
        //appAdminsRef = firebase.database().ref('app_admins'),
        //adminsDataRef = firebase.database().ref('admins_data'),
        adminData = null,
        loggedInAdminUID = null,
        cityData = null,
        adminType = null,
        adminTypes = {
            'ADMIN': 'ADMIN',
            'APP_ADMIN': 'APP_ADMIN'
        };

    // function load(variable, firebasePath, type) {
    //     if (variable === null) {
    //         variable = type(firebase.database().ref(firebasePath));
    //     }
    //     return variable;
    // }

    function storeIDToken(){
        Auth.$getAuth().getToken().then(function(idToken){
            RuntimeService.set('token', idToken);
        });
    }

    return {
        adminTypes: adminTypes,
        getCurrentAdminType: function(){
            return adminType;
        },
        setAdminType: function(type){
            adminType = type;
        },
        getUid: function(){
            return loggedInAdminUID;
        },
        isAdmin: function(uid){
            var deferred = $q.defer(), val;
            adminsRef.child(uid).once('value', function(snapshot){
                val = snapshot.val();
                if (angular.isDefined(val) && val !== null) {
                    loggedInAdminUID = uid;
                    adminType = adminTypes.ADMIN;
                    deferred.resolve(val);
                } else {
                    deferred.reject(false)
                }
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        },
        isAppAdmin: function(uid){
            var deferred = $q.defer(), val;
            appAdminsRef.child(uid).once('value', function(snapshot){
                val = snapshot.val();
                if (angular.isDefined(val) && val !== null) {
                    loggedInAdminUID = uid;
                    adminType = adminTypes.APP_ADMIN;
                    deferred.resolve(val);
                } else {
                    deferred.reject(false)
                }
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        },
        isAdminLoggedIn: function() {
            var deferred = $q.defer(), that = this;
            Auth.$requireSignIn().then(function (userData) {
                storeIDToken();
                that.isAppAdmin(userData.uid).then(function(result){
                    deferred.resolve();
                }).catch(function(error){
                     that.isAdmin(userData.uid).then(function (result) {
                         deferred.resolve();
                     }, function () {
                         deferred.reject();
                     });
                });
            }, function(){
                deferred.reject();
            });
            return deferred.promise;
        },
        isAppAdminLoggedIn: function() {
            var deferred = $q.defer(), that = this;
            Auth.$requireSignIn().then(function (userData) {
                that.isAppAdmin(userData.uid).then(function (result) {
                    deferred.resolve();
                }, function(){
                    deferred.reject();
                })
            }, function(){
                deferred.reject();
            });
            return deferred.promise;
        },
        isAnyAdminLoggedIn: function() {
            var deferred = $q.defer(), that = this;
            Auth.$requireSignIn().then(function (userData) {
                that.isAdmin(userData.uid).then(function (result) {
                    deferred.resolve();
                }, function(){
                    that.isAppAdmin(userData.uid).then(function (result) {
                        deferred.resolve();
                    }, function(){
                        deferred.reject();
                    })
                });
            }, function(){
                deferred.reject();
            });
            return deferred.promise;
        },
        // getUserData: function(){
        //     return load(adminData, '/admins_data/'+loggedInAdminUID, $firebaseObject);
        // },
        // getCityData: function(cityID){
        //     return load(cityData, 'cities/'+cityID, $firebaseObject);
        // },
        login: function(username, password){
            // return $http.post(
            //     CONFIG.api + '/login', {username: username, password: password}
            // );
            return HttpService.formPost('/login', {username: username, password: password});
        },
        isAuthenticated: function(){
            var deferred = $q.defer(),
                that     = this;
            HttpService.handle('GET','/is-authenticated').then(function(result){
                that.setAdminType(result.data.type);
                deferred.resolve(result.data.type);
            }).catch(function(error){
                deferred.reject(error);
            });
            return deferred.promise;  
        },
        logout: function(){
            return HttpService.handle('GET', '/logout');
        }
    };
}]);