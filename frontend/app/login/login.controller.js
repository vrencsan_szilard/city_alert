/**
 * Created by LIH on 7/24/2017.
 */
angular.module('city_alert').controller('LoginCtrl', ['$scope','ValidationFactory','Auth','LoginService', '$rootScope','ErrorService','RuntimeService',
    function($scope, ValidationFactory,Auth,LoginService,$rootScope, ErrorService, RuntimeService){

    //region Declarations - non $scope
    //endregion

    //region Declarations - $scope
    $scope.runtimeData = {
        email: '',
        password: ''
    };
    //endregion


    //region Functions non $scope
    //endregion

    //region Functions $scope
    $scope.login = function(){
        var token, userType;
        // FirebaseService.reset();
        var validationFactory = new ValidationFactory();
        // validationFactory.addRule($scope.runtimeData.email, 'email');
        validationFactory.addRule($scope.runtimeData.email, 'required');
        validationFactory.addRule($scope.runtimeData.password, 'required');

        if (validationFactory.validate()) {

            LoginService.login($scope.runtimeData.email,$scope.runtimeData.password).then(function(response){
                // console.log('rep', response);
                // // token = response.data.token;
                // //     Auth.$signInWithCustomToken(
                // //         token
                // //     ).then(function (userData) {
                // //         LoginService.setAdminType(response.type);
                // //         $scope.goTo('/dashboard');
                // //     }, function (error) {
                // //         ErrorService.showErrorMessage(error);
                // //     });
                $scope.goTo('/dashboard');
            }).catch(function(error){
                ErrorService.showErrorMessage(error);
            });

     
        } else {
            ErrorService.showErrorMessage({code: 'auth/user-not-found'});
        }
    };
    //endregion


    //region Event listeners

    // Unregister
    $scope.$on('$destroy', function () {

    });
    //endregion

    //region Initialization
    var init = function() {
        // FirebaseService.reset();
        $rootScope.showMenu = false;
    };
    init();
    //endregion

}]);