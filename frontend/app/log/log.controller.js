angular.module('city_alert').controller('LogController', ['LogService','$scope',function(LogService, $scope){
    $scope.log = [];
    function init(){
        LogService.getLog().then(function(result){
            $scope.log = result.data;
        });
    }


    $scope.getTime = function(row){
        return moment(row.timestamp, 'x').format('DD.MM.YYYY HH:mm:ss');
    }
    $scope.clear = function(row){
        LogService.clearLog().then(function(result){
            $scope.log = [];
        });
    };
    init();
}]);