angular.module('city_alert').service('LogService', ['HttpService',function(HttpService){
    return {
        getLog: function(){
            return HttpService.handle('GET','/log');
        },
        clearLog: function(){
            return HttpService.handle('DELETE','/log');
        }
    };
}]);