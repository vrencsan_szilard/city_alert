/**
 * Created by LIH on 7/18/2017.
 */
var nextVersionNumber = '0.0.0.0',
    connect = require('gulp-connect'),
    clean = require('gulp-clean'),
    sass = require('gulp-sass'),
    cssmin = require('gulp-cssmin'),
    concat = require('gulp-concat'),
    fs = require('fs'),
    flatten = require('gulp-flatten'),
    filter = require('gulp-filter'),
    runSequence = require('run-sequence'),
    replace = require('gulp-replace'),
    mainBowerFiles = require('gulp-main-bower-files'),
    inject = require('gulp-inject'),
    uglify = require('gulp-uglify'),
    gulp = require('gulp'),
    buildDest = '../backend/build/public';


function handleError(err) {
    console.log(err.toString());
}

gulp.task('connect', function () {
    connect.server({
        root: buildDest + '/',
        //host: '192.168.30.182',
        port: 9090
    });
});

function requireUncached(module) {
    delete require.cache[require.resolve(module)];
    return require(module);
}


gulp.task('increase-version-number', function (cb) {
    var config = requireUncached('./prod-config.js');
    var oldVersionNumberWithDots = config.__config.versionNumber;

    var oldVersionArray = (oldVersionNumberWithDots + '').split('.');
    var newVersionNumber = (parseInt(oldVersionArray.join('')) + 1) + '';
    while (newVersionNumber.length < 4) {
        newVersionNumber = '0' + newVersionNumber;
    }
    var newVersionArray = newVersionNumber.split('');
    var newVersionWithDots = newVersionArray.join('.');
    var newVersionString = ".versionNumber = '" + newVersionArray.join('.') + "';";

    console.log('Old version number: ', oldVersionNumberWithDots);
    console.log('New version number: ', newVersionWithDots);
    nextVersionNumber = newVersionWithDots;

    console.log('replacing with', newVersionString);

    return gulp.src('./prod-config.js', {
            base: './'
        })
        .pipe(replace(/.versionNumber = '\d+\.\d\.\d\.\d';/g, newVersionString))
        .pipe(gulp.dest('./'));
});

gulp.task('clean', function () {
    return gulp.src(buildDest + '/', {
            read: false
        })
        .pipe(clean({
            force: true
        }));
});

gulp.task('clean-dist', function () {
    return gulp.src(buildDest + '/', {
            read: false
        })
        .pipe(clean({
            force: true
        }));
});

gulp.task('sass', function () {
    return gulp.src('app/assets/style/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cssmin())
        .pipe(gulp.dest(buildDest + '/style/'));
});

gulp.task('css', function () {
    return gulp.src(['app/assets/libs/**/*.css'])
        .pipe(cssmin())
        .pipe(gulp.dest(buildDest + '/style/'));
});

gulp.task('prod-js', function () {
    return gulp.src(['prod-config.js', 'app/app.js', 'app/**/*.js'])
        .pipe(concat('script.js'))
        .on('error', handleError)
        .pipe(gulp.dest(buildDest + '/script/'));
});
gulp.task('secondary-js', function () {
    return gulp.src(['secondary-config.js', 'app/app.js', 'app/**/*.js'])
        .pipe(concat('script.js'))
        .on('error', handleError)
        .pipe(gulp.dest(buildDest + '/script/'));
});

gulp.task('head-js', function () {
    return gulp.src(['bower_components/jquery/dist/jquery.min.js'])
        .pipe(gulp.dest(buildDest + '/script/head/'));
});


gulp.task('js', function () {
    return gulp.src(['config.js', 'app/app.js', 'app/**/*.js'])
        .pipe(concat('script.js'))
        .on('error', handleError)
        .pipe(gulp.dest(buildDest + '/script/'));
});

gulp.task('clean-translations', function () {
    return gulp.src(buildDest + '/translations', {
            read: false
        })
        .pipe(clean({
            force: true
        }));
});

gulp.task('clean-generate-translations', function (cb) {
    runSequence('clean-translations', 'generate-translated-files', cb);
});

gulp.task('create-build-dir', function (cb) {
    fs.mkdir(buildDest + '/');
    cb();
});

gulp.task('create-dist-dir', function (cb) {
    fs.mkdir(buildDest + '/');
    cb();
});


gulp.task('generate-translated-files', function (cb) {
    fs.mkdir(buildDest + '/translations/');

    var translations = requireUncached('./app/assets/translations/translations.json');
    var key, languages, language, i, translated, langObj, temp, defaultLanguage;
    languages = translations.languages;
    translated = translations.translated_texts;
    defaultLanguage = translations.default_language;
    for (i = 0; i < languages.length; i++) {
        language = languages[i];
        langObj = '{';
        for (key in translated) {
            if (!translated.hasOwnProperty(key)) continue;
            //If a there is no translation for the given language, the default language will be used
            temp = translated[key][language];
            if (typeof translated[key][language] === 'undefined' || translated[key][language] === null || translated[key][language].length === 0) {
                temp = translated[key][defaultLanguage];
            }
            langObj += '"' + key + '":' + '"' + temp + '",';
        }
        langObj = langObj.substr(0, langObj.length - 1);
        langObj += '}';
        fs.writeFile(buildDest + '/translations/' + language + '.json', langObj);
        //console.log(langObj);
    }
    return cb();
});

gulp.task('favicon', function () {
    return gulp.src(['./favicon.ico'])
        .pipe(gulp.dest(buildDest + '/'));
});


gulp.task('html-files', function () {
    return gulp.src(['app/**/*.html', '!app/landing/index.html'])
        .pipe(flatten())
        .pipe(gulp.dest(buildDest + '/'));
});

gulp.task('images', function () {
    return gulp.src(['app/assets/img/**/*.png', 'app/assets/img/**/*.jpg', 'app/assets/img/**/*.svg'])
        .pipe(gulp.dest(buildDest + '/img'));
});

gulp.task('uglify', function () {
    return gulp.src(buildDest + '/script/**/*.js')
        .pipe(uglify())
        .on('error', handleError)
        .pipe(gulp.dest(buildDest + '/script/'));
});


gulp.task('inject', function () {
    var target = gulp.src([buildDest + '/index.html', buildDest + '/terms-clean.html']);
    var sources = gulp.src([buildDest + '/style/**/*.css', buildDest + '/script/**/*.js'], {
        read: false
    });
    var jqSrc = gulp.src(buildDest + '/script/head/jquery.min.js', {
        read: false
    });
    var headSrc = gulp.src(buildDest + '/script/head/head.js', {
        read: false
    });

    return target
        .pipe(inject(sources, {
            relative: true,
            quiet: true,
            addSuffix: '?v=' + nextVersionNumber
        }))
        .pipe(inject(jqSrc, {
            relative: true,
            quiet: true,
            name: 'jquery'
        }))
        .pipe(inject(headSrc, {
            relative: true,
            quiet: true,
            name: 'head',
            addSuffix: '?v=' + nextVersionNumber
        }))
        .pipe(gulp.dest(buildDest + '/'));
});


gulp.task('fonts', function () {
    return gulp.src(['bower_components/bootstrap/fonts/*.*', 'bower_components/components-font-awesome/fonts/*.*', 'app/assets/fonts/**/*.*'])
        .pipe(gulp.dest(buildDest + '/fonts'));
});

gulp.task('concat-bootstrap', function () {
    return gulp.src([buildDest + '/style/bower.css', 'bower_components/bootstrap/dist/css/bootstrap.min.css'])
        .pipe(concat('bower.css'))
        .on('error', handleError)
        .pipe(gulp.dest(buildDest + '/style/'))
});

gulp.task('concat-bower-css', function () {
    var f = filter('**/*.css');
    return gulp.src('bower.json')
        .pipe(mainBowerFiles())
        .pipe(f)
        .pipe(concat('bower.css'))
        .on('error', handleError)
        .pipe(gulp.dest(buildDest + '/style/'))
});

gulp.task('concat-main-bower-files', function () {
    //var f = filter(['**/*.js', '!bower_components/jquery/dist/jquery.js','!bower_components/jquery/dist/jquery.min.js', '!bower_components/moment/**/*.*']);
    var f = filter(['**/*.js', '!bower_components/jquery/dist/jquery.js', '!bower_components/jquery/dist/jquery.min.js',
        '!bower_components/cryptojslib/**/*.*', 'bower_components/cryptojslib/rollups/hmac-sha1.js',
        'bower_components/cryptojslib/components/enc-base-64.js', '!bower_components/moment/**/*.*'
    ]);
    return gulp.src('bower.json')
        .pipe(mainBowerFiles())
        .pipe(f)
        .pipe(concat('bower.js'))
        .on('error', handleError)
        .pipe(gulp.dest(buildDest + '/script/'))
});

gulp.task('concat-bower-js', ['concat-main-bower-files'], function () {
    var f = filter(['bower_components/moment/min/moment-with-locales.js']);
    return gulp.src([buildDest + '/script/bower.js', 'bower_components/moment/min/moment-with-locales.js'])
        .pipe(concat('bower.js'))
        .on('error', handleError)
        .pipe(gulp.dest(buildDest + '/script/'))
});



gulp.task('reload', function () {
    connect.reload();
});

gulp.task('all-done-log', function () {
    console.log('Done.');
});


gulp.task('rebuild', function (callback) {
    runSequence('clean', 'create-build-dir', ['html-files', 'images', 'fonts', 'js', 'head-js', 'clean-generate-translations', 'favicon'], 'css', 'sass', 'concat-bower-css', 'concat-bootstrap', 'concat-bower-js', 'inject', 'all-done-log', callback);
});

gulp.task('watch', function () {
    return gulp.watch(['config.js', 'app/**/*.html', 'app/assets/style/**/*.scss', 'app/assets/style/**/*.css', 'app/assets/img/**/*.*', 'app/**/*.js', 'app/**/*.json'], function (event) {
        gulp.start('rebuild');
    });
});



gulp.task('build', function (callback) {
    runSequence('clean', 'create-build-dir', ['increase-version-number', 'html-files', 'images', 'fonts', 'prod-js', 'head-js', 'clean-generate-translations', 'favicon'], 'css', 'sass', 'concat-bower-css', 'concat-bower-js', 'concat-bootstrap', 'inject', 'uglify', callback);
});

gulp.task('build-secondary', function (callback) {
    buildDest = './secondary';
    runSequence('clean', 'create-build-dir', ['increase-version-number', 'html-files', 'images', 'fonts', 'secondary-js', 'head-js', 'clean-generate-translations', 'favicon'], 'css', 'sass', 'concat-bower-css', 'concat-bower-js', 'concat-bootstrap', 'inject', 'uglify', callback);
});

gulp.task('build-serve', function (callback) {
    runSequence('clean', 'create-build-dir', ['html-files', 'images', 'fonts', 'prod-js', 'head-js', 'clean-generate-translations', 'favicon'], 'css', 'sass', 'concat-bower-css', 'concat-bower-js', 'concat-bootstrap', 'inject', 'uglify', 'connect', callback);
});

gulp.task('serve', function (callback) {
    runSequence('watch', 'clean', 'create-build-dir', ['html-files', 'images', 'fonts', 'js', 'head-js', 'clean-generate-translations', 'favicon'], 'css', 'sass', 'concat-bower-css', 'concat-bower-js', 'concat-bootstrap', 'inject', 'all-done-log', callback);
});

gulp.task('dist', function(callback){
    buildDest = '../backend/dist/public';
    runSequence('clean-dist','create-dist-dir', ['increase-version-number', 'html-files', 'images', 'fonts', 'prod-js', 'head-js', 'clean-generate-translations', 'favicon'], 'css', 'sass', 'concat-bower-css', 'concat-bower-js', 'concat-bootstrap', 'inject', 'uglify', callback);
    
});




gulp.task('dev-rebuild', function(callback){
    runSequence('clean','create-build-dir',['html-files', 'images','fonts', 'js','head-js',  'clean-generate-translations','favicon'], 'css','sass', 'concat-bower-css', 'concat-bootstrap','concat-bower-js','inject', 'reload', 'all-done-log', callback);
});

gulp.task('dev-watch', function(){
    return gulp.watch(['config.js','app/**/*.html', 'app/assets/style/**/*.scss','app/assets/style/**/*.css', 'app/assets/img/**/*.*', 'app/**/*.js', 'app/**/*.json'], function(event){
        gulp.start('dev-rebuild');
    });
});

gulp.task('dev-serve', function(callback){
    buildDest = '../backend/build/public';
    runSequence('dev-watch','clean','create-build-dir',['html-files', 'images','fonts', 'js','head-js',  'clean-generate-translations','favicon'], 'css','sass', 'concat-bower-css','concat-bower-js', 'concat-bootstrap','inject','all-done-log', callback);
});

