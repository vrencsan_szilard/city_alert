const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
// const WebpackCleanPlugin = require('webpack-clean-plugin');


module.exports = merge(common, {
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'app.js'
  },
  
  watchOptions: {
    ignored: [path.join(__dirname, 'node_modules'),path.join(__dirname, 'build/public'),]
  },
  plugins: [
    new CleanWebpackPlugin(['build'], {verbose: true, dry: false, exclude: ['public']}),
    // new WebpackCleanPlugin({on: 'compile', path:'./build'}),
    new webpack.BannerPlugin({banner: 'require("source-map-support").install();', raw: true, entryOnly: false})
  ],
  devtool: 'inline-source-map'
});

