/**
 * Created by LIH on 8/8/2017.
 */
module.exports = {
    SERVER_PORT                 : 1500,
    DATABASE_HOST               : 'localhost',
    DATABASE_USER               : 'root',
    DATABASE_PASS               : 'root',
    DATABASE_NAME               : 'cityalert',
    DATABASE_UPGRADE_VERSION    : '0.1',
    HOST                        : 'https://clujnow.e-primariaclujnapoca.ro',
    FRONTEND_URL                : 'https://clujnow.e-primariaclujnapoca.ro',
    BACKEND_PATH                : '/api',
    NOTIFICATION_TYPE_IMAGE_PATH: '/img/notification_types',
    MOBILE_TOKEN                : '275d3e5e6a6d3926686f5f263120466b3d436c2d5042797b76214f7329',
    CITY_HALL_TOKEN             : 'a5f23d5usz2q9av4qbd6u347a198sdf98u3mn289wef98jew989fjwe983',
    GEOCODING_API_KEY           : 'AIzaSyCAlB0s33qCspG2c7A6loO6aR3mrsMBfaY',
    FIREBASE_CONFIG: {
        apiKey           : "AIzaSyDArAyiMRnt2Zf0HrKSIXciBuuGvCqN56c",
        authDomain       : "cityalert-f4d9c.firebaseapp.com",
        databaseURL      : "https://cityalert-f4d9c.firebaseio.com",
        projectId        : "cityalert-f4d9c",
        storageBucket    : "cityalert-f4d9c.appspot.com",
        messagingSenderId: "182375028114"
    }
};

