/**
 * Created by LIH on 8/8/2017.
 */
module.exports = function (district, axios, logger) {
    var firebaseService = require('./firebase.service.js')(district, axios, logger);
    return {
        init        : firebaseService.init,
        getRef      : firebaseService.getRef,
        getOnce     : firebaseService.getOnce,
        query       : firebaseService.query,
        insert      : firebaseService.insert,
        update      : firebaseService.update,
        getUserToken: firebaseService.getUserToken,
        verifyIDToken: firebaseService.verifyIDToken,
        getIDToken: firebaseService.getIDToken,
        sendPushNotification: firebaseService.sendPushNotification,
        reverseGeocoding: firebaseService.reverseGeocoding
    };
};