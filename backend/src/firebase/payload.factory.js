(function () {
    var payloadField = {
        ios: 'notification',
        android: 'data'
    };

    function getPayLoadField(type) {
        return payloadField[type];
    }

    var Payload = module.exports = function Payload(type) {
        this.type = type;
        switch (type) {
            case 'ios':
                {
                    this.notification = {
                    };
                    break;
                }
                // 'category':'newCategory',
                //         'mutable_content':'true'
            default:
            case 'android':
                {
                    this.data = {
                    };
                    break;
                }
        }
    };

    Payload.prototype.setTopic = function (topic) {
        this.topic = this.type +'_'+topic;
    };
    // Payload.prototype.setImageUrl = function (url) {
    //     this[getPayLoadField[this.type]].imageUrl = this.type +'_'+topic;
    // };
    Payload.prototype.setField = function (field, value) {
        this[getPayLoadField(this.type)][field] = value + '';
    };
    Payload.prototype.getPayload = function(){
        var obj = {};
        obj[getPayLoadField(this.type)] =  this[getPayLoadField(this.type)];
        // console.log(this.topic, obj);
        return  obj;
    };
})();
