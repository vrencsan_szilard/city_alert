/**
 * Created by LIH on 8/8/2017.
 */
const admin         = require('firebase-admin'),
    // firebase        = require('firebase'),
    serviceAccount  = require('./serviceAccountKey.json'),
    Payload         = require('./payload.factory');
    _               = require('underscore');

var refs            = [], vars, app,db,defaultAuth,config;

function getRef(path) {
    if (_.isUndefined(refs[path])) {
        refs[path] = db.ref(path);
    }
    return refs[path];
}

function setPayloadsField(payloads, field, value){
    _.forEach(payloads, function(payload){
        payload.setField(field, value);
    });
}
function setPayloadsTopic(payloads, topic){
    _.forEach(payloads, function(payload){
        payload.setTopic(topic);
    });
}

function initPayloads(){
    var array = [];
    array.push(new Payload('android'));
    array.push(new Payload('ios'));
    return array;
}

module.exports = function(districtService, axios, logger){
    return {
        init: function (pConfig) {
            // Initialize the firebase app with a service account, granting admin privileges
            admin.initializeApp({
                credential: admin.credential.cert(serviceAccount),
                databaseURL: "https://cityalert-f4d9c.firebaseio.com"
            });
            config = pConfig;

            // db = admin.database();
            // defaultAuth = admin.auth();

            // app = firebase.initializeApp(config.FIREBASE_CONFIG);
        },
        getRef: function (path) {
            return getRef(path);
        },
        insert: function (path, data) {
            return this.getRef(path).push(data);
        },
        getUserToken: function (email, pass) {
            var promise, that = this;
            promise = new Promise(function (resolve, reject) {
                app.auth().signInWithEmailAndPassword(email, pass).then(function (user) {
                    app.auth().signOut();
                    admin.auth().createCustomToken(user.uid).then(function (customToken) {
                        resolve({
                            token: customToken,
                            uid: user.uid
                        });
                    }).catch(function (error) {
                        reject(error);
                    });
                }).catch(function (error) {
                    reject(error);
                });
            });
            return promise;
        },
        getOnce: function (path) {
            return this.getRef(path).once('value');
        },
        query: function (path, propertyName, propertyValue) {
            var ref = this.getRef(path),
                promise;
            promise = new Promise(function (resolve, reject) {
                ref.orderByChild(propertyName).equalTo(propertyValue).once("value", function (snapshot) {
                    resolve(snapshot);
                }, function (error) {
                    reject(error);
                });
            });
            return promise;
        },
        update: function (path, data) {
            var ref = this.getRef(path),
                promise;
            //promise = new Promise(function(resolve, reject){
            //    ref.update(data, function(){
            //        resolve();
            //    }, function(error){
            //        reject(error)
            //    })
            //});
            //return promise;
            return ref.update(data);
        },
        verifyIDToken: function (idToken) {
            return admin.auth().verifyIdToken(idToken);
        },
        getIDToken: function (email, password) {
            var promise;
            promise = new Promise(function (resolve, reject) {
                app.auth().signInWithEmailAndPassword(email, password).then(function () {
                    app.auth().currentUser.getIdToken().then(function (idToken) {
                        app.auth().signOut();
                        resolve({
                            token: idToken
                        });
                    }).catch(function (error) {
                        reject(error);
                    });
                }).catch(function (error) {
                    reject(error);
                });
            });
            return promise;
        },

        sendPushNotification: function (notificationRows) {
            var topic, promiseList = [], nrOfDistrictsReturned = 0, notificationID, imageUrl, districtList, payloads,
                options = {mutableContent: true}, notificationLedger = [];
            _.each(notificationRows, function(notification){
                //the notification id and the type image are the same for every notification
                notificationID                          = notification.id;
                imageUrl                                = config.HOST + config.NOTIFICATION_TYPE_IMAGE_PATH + '/' + notification.typeIMG;
                //one notification for each district
                _.each(notification.locations, function(location){
                    if (location === null || location.id === null || _.isUndefined(location.id)) {
                        //this is a city notification and not one for districts
                        payloads = initPayloads();
                        setPayloadsField(payloads, 'title', notification.subject);
                        setPayloadsField(payloads, 'imageUrl', imageUrl);
                        setPayloadsTopic(payloads, 'CITY_NOTIFICATION' + '_' + notification.topic); //city notification
                        setPayloadsField(payloads, 'body', notification.typeName); 
                        setPayloadsField(payloads, 'notificationId', notificationID); 
                        _.each(payloads, function(payload){
                            promiseList.push(admin.messaging().sendToTopic(payload.topic, payload.getPayload(), options));
                            logger.logNotification(payload);
                        });
                     } else {
                        //check if a notification was sent to the district
                        if (_.isUndefined(notificationLedger[location.id])) {
                            notificationLedger[location.id] = true; //mark districtID as sent
                            // payload = new Payload();
                            payloads = initPayloads();
                            setPayloadsField(payloads, 'title', notification.subject);
                            setPayloadsField(payloads, 'imageUrl', imageUrl);
                            setPayloadsTopic(payloads, location.id   + '_'  + notification.topic);
                            setPayloadsField(payloads, 'body', notification.typeName);
                            setPayloadsField(payloads, 'notificationId', notificationID); 
                            _.each(payloads, function(payload){
                                promiseList.push(admin.messaging().sendToTopic(payload.topic, payload.getPayload(), options));
                                logger.logNotification(payload);
                            });
                            //send a silent notification too because reasons
                            promiseList.push(admin.messaging().sendToTopic('CITY_HEALTH_SILENT',{data: {'notificationId': notificationID+'', 'distrcitId': location.id+''}}, {contentAvailable: true}));
                            logger.logNotification({topic:'CITY_HEALTH_SILENT', data: {'notificationId': notificationID+'', 'distrcitId': location.id+''}});
                        }
                    }
                });
            });
            // Send a message to devices subscribed to the provided topic.
            return Promise.all(promiseList);
        },
        reverseGeocoding: function(latitude, longitude){
            var found = false, splitAddress = '',
            promise = new Promise(function(resolve, reject){
                axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&key='+config.GEOCODING_API_KEY).then(function(result){
                    if (!_.isUndefined(result.data) && !_.isUndefined(result.data.results) && result.data.results.length > 0) {
                        try{
                            splitAddress = result.data.results[0].formatted_address.split(',');
                            if (splitAddress.length === 3) {
                                resolve(splitAddress[0]);
                            } else {
                                resolve(addressComponent.formatted_address);
                            }
                        } catch(error) {
                            resolve('');
                        }
                    } else {
                        resolve('');
                    }
                }).catch(function(error){
                    reject(error);
                });
            });
            return promise;
        }
    };
};