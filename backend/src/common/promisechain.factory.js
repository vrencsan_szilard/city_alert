/**
 * Created by LIH on 4/18/2017.
 */
var _ = require('underscore');

function log(id, obj, message, shouldLog){
    if (shouldLog) {
        console.log('['+id+'] '+
            (obj !== null ? obj.description : '') +
            ' '+message);
    }
}

var PromiseChain = module.exports = function PromiseChain(shouldLog, id) {
    this.promises = [];
    this.id = _.isUndefined(id) ? (Math.random() + '').replace('.','') : id;
    this.shouldLog = _.isUndefined(shouldLog) ? false : shouldLog;
    log(this.id, null, 'init', this.shouldLog);
};




/**
 * The promise must be a function if you want to flatten in chain, and a promise if you want to flatten at once
 * @param promise
 * @param description
 * @constructor
 */
PromiseChain.prototype.chain = function(p, description){
    var obj = {promise: p, description: description};
    log(this.id, obj, 'chained', this.shouldLog);
    this.promises.push(obj);
};


/**
 * Promises should be functions returning a promise
 * @returns {*}
 */
PromiseChain.prototype.flattenInChain = function(){
    var resolvedPromiseIndex = 0, n=this.promises.length, results=[], that = this, obj;
    var promise = new Promise(function(resolve, reject){
        function resolvePromise(index){
            if (!_.isUndefined(that.promises[index])) {
                obj = that.promises[index];
                log(that.id, obj, 'run', that.shouldLog);
                obj.promise().then(function (result) {
                    log(that.id, obj, 'success: '+JSON.stringify(result), that.shouldLog);
                    results[index] = result;
                    resolvePromise(index + 1);
                }, function (error) {
                    log(that.id, obj, 'fail ' + error, that.shouldLog);
                    reject(error);
                });
            } else {
                resolve(results);
            }
        }
        if (n > 0) {
            resolvePromise(0);
        } else {
            resolve(results);
        }
    });

return promise;

};

PromiseChain.prototype.flattenAllAtOnce = function(){
    var i= 0, n=this.promises.length, results=[], resolvedNr = 0, that = this;

    var promise = new Promise(function(resolve, reject){
        if (n > 0) {
            _.forEach(that.promises, function(obj, promiseIndex){
                log(that.id, obj, 'run', this.shouldLog);
                obj.promise.then(function success(result){
                    log(that.id, obj, 'success: '+ JSON.stringify(result), that.shouldLog);
                    results[promiseIndex] = result;
                    resolvedNr++;
                    if (resolvedNr === n) {
                        log(that.id, null, 'all done', that.shouldLog);
                        resolve(results);
                    }
                }, function error(error){
                    log(that.id, obj, 'fail ' + error, that.shouldLog);
                    reject(error);
                });
            });
        } else {
            resolve(results);
        }
    });

    return promise;
};



