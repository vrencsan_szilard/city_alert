module.exports = function(database, ino, _){
    var districtService = require('./district.service')(database, ino, _);
    return {
        getDistrict     : districtService.getDistrict,
        getDistrictByID : districtService.getDistrictByID
    };
};