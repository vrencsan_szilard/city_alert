module.exports = function (database, ino, _) {
    return {
        getDistrict: function (latitude, longitude) {
            var promise, districtsPromise, poly,resultDistrict = {},
                found = false,
                polygon = [];
            promise = new Promise(function (resolve, reject) {
                districtsPromise = database.query("SELECT * FROM districts");
                districtsPromise.then(function (districts) {
                    _.forEach(districts, function (district) {
                        if (!found && district.polygon !== null) {
                            polygon = JSON.parse(district.polygon);
                            _.forEach(polygon, function (coord) {
                                a = coord[0];
                                coord[0] = coord[1];
                                coord[1] = a;
                            });
                            poly = new ino.Geofence(polygon);
                            found = poly.inside([latitude, longitude]);
                            if (found) {
                                resultDistrict = district;
                            }
                        }
                    });
                    if (!found) {
                        reject({code: "DISTRICT_NOT_FOUND", latitude: latitude, longitude: longitude});
                    } else {
                        resolve({id: resultDistrict.id, name: resultDistrict.name});
                    }
                }).catch(function (error) {
                    reject(error);
                });
            });
            return promise;
        },
        getDistrictByID: function(id) {
            return database.queryWithData("SELECT * FROM `districts` WHERE ?", {id: id});
        }
    };
};