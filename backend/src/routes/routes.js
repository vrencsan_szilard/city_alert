module.exports = function(database, logger){
const _            = require('underscore'),
      path         = require('path'),
      bcrypt       = require('bcrypt-nodejs'),
      crypto       = require('crypto'),
      moment       = require('moment'),
      PromiseChain = require('../common/promisechain.factory');
var   conf;

function mergeRows(rows, idColumnOfMergedRow){
    var mergedResult = [], //the result that will be returned
        splitColumnName  = [], //will split a column name into this array
        currentRow       = {},
        mergeById        = idColumnOfMergedRow ? idColumnOfMergedRow : 'id', //the id column of the merged row
        processedRowData = {}, 
        indexOfRow       = 0,
        mergedColumns    = {}; //the temporary object which contains merged columns
    _.forEach(rows, function(row){
        currentRow = {};
        mergedColumns = {};
        //process the row by iterating through every column
        _.forEach(row, function(value, columnName){

            if (columnName.indexOf('merge_') === 0) {
                if (value !== null) {
                    splitColumnName = columnName.split('_'); //columnName looks like this: merge_districts_id, or merge_districts_name
                    //ex: mergedColumns.districts = {}
                    if (_.isUndefined(mergedColumns[splitColumnName[1]])) {
                        mergedColumns[splitColumnName[1]] = {};
                    }
                    //ex: mergedColumns.districts.id = 1, or mergedColumns.districts.name = 'Manastur'
                    mergedColumns[splitColumnName[1]][splitColumnName[2]] = value;
                } 
            } else {
                currentRow[columnName] = value;
            }

        });
        if (_.isUndefined(processedRowData[row[mergeById]])) {
            //add the processed row to the result array, it does not contain the merged columns yet
            mergedResult.push(currentRow);
            processedRowData[row[mergeById]] = mergedResult.length - 1; //store the index in the result array of the current row
        }

        indexOfRow = processedRowData[row[mergeById]];
        currentRow = mergedResult[indexOfRow];
     
        //let's update the results with merged columns for the current row
        _.forEach(mergedColumns, function(item, columnName){
            if (_.isUndefined(currentRow[columnName])) {
                //currentRow.districts = [];
                currentRow[columnName] = [];
            }
            //currentRow.districts.push(id: 1, name: 'Manastur');
            currentRow[columnName].push(item);
        });
    });
    return mergedResult;
}

function handleDatabasePromise(promise, req, res, shouldReturnResults){
    promise.then(function(result){
        if (shouldReturnResults) {
           return res.status(200).json(result);
        } else {
           return res.status(200).json({});
        }
    }).catch(function(error){
        logger.log(req, error);
       return res.status(400).json(error);
    });
}

/**
 * If there are field names like this: merge_xxx_yyy. They will be merged into an array of objects like this: xxx:[{yyy: value}]
 * This is handy for one to many relations. Ex: merge_distrircts_id, merge_districts_name will be districts:[{id:.., name:...}]
**/
function handleDatabasePromiseWithMerge(promise, req, res, idColumnOfMergedRow){
    var mergedResult; //the result that will be returned
    promise.then(function(result){
        //iterate through each row
        mergedResult = mergeRows(result, idColumnOfMergedRow);
        return res.status(200).json(mergedResult); 
    }).catch(function(error){
        logger.log(req, error);
       return res.status(400).json(error);
    });
}


function isAppAdminAuthenticated(req, res, next){
    if (!req.user || req.user.type !== 'APP_ADMIN') {
        return res.status(401).send('Unauthorized');

    } else {
        return next();
    }
}

function isAuthenticated(req, res, next){
    if (!req.user) {
        return res.status(401).send('Unauthorized');

    } else {
        return next();
    }
}

function mobileAuthorization(req, res, next) {
    const mobileToken  = conf.MOBILE_TOKEN,
          requestToken = req.headers.token;
    
    if (mobileToken !== requestToken) {
        return res.status(401).send('Unauthorized');
    } else {
        return next();
    }
}
function cityHallAuthorization(req, res, next) {
    const cityHallTOken  = conf.CITY_HALL_TOKEN,
          requestToken = req.headers.token;
    
    if (cityHallTOken !== requestToken) {
        return res.status(401).send('Unauthorized');
    } else {
        return next();
    }
}


// // route middleware to make sure
// function isLoggedIn(req, res, next) {

//     // if user is authenticated in the session, carry on
//     if (req.isAuthenticated())
//         return next();

//     // if they aren't redirect them to the home page
//     res.redirect('/#/login');
// }

// function loginMiddleWare(req, res, next){
//     console.log('login midd',req.isAuthenticated());
//     if (req.isAuthenticated()) {
//         res.redirect('/#/dashboard');
//     } else {
//         return next();
//     }
// }
return {
    init: function (app, config, firebase, passport, cron, districtService, notification) {
        conf = config;
        app.get('/api/test', function (req, res) {
            var a, b;
            if (process.env.NODE_ENV !== 'production') {
                console.log('Looks like we are in development mode!');
            }
           return res.status(200).send('Server running!');
        });

        // =====================================
        // LOGOUT ==============================
        // =====================================
        app.get('/api/logout', function (req, res) {
            req.logout();
            res.send('');
        });

        app.get('/api/is-authenticated', function (req, res) {
            if (req.user) {
                return res.status(200).json({
                    type: req.user.type ? req.user.type : 'ADMIN'
                });
            } else {
                return  res.status(401).send('Unauthorized');
            }
        });

        app.post('/api/user/create',isAuthenticated, function(req, res, next){
            var bridgeValues = [],
                promise, types = [];
            passport.authenticate('local-signup', function(err, user, info){
                if (err) {
                    // return next(err); // will generate a 500 error
                    return res.status(500).json(err);
                }
                  // Generate a JSON response reflecting authentication status
                if (! user) {
                    return res.status(403).send(info);
                } else {
                    if (req.body.notificationTypes && req.body.notificationTypes.length > 0) {
                        types = req.body.notificationTypes.split(' ');
                        _.forEach(types, function(notificationTypeID){
                            bridgeValues.push([user.id, notificationTypeID]);
                        });
                        promise = database.queryWithData('INSERT INTO  user_notification_types (userID, notificationTypeID) VALUES ?', [bridgeValues]);
                        promise.then(function(){
                            return res.send({type: user.type});
                        }).catch(function(error){
                            database.queryWithData("DELETE FROM `users` WHERE ?", {id: user.id});
                            return res.status(400).json(error);
                        });
                    } else {
                        return res.send({type: user.type});
                    }
                }
            })(req, res, next);
        });

	    // process the login form
        app.post('/api/login', function (req, res, next) {
            passport.authenticate('local-login', function (err, user, info) {
                if (err) {
                    return res.status(500).json(err);
                    // console.log('the err', err.code);
                    // return next(err); // will generate a 500 error
                }
                // Generate a JSON response reflecting authentication status
                if (!user) {
                    return res.status(403).send(info);
                }
                // ***********************************************************************
                // "Note that when using a custom callback, it becomes the application's
                // responsibility to establish a session (by calling req.login()) and send
                // a response."
                // Source: http://passportjs.org/docs
                // ***********************************************************************
                req.login(user, function(loginErr) {
                    if (loginErr) {
                        return next(loginErr);
                    }
                    return res.send({
                        type: user.type
                    });
                });

                // req.session.cookie.expires = false;
                req.session.cookie.maxAge = 1000 * 60 * 3;
                //remember me
                // if (req.body.remember) {
                //     req.session.cookie.maxAge = 1000 * 60 * 3;
                //   } else {
                //     req.session.cookie.expires = false;
                //   }
            })(req, res, next);
        });

        /**
         * GET USERS
         */
        app.get('/api/users',isAuthenticated, function(req, res){
            var promise = database.query("SELECT users.id AS id, username, type, notification_types.id AS merge_notificationTypes_id, notification_types.name AS merge_notificationTypes_name FROM `users`"+
                                         " LEFT JOIN `user_notification_types` ON users.id = user_notification_types.userID OR user_notification_types.userID IS NULL"+
                                         " LEFT JOIN `notification_types` ON user_notification_types.notificationTypeID = notification_types.id");
            if (req.isAuthenticated() && req.user.type === 'APP_ADMIN') {
                if (req.user.type === 'APP_ADMIN') {
                    // return res.status(200)
                    return handleDatabasePromiseWithMerge(promise, req, res);
                } else {
                    return res.status(401).send('Unauthorized');
                }
            } else {
               return res.status(401).send('Unauthorized');
            }
        });
      
        /**
         * UPDATE USER
         */
        app.put('/api/user', isAuthenticated, function(req, res){
            var data              = req.body,
                userID            = req.body.id,
                notificationTypes = req.body.notificationTypes,
                promise, types, 
                bridgeValues      = [],
                deletePromise     = function(){return database.queryWithData("DELETE FROM `user_notification_types` WHERE ?", {userID: userID})},
                insertPromise,
                promiseChain      = new PromiseChain();

            delete data.id;
            delete data.visible;
            delete data.notificationTypes;
            if (req.isAuthenticated()) {
                if (req.user.type === 'APP_ADMIN') {
                    // return res.status(200)
                    if (!_.isUndefined(data.password)) {
                        data.password = bcrypt.hashSync(data.password, null, null);  // use the generateHash function in our user model
                    }
                    promise = function(){return database.queryWithData("UPDATE users SET ? WHERE ?", [data, {id: userID}]);};
                    promiseChain.chain(deletePromise, 'delete bridge');
                    promiseChain.chain(promise, 'update user');
                    if (notificationTypes && notificationTypes.length) {
                        types = notificationTypes.split(' ');
                        _.forEach(types, function(notificationTypeID){
                            bridgeValues.push([userID, notificationTypeID]);
                        });
                        insertPromise = function(){ return database.queryWithData('INSERT INTO  user_notification_types (userID, notificationTypeID) VALUES ?', [bridgeValues])};
                        promiseChain.chain(insertPromise, 'inser bridge');
                    }
                    return handleDatabasePromise(promiseChain.flattenInChain(), req, res, false);
                } else {
                    return res.status(401).send('Unauthorized');
                }
            } else {
               return res.status(401).send('Unauthorized');
            }
        });

        /**
         * DELETE USER
         */
        app.delete('/api/user/:userID', isAuthenticated, function (req, res) {
            if (req.isAuthenticated() && req.user.type === 'APP_ADMIN') {
                var userID              = req.params.userID,
                    promiseChain        = new PromiseChain(),
                    deleteBridgePromise = function(){return database.queryWithData("DELETE FROM `user_notification_types` WHERE ?", {userID: userID});},
                    deleteUser          = function(){return database.queryWithData("DELETE FROM `users` WHERE ?", {id: userID});};
                promiseChain.chain(deleteBridgePromise, 'delete bridge');
                promiseChain.chain(deleteUser, 'delete user');
                return handleDatabasePromise(promiseChain.flattenInChain(), req, res, false);
            } else {
                return  res.status(401).send('Unauthorized');
            }
        });

        /**
         * TERMS AND CONDITIONS
         */
        app.get('/api/terms', function(req, res){
            var promise = database.query('SELECT * FROM `terms`');
            return handleDatabasePromise(promise, req, res, true);
        });
    
        app.put('/api/terms', isAuthenticated, function(req, res){
            var data    = req.body;
                promise = database.queryWithData("UPDATE `terms` SET ? WHERE ?", [data, {id: data.id}]);
            return handleDatabasePromise(promise, req, res, true);
        });

        /**
         * NOTIFICATION TYPE
         */
        app.post('/api/notification/type', isAuthenticated, function (req, res) {
            var data    = req.body,
                topic   = 'CITY_ALERT_' + crypto.randomBytes(10).toString('hex'),
                promise;

            data.topic = topic;    
            promise    = database.queryWithData("INSERT INTO `notification_types` SET ?", data);
            
            promise.then(function (result) {
               return res.status(200).json({
                    id: result.insertId
                });
            }).catch(function (error) {
                if (error.code === 'ER_DUP_ENTRY') {
                    data.topic = 'CITY_ALERT_' + crypto.randomBytes(10).toString('hex');
                    database.queryWithData("INSERT INTO `notification_types` SET ?", data).then(function(result2){
                        return res.status(200).json({
                            id: result2.insertId
                        });
                    }).catch(function(error){
                        if (error.code === 'ER_DUP_ENTRY') {
                            data.topic = 'CITY_ALERT_' + crypto.randomBytes(5).toString('hex') + moment().format('x');
                            database.queryWithData("INSERT INTO `notification_types` SET ?", data).then(function(result3){
                                return res.status(200).json({
                                    id: result3.insertId
                                });
                            }).catch(function(error){
                                return res.status(400).json(error);                      
                            });
                        } else {
                            return res.status(400).json(error);         
                        }
                    });
                } else {
                    return res.status(400).json(error);
                }
            });
        });

        app.put('/api/notification/type/:notificationTypeID', isAuthenticated, function (req, res) {
            var data                = req.body,
                notificationTypeID  = req.params.notificationTypeID,
                promise;
            if (!_.isUndefined(data.id)) {
                delete data.id;
            }
            promise = database.queryWithData("UPDATE `notification_types` SET ? WHERE ?", [data, {
                id: notificationTypeID
            }]);
            return handleDatabasePromise(promise, req, res, false);
        });
        
        app.delete('/api/notification/type/:notificationTypeID',isAuthenticated, function(req, res){
            var notificationTypeID = req.params.notificationTypeID, 
                promise            = database.queryWithData("DELETE FROM `notification_types`  WHERE ?", {id: notificationTypeID});
            return handleDatabasePromise(promise, req, res, false);
        });

        app.get('/api/notification/types',isAuthenticated, function(req, res){
            var promise, 
                user  = req.user,
                query = ('SELECT * FROM `notification_types`');
            if (req.user.type !== 'APP_ADMIN') {
                query += " INNER JOIN `user_notification_types` ON userID = ? AND notificationTypeID = notification_types.id";
                promise = database.queryWithData(query, user.id);
            } else {
                promise = database.query(query);
            }
            return handleDatabasePromise(promise, req, res, true);
        });

        /**
         * DISTRICTS
         */

        app.get('/api/districts', isAuthenticated, function (req, res) {
            var promise = database.query('SELECT * FROM `districts`');
            return handleDatabasePromise(promise, req, res, true);
        });

        app.post('/api/district', isAuthenticated, function (req, res) {
            var data = req.body;
            districtService.getDistrict(data.latitude, data.longitude).then(function(result){
                return res.status(200).json(result);
            }).catch(function(error){
                return res.status(404).json(error);
            });
        });

        /**
         * NOTIFICATIONS
         */

        app.post('/api/notification',isAuthenticated, function (req, res) {
            return notification.insert(req, res, false);
        });

        app.get('/api/notifications', isAuthenticated, function (req, res) {
            var promise = database.query('SELECT * FROM `notifications`');
            return handleDatabasePromise(promise, req, res, true);
        });

        app.get('/api/grid/notifications', isAuthenticated, function (req, res) {
            var user    = req.user, promise,
                query   = "SELECT notifications.*, notification_types.img AS typeIMG, notification_types.name AS typeName, districts.id AS merge_districts_id, districts.name AS merge_districts_name, " + 
            " notifications_districts.address AS merge_districts_address FROM notifications" +
            " LEFT JOIN notification_types ON notifications.typeID = notification_types.id" +
            " LEFT JOIN notifications_districts ON notifications.id = notifications_districts.notificationID" +
            " LEFT JOIN districts ON notifications_districts.districtID = districts.id";
            if (req.user.type !== 'APP_ADMIN') {
                query += " INNER JOIN user_notification_types ON userID = ? AND notificationTypeID = notification_types.id";
                promise = database.queryWithData(query, user.id);
            } else {
                promise = database.query(query);
            }
            return handleDatabasePromiseWithMerge(promise, req, res);
        });

        app.get('/api/notification/:notificationID', isAuthenticated, function (req, res) {
            var notificationID  = req.params.notificationID,
                query           = "SELECT notifications.*, notifications_districts.districtID AS merge_locations_districtID,"+
                                  " notifications_districts.address AS merge_locations_address," +
                                  " notifications_districts.latitude AS merge_locations_latitude, notifications_districts.longitude AS merge_locations_longitude FROM notifications"+
                                  " LEFT JOIN notifications_districts ON notifications.id = notifications_districts.notificationID",
                promise, user   = req.user;

            if (req.user.type !== 'APP_ADMIN') {
                query += " INNER JOIN user_notification_types ON notifications.typeID = notificationTypeID AND  userID = ?  WHERE notifications.id = ?";
                promise = database.queryWithData(query, [user.id, notificationID]);
            } else {
                query += " WHERE notifications.id = ?";
                promise = database.queryWithData(query, notificationID);
            }
            return handleDatabasePromiseWithMerge(promise, req, res);
        });

        app.delete('/api/notification/:notificationID', isAuthenticated, function (req, res) {
            var notificationID      = req.params.notificationID,
                promiseChain        = new PromiseChain(),
                deleteBridgePromise = function(){return database.queryWithData("DELETE FROM `notifications_districts` WHERE ?", {notificationID: notificationID});},
                deleteNotification  = function(){return database.queryWithData("DELETE FROM `notifications`  WHERE ?", {
                    id: notificationID
                });};
            promiseChain.chain(deleteBridgePromise, 'delete bridge');
            promiseChain.chain(deleteNotification, 'delete notification');
            return handleDatabasePromise(promiseChain.flattenInChain(), req, res, false);
        });
        app.post('/api/notifications/delete', isAuthenticated, function (req, res) {
            var notifications       = req.body.notifications,
                promiseChain        = new PromiseChain(),
                deleteBridgePromise = function(){return database.queryWithData("DELETE FROM `notifications_districts` WHERE notificationID IN (?)", [notifications]);},
                deleteNotification  = function(){return database.queryWithData("DELETE FROM `notifications`  WHERE id IN (?)", [notifications]);};
            promiseChain.chain(deleteBridgePromise, 'delete bridge');
            promiseChain.chain(deleteNotification, 'delete notifications');
            return handleDatabasePromise(promiseChain.flattenInChain(), req, res, false);
        });

        app.put('/api/notification/:notificationID', isAuthenticated, function (req, res) {
            var data               = req.body,
                notificationID     = req.params.notificationID,
                locations          = req.body.locations,
                addressIndex       = 0,
                promiseChain       = new PromiseChain(),
                addressChain       = new PromiseChain(),
                bridgeValues       = [],
                deleteBridge       = function(){return database.queryWithData("DELETE FROM `notifications_districts` WHERE ?", {notificationID: notificationID});},
                updateBridge       = null,
                sendNotification   = null,
                isCityNotification = _.isUndefined(req.body.locations) || req.body.locations.length === 0;
                updateNotification = function(){return database.queryWithData("UPDATE `notifications` SET ? WHERE ?", [data, {id: notificationID}]);};
            
            if (!_.isUndefined(data.id)) {
                delete data.id;
            }
            if (data.schedule === 'now' || moment(data.schedule ,'x').isSameOrBefore(moment())) {
                data.schedule = moment().subtract(1, 'minutes').format('x');
                sendNotificationNow = true;
            } 
            delete data.locations;

            if (!isCityNotification) {
                _.forEach(locations, function(location){
                    if (_.isUndefined(location.address)) {
                        addressChain.chain(firebase.reverseGeocoding(location.latitude, location.longitude));
                    }
                });
            }

            addressChain.flattenAllAtOnce().then(function(addressResults){
                addressIndex = 0;
                if (!isCityNotification) {
                    _.forEach(locations, function(location){
                        if (_.isUndefined(location.address)) {
                            location.address = addressResults[addressIndex++];
                        }
                        bridgeValues.push([notificationID, location.districtID, location.latitude, location.longitude, location.address]);
                    });
                    updateBridge = function(){return database.queryWithData('INSERT INTO  notifications_districts (notificationID, districtID, latitude, longitude, address) VALUES ?', [bridgeValues]);};
                }
                sendNotification = function(){return cron.sendNotification();};
    
                promiseChain.chain(deleteBridge);
                if (!isCityNotification) {
                    promiseChain.chain(updateBridge);
                }
                promiseChain.chain(updateNotification);
                promiseChain.chain(sendNotification);
                return handleDatabasePromise(promiseChain.flattenInChain(), req, res, false);
            }).catch(function(error){
                return res.status(404).json(error);
            });
        });

        app.get('/api/address/:lat/:lng', isAuthenticated, function (req, res) {
            firebase.reverseGeocoding( req.params.lat, req.params.lng).then(function(address){
                res.status(200).json({address: address});
            }).catch(function(error){
                res.status(400).send(error);
            });
        });

        app.get('/api/log', isAuthenticated, function (req, res) {
            var promise         = database.queryWithData("SELECT * FROM log");
            return handleDatabasePromise(promise, req, res, true);
        });
        app.delete('/api/log',isAuthenticated, function (req, res) {
            var promise         = database.queryWithData("DELETE FROM log");
            return handleDatabasePromise(promise, req, res, false);
        });

        
        /**
         * MOBILE
         */
        app.post('/api/mobile/notifications', mobileAuthorization, function(req, res){
            var districtIDList = req.body,
                query          = 'SELECT notifications.*, notification_types.img AS typeIMG, notification_types.name AS typeName, districts.name AS merge_locations_district, notifications_districts.address AS merge_locations_address, districts.id AS merge_locations_districtID,'+
                ' notifications_districts.latitude AS merge_locations_latitude, notifications_districts.longitude AS merge_locations_longitude, notifications_districts.id AS merge_locations_id'+
                ' FROM notifications'+
                ' LEFT JOIN notification_types ON notifications.typeID = notification_types.id'+
                ' LEFT JOIN notifications_districts ON notifications.id = notifications_districts.notificationID' +
                ' LEFT JOIN districts ON notifications_districts.districtID = districts.id'+
                ' WHERE ((notifications.activeFrom <= ? AND notifications.activeUntil >= ?) OR (notifications.activeFrom >= ?)) AND notifications.sentAt IS NOT NULL AND (notifications_districts.districtID IS NULL OR notifications_districts.districtID IN ('+districtIDList.join()+'))',
                promise, m     = moment().format('x');
            
            promise = database.queryWithData(query, [m,m,m]);
            return handleDatabasePromiseWithMerge(promise, req, res);
        });

        app.post('/api/mobile/notification/:notificationID/clicked', mobileAuthorization, function(req, res){
            var notificationID  = req.params.notificationID;
            return notification.getByID(req, res, notificationID);
        });
        
        app.get('/api/mobile/notifications',mobileAuthorization, function(req, res){
            var query          = 'SELECT notifications.*, notification_types.img AS typeIMG, notification_types.name AS typeName, districts.name AS merge_locations_district, notifications_districts.address AS merge_locations_address, districts.id AS merge_locations_districtID,'+
                ' notifications_districts.latitude AS merge_locations_latitude, notifications_districts.longitude AS merge_locations_longitude, notifications_districts.id AS merge_locations_id'+
                ' FROM notifications'+
                ' LEFT JOIN notification_types ON notifications.typeID = notification_types.id'+
                ' LEFT JOIN notifications_districts ON notifications.id = notifications_districts.notificationID' +
                ' LEFT JOIN districts ON notifications_districts.districtID = districts.id'+
                ' WHERE ((notifications.activeFrom <= ? AND notifications.activeUntil >= ?) OR (notifications.activeFrom >= ?)) AND notifications.sentAt IS NOT NULL',
                promise, m     = moment().format('x');
            
            promise = database.queryWithData(query, [m,m,m]);
            return handleDatabasePromiseWithMerge(promise, req, res);
        });

        app.get('/api/mobile/districts', mobileAuthorization, function (req, res) {
            var promise = database.query('SELECT * FROM `districts`');
            return handleDatabasePromise(promise, req, res, true);
        });


        app.get('/api/mobile/notification/types', mobileAuthorization,  function(req, res){
            var promise = database.query('SELECT * FROM `notification_types`');
            return handleDatabasePromise(promise, req, res, true);
        });


        app.get('/api/mobile/notification/:notificationID', mobileAuthorization, function (req, res) {
            var notificationID  = req.params.notificationID,
                promise         = database.queryWithData('SELECT notifications.*, notifications_districts.districtID AS merge_locations_districtID,'+
                ' notification_types.img AS typeIMG, notification_types.name AS typeName, '+
                ' notifications_districts.address AS merge_locations_address, districts.name AS merge_locations_district,notifications_districts.id AS merge_locations_id,' +
                ' notifications_districts.latitude AS merge_locations_latitude, notifications_districts.longitude AS merge_locations_longitude FROM notifications'+
                ' LEFT JOIN notification_types ON notifications.typeID = notification_types.id'+
                ' LEFT JOIN notifications_districts ON notifications.id = notifications_districts.notificationID'+
                ' LEFT JOIN districts ON notifications_districts.districtID = districts.id  WHERE notifications.id = ?', notificationID);
            return handleDatabasePromiseWithMerge(promise, req, res);
        });


        if (process.env.NODE_ENV !== 'production') {
            app.get('/api/mobile/push',  function(req, res){
                var promise = cron.sendNotification();
                return handleDatabasePromise(promise, req, res, true);
            });
        }

        /**
         * CITY HALL
         */

        app.post('/api/city-hall/notification', cityHallAuthorization, function(req, res){
            return notification.insert(req, res, true);
        });

        /**
         * STATIC FILES
         */
        app.get('*', function (req, res) {
          return res.sendFile(path.resolve('./public/index.html'));
        });
    
        app.listen(config.SERVER_PORT, function () {
            console.log('server listening on port ', config.SERVER_PORT);
        });
    }
};
}