module.exports = function(database, logger){
    var routes = require('./routes.js')(database, logger);
    return {
        init: routes.init
    }
}