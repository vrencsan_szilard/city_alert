// config/passport.js

// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;

// load up the user model
// var mysql = require('mysql');
var bcrypt = require('bcrypt-nodejs');
// var dbconfig = require('./database');
// var connection = mysql.createConnection(dbconfig.connection);

// connection.query('USE ' + dbconfig.database);
// expose this function to our app using module.exports
module.exports = function(passport, database) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        var allowedNotificationTypes = [];
        database.queryWithData("SELECT * FROM users WHERE id = ? ",[id], function(err, rows){            
            if (err){
                done(err, rows[0]);
            } else {
                if (rows[0].type === 'APP_ADMIN') {
                    // all is well, return successful user
                    return done(null, rows[0]);
                } else {
                    database.queryWithData("SELECT notificationTypeID FROM `user_notification_types` WHERE ?", {userID: id}, function(err, allowedNotificationTypeRows){
                        allowedNotificationTypeRows.forEach(function(notificationType){
                            allowedNotificationTypes.push(notificationType.notificationTypeID);
                        });
                        rows[0].notificationTypes = allowedNotificationTypes;
                        // all is well, return successful user
                        return done(null, rows[0]);
                    });
                }
            }
        }).catch(function(err){
            done(err);
        });
        // database.queryWithData("SELECT * FROM users WHERE id = ? ", [id]).then(function(rows){
        //     done(null, rows[0]);
        // }).catch(function(err){
        //     done(err);
        // });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use(
        'local-signup',
        new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {
            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            database.queryWithData("SELECT * FROM users WHERE username = ?",[username], function(err, rows) {
                if (err)
                    return done(err);
                if (rows.length) {
                    return done(null, false, {code: 'auth/user-already-in-use'});
                } else {
                    // if there is no user with that username
                    // create the user
                    var newUserMysql = {
                        username: username,
                        password: bcrypt.hashSync(password, null, null),  // use the generateHash function in our user model
                        type: req.body.type
                    };

                    var insertQuery = "INSERT INTO users ( username, password, type ) values (?,?,?)";

                    database.queryWithData(insertQuery,[newUserMysql.username, newUserMysql.password, newUserMysql.type],function(err, rows) {
                        newUserMysql.id = rows.insertId;

                        return done(null, newUserMysql);
                    });
                }
            });
        })
    );

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use(
        'local-login',
        new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) { // callback with email and password from our form
            var allowedNotificationTypes = [];
            database.queryWithData("SELECT * FROM users WHERE username = ?",[username], function(err, rows){
                if (err)
                    return done(err);
                if (!rows.length) {
                    return done(null, false, {code: 'auth/user-not-found'});
                }

                // if the user is found but the password is wrong
                if (!bcrypt.compareSync(password, rows[0].password))
                    return done(null, false, {code: 'auth/wrong-password'}); // create the loginMessage and save it to session as flashdata

                if (rows[0].type === 'APP_ADMIN') {
                    // all is well, return successful user
                    return done(null, rows[0]);
                } else {
                    database.queryWithData("SELECT notificationTypeID FROM `user_notification_types` WHERE ?", {userID: rows[0].id}, function(err, allowedNotificationTypeRows){
                        allowedNotificationTypeRows.forEach(function(notificationType){
                            allowedNotificationTypes.push(notificationType.notificationTypeID);
                        });
                        rows[0].notificationTypes = allowedNotificationTypes;
                        // all is well, return successful user
                        return done(null, rows[0]);
                    });
                }
                
            });
        })
    );
};
