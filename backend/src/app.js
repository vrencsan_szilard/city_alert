var express      = require('express'),
    bodyParser   = require('body-parser'),
    cors         = require('cors'),
    _            = require('underscore'),
    database     = require('./database/index'),
    moment       = require('moment'),
    logger       = require('./logger/index')(database, moment),
    routes       = require('./routes/index')(database, logger),
    app          = express(),
    death        = require('death')({uncaughtException: true}),
    path         = require('path'),
    session      = require('express-session'),
    cookieParser = require('cookie-parser'),
    passport     = require('passport'),
    flash        = require('flash'),
    ino          = require('in-n-out'),
    axios        = require('axios'),
    district     = require('./districts/index')(database, ino, _),
    firebase     = require('./firebase/index')(district, axios, logger),
    promiseChain = require('./common/promisechain.factory'),
    cron         = require('./cron/cron.service')(firebase, database, promiseChain, logger),
    notification = require('./notification/index')(firebase, database ,logger, cron, district, moment, promiseChain),
    config, crontTask;

if (process.env.NODE_ENV !== 'production') {
    config = require('../config.dev.js');
} else {
    config = require('../config.prod.js');
}

database.init(config);
require('./passport/passport.config')(passport, database); // pass passport for configuration

function middleware(req, res, next) {
    var allowedOrigins = [],
        origin         =  req.headers.origin;
    allowedOrigins.push(config.FRONTEND_URL);

    // Website you wish to allow to connect
    if(allowedOrigins.indexOf(origin) > -1){
        res.setHeader('Access-Control-Allow-Origin', origin);
   }
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,token');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    return next();
}
function init() {
    app.use(cookieParser());
    app.use(express.static(path.resolve('./public')));    
    firebase.init(config);
    // app.use(cors());
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());
    app.use(bodyParser.json({ type: 'application/api+json' }));

    app.use(middleware);

    // required for passport
    app.use(session({
	    secret: 'cityalertsecretmessageqwerty',
	    resave: true,
	    saveUninitialized: true
    } )); // session secret
    app.use(passport.initialize());
    app.use(passport.session()); // persistent login sessions
    // app.use(flash()); // use connect-flash for flash messages stored in session
    routes.init(app, config, firebase, passport, cron, district, notification);
    crontTask = cron.start();
}


init();

//cleanup
var off_death = death(function(signal, err){
    // database.endConnection();
    off_death();
    crontTask.destroy();
});