var upgradeSqlFile;
    importer  = require('node-mysql-importer'),
    path      = require('path'),
    fs        = require('fs'),
    fileFound = true,
    version   = '0.0'; 

if (process.env.NODE_ENV !== 'production') {
    config = require('../../config.dev.js');
} else {
    config = require('../../config.prod.js');
}

function increaseVersion(version){
    var number = parseInt(version.split('.').join('')) + 1;
        newVersion = (number+'').split('').join('.');
    while (newVersion.length < version.length) {
        newVersion = '0.' + newVersion;
    }
    console.log(version + ' -> ' + newVersion);
    return newVersion;
}

function sqlUpgrade(toVersion){
    var version;
    upgradeSqlFile = path.join(__dirname, 'upgrade_'+toVersion+'.sql');
    if (fs.existsSync(upgradeSqlFile)) { 
        importer.importSQL(upgradeSqlFile).then(function () {
            version = increaseVersion(toVersion);
            sqlUpgrade(version);
        }).catch(function (error) {
            console.log('Error: ', error);
        });
    }
}

function upgrade(config) {
    importer.config({
        host    : config.DATABASE_HOST,
        user    : config.DATABASE_USER,
        password: config.DATABASE_PASS,
        database: config.DATABASE_NAME,
    });
    if (typeof config.DATABASE_UPGRADE_VERSION !== 'undefined') {
        version = increaseVersion(config.DATABASE_UPGRADE_VERSION);
        sqlUpgrade(version);
    }
}

upgrade(config);