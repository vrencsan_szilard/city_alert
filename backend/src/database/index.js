var databaseService = require('./database.service.js');
module.exports = {
    init: databaseService.init,
    query: databaseService.query,
    queryWithData: databaseService.queryWithData
};