SET FOREIGN_KEY_CHECKS = 0;
drop table if exists app_admins;
drop table if exists user_notification_types;
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE `cityalert`.`user_notification_types` (`userID` INT NOT NULL , `notificationTypeID` INT NOT NULL) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_bin;
