const   mysql    = require('mysql'),
        path     = require('path');
        _        = require('underscore');
var pool ,dbConfig;

function createTables(config){
    importer.config({
        host    : config.DATABASE_HOST,
        user    : config.DATABASE_USER,
        password: config.DATABASE_PASS,
        database : config.DATABASE_NAME     
    });
    initSqlFile = path.join(__dirname,'init.sql');
    importer.importSQL('init.sql').then(function(){
    }).catch(function(error){console.log('err', error);});
}


/**
 * If there are field names like this: merge_xxx_yyy. They will be merged into an array of objects like this: xxx:[{yyy: value}]
 * This is handy for one to many relations. Ex: merge_distrircts_id, merge_districts_name will be districts:[{id:.., name:...}]
**/
function mergeRows(rows, idColumnOfMergedRow){
    var mergedResult     = [], //the result that will be returned
        splitColumnName  = [], //will split a column name into this array
        currentRow       = {},
        mergeById        = idColumnOfMergedRow ? idColumnOfMergedRow : 'id', //the id column of the merged row
        processedRowData = {}, 
        indexOfRow       = 0,
        mergedColumns    = {}; //the temporary object which contains merged columns

        //iterate through each row
        _.forEach(rows, function(row){
            currentRow = {};
            mergedColumns = {};

            //process the row by iterating through every column
            _.forEach(row, function(value, columnName){
                if (columnName.indexOf('merge_') === 0) {
                    splitColumnName = columnName.split('_'); //columnName looks like this: merge_districts_id, or merge_districts_name
                    //ex: mergedColumns.districts = {}
                    if (_.isUndefined(mergedColumns[splitColumnName[1]])) {
                        mergedColumns[splitColumnName[1]] = {};
                    }
                    //ex: mergedColumns.districts.id = 1, or mergedColumns.districts.name = 'Manastur'
                    mergedColumns[splitColumnName[1]][splitColumnName[2]] = value;
                } else {
                    currentRow[columnName] = value;
                }
            });

            if (_.isUndefined(processedRowData[row[mergeById]])) {
                //add the processed row to the result array, it does not contain the merged columns yet
                mergedResult.push(currentRow);
                processedRowData[row[mergeById]] = mergedResult.length - 1; //store the index in the result array of the current row
            }

            indexOfRow = processedRowData[row[mergeById]];
            currentRow = mergedResult[indexOfRow];
         
            //let's update the results with merged columns for the current row
            _.forEach(mergedColumns, function(item, columnName){
                if (_.isUndefined(currentRow[columnName])) {
                    //currentRow.districts = [];
                    currentRow[columnName] = [];
                }
                //currentRow.districts.push(id: 1, name: 'Manastur');
                currentRow[columnName].push(item);
            });
        });
        return mergedResult;

}

module.exports = {
    init: function(config){
        pool = mysql.createPool({
            connectionLimit : 100, //important
            host     : config.DATABASE_HOST,
            user     : config.DATABASE_USER,
            password : config.DATABASE_PASS,
            database : config.DATABASE_NAME,
            debug    : false            
        });
        // createTables(config);
    },
    queryWithData2: function (query, data, callback) {
        var promise;
        promise = new Promise(function (resolve, reject) {
            pool.getConnection(function (err, connection) {
                if (err) {
                    if (_.isFunction(callback)) {
                        callback({
                            code: 100,
                            status: "Error in connection database"
                        });
                    }
                    reject({
                        code: 100,
                        status: "Error in connection database"
                    });
                }
                connection.query(query, data, function (err, rows) {
                    if (!err) {
                        if (_.isFunction(callback)) {
                            callback(null, rows);
                        }
                        resolve(rows);
                    } else {
                        if (_.isFunction(callback)) {
                            callback(err);
                        }
                        reject(err);
                    }
                });
                //     connection.on('error', function(err) {   
                //         reject({"code" : 100, "status" : "Error in connection database"});
                //   })
                connection.on('error', function () {
                    connection.release();
                    pool.end();
                    reject({"code" : 100, "status" : "Error in connection database"});
                });
                connection.on('end', function () {
                    connection.release();
                    pool.end();
                });
            });
        });

        return promise;
        },
    queryWithData: function (query, data, callback, mergeData) {
        var promise;
        promise = new Promise(function (resolve, reject) {
            pool.query(query, data, function(err, rows, fields){
                if (err) {
                    if (_.isFunction(callback)) {
                        callback(err);
                    }
                    return reject(err);
                }
                if (!_.isUndefined(mergeData)) {
                    rows = mergeRows(rows, mergeData.idColumnOfMergedRow);
                }
                if (_.isFunction(callback)) {
                    callback(null, rows);
                }
                return resolve(rows);
            });
        });

        return promise;
        },
        query2: function (query) {
            var promise;
            promise = new Promise(function (resolve, reject) {
                pool.getConnection(function (err, connection) {
                    if (err) {
                        reject({
                            code: 100,
                            status: "Error in connection database"
                        });
                    }
                    connection.query(query, function (err, rows) {
                        connection.release();
                        if (!err) {
                            resolve(rows);
                        } else {
                            reject(err);
                        }
                    });
                    //     connection.on('error', function(err) {      
                    //         reject({"code" : 100, "status" : "Error in connection database"});
                    //   });
                    connection.on('error', function () {
                        connection.release();
                        pool.end();
                        reject({
                            "code": 100,
                            "status": "Error in connection database"
                        });
                    });
                    connection.on('end', function () {
                        connection.release();
                        pool.end();
                    });
                });
            });

            return promise;
        },
        query: function (query, mergeData) {
            var promise;
            promise = new Promise(function (resolve, reject) {
                pool.query(query, function(err, rows, fields){
                    if (err) {
                        console.log('err',err);
                        return reject(err);
                    }
                    if (!_.isUndefined(mergeData)) {
                        rows = mergeRows(rows, mergeData.idColumnOfMergedRow);
                    }
                    return resolve(rows);
                });
            });

            return promise;
        }
        };