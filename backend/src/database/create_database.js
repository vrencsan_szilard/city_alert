var initSqlFile;
    importer = require('node-mysql-importer'),
    path     = require('path');

if (process.env.NODE_ENV !== 'production') {
    config = require('../../config.dev.js');
} else {
    config = require('../../config.prod.js');
}



function createTables(config) {
    importer.config({
        host    : config.DATABASE_HOST,
        user    : config.DATABASE_USER,
        password: config.DATABASE_PASS,
        database: config.DATABASE_NAME,
    });
    initSqlFile = path.join(__dirname, 'init.sql');
    importer.importSQL(initSqlFile).then(function () {}).catch(function (error) {
        console.log('err', error);
    });
}

createTables(config);