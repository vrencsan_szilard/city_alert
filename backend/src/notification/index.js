
module.exports = function(firebase, database ,logger, cron, districtService, moment, PromiseChain){
    var service = require('./notification.service')(firebase, database ,logger, cron, districtService, moment, PromiseChain);
    return {
        insert: service.insert,
        getByID: service.getByID
    };
};