module.exports = function(firebase, database ,logger, cron, districtService, moment, PromiseChain){
    function postNotificationCore(req, res, insertedNotification, sendNotificationNow) {
        if (sendNotificationNow) {
            cron.sendNotification().then(function(){
                return res.status(200).json({
                    id: insertedNotification.insertId
                });
            }).catch(function(error){
                logger.log(req, error);
                return res.status(400).json(error);
            });
        } else {
            return res.status(200).json({
                id: insertedNotification.insertId
            });
        }
    }
    
    function insertNotificationLocations(req, res, locations, insertedNotification, sendNotificationNow) {
        var values               = [],
            promise2,
            districtResultIndex  = 0,
            addressResultIndex   = 0,
            promiseChain         = new PromiseChain(),
            addressChain         = new PromiseChain();
        //get the district where it's missing
        _.forEach(locations, function (location) {
            if (_.isUndefined(location.districtID)) {
                promiseChain.chain(districtService.getDistrict(location.latitude, location.longitude));
            }
        });
        //get the address where it's missing
        _.forEach(locations, function (location) {
            if (_.isUndefined(location.address)) {
                addressChain.chain(firebase.reverseGeocoding(location.latitude, location.longitude));
            }
        });
        promiseChain.flattenAllAtOnce().then(function (districtResults) {
            addressChain.flattenAllAtOnce().then(function (addressResults) {
                districtResultIndex = 0;
                addressResultIndex = 0;
                //prepare values, districtID, address, longitude, latitude, notificationID
                _.forEach(locations, function (location) {
                    if (_.isUndefined(location.districtID)) {
                        location.districtID = districtResults[districtResultIndex++].id;
                    }
                    if (_.isUndefined(location.address)) {
                        location.address = addressResults[addressResultIndex++];
                    }
                    values.push([insertedNotification.insertId, location.districtID, location.latitude, location.longitude, location.address]);
                });
                //insert into notifications_districts rows with the last inserted notification id, and the districtID
                promise2 = database.queryWithData('INSERT INTO  notifications_districts (notificationID, districtID, latitude, longitude, address) VALUES ?', [values]);
                promise2.then(function (result) {
                    return postNotificationCore(req, res, insertedNotification, sendNotificationNow);
                }).catch(function (error) {
                    logger.log(req, error);
                    return res.status(400).json(error);
                });
            }).catch(function (error) {
                logger.log(req, error);
                return res.status(400).send(error);
            });
        }).catch(function (error) {
            logger.log(req, error);
            return res.status(400).send(error);
        });
    }

    function insertNotification(req,res, data, sendNotificationNow, isCityNotification, locations){
        var promise;
        promise = database.queryWithData('INSERT INTO `notifications` SET ?;', data);
        promise.then(function(insertedNotification){
            //a new notification was created in the database
            if (!isCityNotification) {
                //this is not a city notifification so locations are inserted
                return insertNotificationLocations(req, res, locations, insertedNotification, sendNotificationNow);
            } else {
                //this is a city notification so locations are not existent we move on to the core
                return postNotificationCore(req, res, insertedNotification, sendNotificationNow);
            }
        }).catch(function(error){
            logger.log(req, error);
            return  res.status(400).json(error);
        });
    }
   
    function postNotification(req, res, notificationIsInsertedByWhiteHall){
        var data                 = req.body,
            sendNotificationNow  = false,
            locations            = req.body.locations,
            getNotificationsPromise,
            isCityNotification   = false,
            notificationsQuery   = 'SELECT notifications.id FROM notifications WHERE activeFrom = '+data.activeFrom+' AND activeUntil = '+data.activeUntil+' AND typeID = '+data.typeID;
        if (!_.isUndefined(data.id)) {
            delete data.id;
        }
        if (data.schedule === 'now' || moment(data.schedule ,'x').isSameOrBefore(moment())) {
            if (data.schedule === 'now') {
                data.schedule = moment().format('x');
            }
            sendNotificationNow = true;
        }
        if (_.isUndefined(data.locations) || data.locations.length === 0) {
            isCityNotification = true;
        }
        delete data.locations;

        if (!isCityNotification && notificationIsInsertedByWhiteHall) {
            //if it's not a city notification we check if there are other notifications in the database with the same properties
            getNotificationsPromise = database.query(notificationsQuery);
            getNotificationsPromise.then(function(notificationsInDB) {
                if (notificationsInDB[0] && notificationsInDB.length > 0) {
                    //there is at least one other notification with the same parameters, so we add the locations to that one
                    return insertNotificationLocations(req, res, locations, {insertId: notificationsInDB[0].id}, sendNotificationNow);
                } else {
                    //there are no other notifications with the same params, so a new notification is created
                    return insertNotification(req, res, data, sendNotificationNow, isCityNotification, locations);
                }
            }).catch(function(error){
                logger.log(req, error);
                return  res.status(400).json(error);
            });
        } else {
            //this is either a city notification, or a notification added from the dashboard so a new notification in created
            return insertNotification(req, res, data, sendNotificationNow, isCityNotification, locations);
        }
    }

    function getByID(req, res, id){
        var query         = "SELECT views FROM notifications WHERE ?",
            updateQuery   = "UPDATE notifications SET ? WHERE ?",
            promise       = database.queryWithData(query, {id: id}),
            currentViews  = 0;
        promise.then(function (result) {
            currentViews = result[0].views;
            database.queryWithData(updateQuery, [{views: currentViews+1}, {id: parseInt(id)}]).then(function(){
                return res.status(200).json({views: currentViews+1});
            }).catch(function(error){
                logger.log(req, error);
                return res.status(400).json(error);
            });
        }).catch(function (error) {
            logger.log(req, error);
            return res.status(400).json(error);
        });
    }
    
    return {
        insert: function(req, res, notificationIsInsertedByWhiteHall){
            return postNotification(req, res, notificationIsInsertedByWhiteHall);
        },
        getByID: getByID   
    };
};