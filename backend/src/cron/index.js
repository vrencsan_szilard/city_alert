var cronService = require('./cron.service');
module.exports = {
    start           : cronService.start,
    sendNotification: cronService.sendNotification
};