const cron = require('node-cron'),
    moment = require('moment'),
    _      = require('underscore');

function preparePushNotificationData(rows) {
    var retValue = {}, topic;
    _.each(rows, function (row) {
        topic = row.districtID + '_' + row.topic;
        if (_.isUndefined(retValue[topic])) {
            retValue[topic] = [];
        }
        retValue[topic].push(row);
    });
    return retValue;
}

module.exports = function (firebase, database, PromiseChain, logger) {
    function sendNotification() {
        var promise, preparedData,
            rowsToSend = [];

        promise = new Promise(function (resolve, reject) {
            database.query('SELECT notifications.*, notification_types.img AS typeIMG,  notification_types.name AS typeName, notification_types.topic AS topic,'+ 
            ' notifications_districts.districtID AS merge_locations_id, districts.name as merge_locations_name FROM notifications'+
            ' LEFT JOIN notification_types ON notifications.typeID = notification_types.id'+
            ' LEFT JOIN notifications_districts ON notifications.id = notifications_districts.notificationID'+
            ' LEFT JOIN districts ON notifications_districts.districtID = districts.id'+
            ' WHERE sentAt IS NULL AND notification_types.selected IS TRUE', {idColumnOfMergedRow: 'id'}).then(function (rows) {
                _.each(rows, function (row) {
                    if (moment(row.schedule, 'x').isSameOrBefore(moment())) {
                        //this row was scheduled and not sent yet
                        rowsToSend.push(row);
                    } 
                });
                if (rowsToSend.length > 0) {
                    // preparedData = preparePushNotificationData(rowsToSend);
                    firebase.sendPushNotification(rowsToSend).then(function (pushNotificationResults) {
                        logger.logNotificationResults(pushNotificationResults);
                        _.each(rowsToSend, function (row) {
                            database.queryWithData('UPDATE notifications SET ? WHERE ?', [{
                                sentAt: moment().format('x')
                            }, {
                                id: row.id
                            }]);
                        });
                        resolve(preparedData);
                    }).catch(function (error) {
                        logger.logNotificationResults(error);
                        reject('Error: '+ error);
                    });
                } else {
                    resolve('No data to send.');
                }
            });
        });

        return promise;
    }

    function deleteExpired(){
        var getNotificationsQuery = "SELECT * FROM notifications",
            deleteChain = new PromiseChain();
        database.query(getNotificationsQuery).then(function(rows){
            _.each(rows, function (notificationRow) {
                var activeUntil = moment(notificationRow.activeUntil, 'x');
                    if (moment().diff(activeUntil, 'days') > 2) {
                    deleteChain.chain(function(){
                        return database.queryWithData('DELETE FROM notifications_districts WHERE ?', {notificationID: notificationRow.id});
                    });
                    deleteChain.chain(function(){return database.queryWithData('DELETE FROM notifications WHERE ?', {id: notificationRow.id});});
                }
            });
            deleteChain.flattenInChain();
        });
    }

    return {
        start: function () {

            if (process.env.NODE_ENV !== 'production') {
                return {
                    destroy: function () {}
                };
            }
            // ┌────────────── second (optional)
            // │ ┌──────────── minute
            // │ │ ┌────────── hour
            // │ │ │ ┌──────── day of month
            // │ │ │ │ ┌────── month
            // │ │ │ │ │ ┌──── day of week
            // │ │ │ │ │ │
            // │ │ │ │ │ │
            // * * * * * *
            return cron.schedule('0,15,30,45 * * * *', function () {
            // return cron.schedule('*/1 * * * *', function () {
                sendNotification();
                deleteExpired();
            });
        },
        sendNotification: sendNotification
    };
};