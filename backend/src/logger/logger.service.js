module.exports = function(database,  moment){
    const _ = require('underscore');
    function deleteOlderLogs(){
        var oldestAllowedTimestamp = moment().subtract(10, 'days').format('x');
        database.queryWithData('DELETE FROM `log` WHERE timestamp < ?', oldestAllowedTimestamp).catch(function(){
            
        });
    }
    return {
        log: function(request, error){
           var type = '';
            _.each(request.route.methods, function(value, key){
               if (value) {
                   type += key + ' ';
               }
           }); 
            var query = "INSERT INTO `log` SET ?",
                logObject = {
                    request: type + request.route.path,
                    payload: JSON.stringify(request.body),
                    response: JSON.stringify(error),
                    timestamp: moment().format('x')
                };
            database.queryWithData(query, logObject);
            deleteOlderLogs();
        },
        logNotification: function(payload){
            var query = "INSERT INTO `log` SET ?",
            logObject = {
                request: 'Push Notification',
                payload: JSON.stringify(payload),
                response: '',
                timestamp: moment().format('x')
            };
            database.queryWithData(query, logObject);
            deleteOlderLogs();
        },
        logNotificationResults: function(results){
            var query = "INSERT INTO `log` SET ?",
            logObject = {
                request: 'Push notification results for the previous '+results.length +' notifications.',
                payload: '',
                response: JSON.stringify(results),
                timestamp: moment().format('x')
            };
            database.queryWithData(query, logObject);
        }
    };
};