module.exports = function(database, moment){
    var logger = require('./logger.service.js')(database, moment);
    return {
        log: logger.log,
        logNotification: logger.logNotification,
        logNotificationResults: logger.logNotificationResults
    }
};