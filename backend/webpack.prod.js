const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');


module.exports = merge(common, {
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'app.js'
  },
  plugins: [
    // new CleanWebpackPlugin(['**/*'], {root:  path.join(__dirname, 'dist'), verbose: true, dry: false, exclude: ['public']}),
    new CleanWebpackPlugin(['dist'], {verbose: true, dry: false, exclude: ['public']}),
    new UglifyJSPlugin({
        sourceMap: false
    }),
    new webpack.DefinePlugin({
        'process.env': {
            'NODE_ENV': JSON.stringify('production')
        }
    }),
    new CopyWebpackPlugin([{from: './src/assets/dist-related/package.json', to: 'package.json'}])
  ]
});